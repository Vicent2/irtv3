# -*- coding: utf-8 -*-
import os
import pandas as pd
import xlwt
import lxml
from lxml import etree
from datetime import datetime
from xlwt import *
import xlsxwriter
import math
from os import listdir
#IMPORTANTE, ANTES DE USAR EL SCRIPT
#Los xmls deben estar dentro de una carpeta, la ruta de esta carpeta es el path que es necesario modificar

#filename = '_LIVE_STAGE_SS16.xml'
#file = open(filename, mode='r')#archivo de lectura

#print(file.read())
#file.close()
#Estilos
#Estilo cabecera
style0 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour blue; borders: left thin, right thin, top thin, bottom thin;')
#Estilo celdas nombres xmls
style1 = xlwt.easyxf('font: name Arial; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
#Estilo celdas interiores
style3 = xlwt.easyxf('font: name Arial; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
#Estilo resultados
style4 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour black;')
#Estilo para la columna del tiempo
#style2 = xlwt.easyxf('',num_format_str='h:mm:ss')
#creamos el excel y la fila de las cabeceras
wb = xlwt.Workbook()
ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)
nombres = ws.col(0)
nombres.width = 256 * 25
zona = ws.col(11)
zona.width = 256 * 15
over = ws.col(6)
over.width = 256 * 20

#-------------------------------------
path = 'xmls/'
#Lista vacia para incluir los ficheros
lstFiles = []
#Lista con todos los ficheros del directorio:
lstDir = os.walk(path)   #os.walk()Lista directorios y ficheros
# Crea una lista de los ficheros xml que existen en el directorio y los incluye a la lista.
for root, dirs, files in lstDir:
    for fichero in files:
        (nombreFichero, extension) = os.path.splitext(fichero)
        if (extension == ".xml"):
            lstFiles.append(nombreFichero + extension)
i = 1
lstFiles.sort()

for fichero in lstFiles:
    print(fichero)
    sumT = 0
    if fichero.startswith('_'):
        Dividendo = 0
        divisor = 0
        minutos = 0
        segundos = 0
        Dividendo2 = 0
        divisor2 = 0
        minutos2 = 0
        segundos2 = 0
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        print(video)
        #if fichero.startswith('_'):
        #    part = video.text.split("-")
        #    pais = part[1]
        ws.write(i, 0, video.text, style0)  # escribimos el titulo en la celda correspondiente
        ws.write_merge(i, i, 7, 8, 'Visualizations', style1)
        ws.write(i + 1, 0, 'Brands', style0)
        ws.write(i + 1, 1, 'Impacts', style1)
        ws.write(i + 1, 2, 'Total', style1)
        ws.write(i + 1, 3, 'Max', style1)
        ws.write(i + 1, 4, 'Average', style1)
        ws.write(i + 1, 5, 'Percentage', style1)
        ws.write(i + 1, 6, 'Impacts Over time', style1)
        ws.write(i + 1, 7, 'Max', style1)
        ws.write(i + 1, 8, 'Minute', style1)
        ws.write(i + 1, 9, 'Area', style1)
        ws.write(i + 1, 10, 'Summary', style1)
        ws.write(i + 1, 11, 'Position', style1)
        ws.write(i + 2, 0, 'eurovision', style1)
        ws.write(i + 3, 0, 'glasgow', style1)
        ws.write(i + 4, 0, 'scotland', style1)
        ws.write(i + 5, 0, 'spar', style1)


        eurovision = proyecto[4].find("eurovision")
        impactosC = eurovision[0].find("Impactos")
        ws.write(i + 2, 1, impactosC.text, style1)
        total = eurovision[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 2, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 2, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = eurovision[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if (minutos2 == 0):
            ws.write(i + 2, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 2, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = eurovision[0].find("Tiempo_acumulado_average")
        ws.write(i + 2, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 2, 5, str('%.2f' % per + '%'), style1)
        ws.write(i + 2, 6, impactosC.text, style1)
        ws.write(i + 2, 7, '%.f' % float(max.text), style1)
        graf3 = eurovision.find('PRESENCE_Graf_3')
        m = 0
        total = 0
        resta = 0
        inicioF = 0
        while m <= 1000:
            apa = 'Aparicion' + str(m)
            aparicion = graf3.find(apa)
            if (aparicion is None):
                break
            else:
                m = m + 1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                    total = resta
                    inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i + 2, 8, minutos3, style1)
        area = eurovision.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 2, 9, str('%.2f' % ar + '%'), style1)
        summary = eurovision.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        ws.write(i + 2, 10, str(math.floor(sum)) + '%', style1)
        pos = eurovision.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            if (z > zo):
                zo = z
                zon1 = zon

            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 2, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 2, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 2, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 2, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 2, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 2, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 2, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 2, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 2, 11, 'Lower-Right', style1)

        # -------------------------------------------------------------------------------------------
        glasgow = proyecto[4].find("glasgow")
        if glasgow != None:
            impactosD = glasgow[0].find("Impactos")
            ws.write(i + 3, 1, impactosD.text, style1)
            total = glasgow[0].find("Tiempo_acumulado_brand")
            Dividendo = float(total.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if (minutos == 0):
                ws.write(i + 3, 2, str('%.f' % (segundos)) + 'sec', style1)
            else:
                ws.write(i + 3, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
            max = glasgow[0].find("Tiempo_acumulado_max")
            Dividendo2 = float(max.text)
            divisor2 = int(60)
            minutos2 = math.floor(Dividendo2 / divisor2)
            segundos2 = Dividendo2 % divisor2
            if (minutos2 == 0):
                ws.write(i + 3, 3, str('%.f' % (segundos2)) + 'sec', style1)
            else:
                ws.write(i + 3, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
            average = glasgow[0].find("Tiempo_acumulado_average")
            ws.write(i + 3, 4, str('%.2f' % float(average.text)) + 'sec', style1)
            presence = glasgow.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 3, 5, str('%.2f' % per + '%'), style1)
            ws.write(i + 3, 6, impactosD.text, style1)
            ws.write(i + 3, 7, '%.f' % float(max.text), style1)
            graf3 = glasgow.find('PRESENCE_Graf_3')
            m = 0
            total = 0
            resta = 0
            inicioF = 0
            while m <= 1000:
                apa = 'Aparicion' + str(m)
                aparicion = graf3.find(apa)
                if (aparicion is None):
                    break
                else:
                    m = m + 1
                    inicio = float(aparicion.find('Inicio').text)
                    fin = float(aparicion.find('Fin').text)
                    resta = fin - inicio

                    if resta > total:
                        total = resta
                        inicioF = inicio
            Dividendo3 = inicioF
            divisor3 = int(60)
            minutos3 = math.floor(Dividendo3 / divisor3)
            ws.write(i + 3, 8, minutos3, style1)
            area = glasgow.find('AREA')
            ar = float(area.find('Media_Porcentaje_Frames').text)
            ws.write(i + 3, 9, str('%.2f' % ar + '%'), style1)
            summary = glasgow.find('SUMMARY')
            sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
            sumT = math.floor(sum) + sumT
            partes = str(sum).split(".")
            if(float(partes[1]) > 59):
                partes[0] = float(partes[0]) +1

            ws.write(i + 3, 10, str('%.f'% float(partes[0])) + '%', style1)
            pos = glasgow.find('POSITION')
            zo = 0
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                if (z > zo):
                    zo = z
                    zon1 = zon
                j = j + 1
            if zon1 == 'Zona1':
                ws.write(i + 3, 11, 'Higher-Left', style1)
            if zon1 == 'Zona2':
                ws.write(i + 3, 11, 'Higher-Center', style1)
            if zon1 == 'Zona3':
                ws.write(i + 3, 11, 'Higher-Right', style1)
            if zon1 == 'Zona4':
                ws.write(i + 3, 11, 'Center-Left', style1)
            if zon1 == 'Zona5':
                ws.write(i + 3, 11, 'Center-Center', style1)
            if zon1 == 'Zona6':
                ws.write(i + 3, 11, 'Center-Right', style1)
            if zon1 == 'Zona7':
                ws.write(i + 3, 11, 'Lower-Left', style1)
            if zon1 == 'Zona8':
                ws.write(i + 3, 11, 'Lower-Center', style1)
            if zon1 == 'Zona9':
                ws.write(i + 3, 11, 'Lower-Right', style1)
        # -------------------------------------------------------------------------------------------
        scotland = proyecto[4].find("scotland")
        if scotland != None:
            impactosW = scotland[0].find("Impactos")
            ws.write(i + 4, 1, impactosW.text, style1)
            total = scotland[0].find("Tiempo_acumulado_brand")
            Dividendo = float(total.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if (minutos == 0):
                ws.write(i + 4, 2, str('%.f' % (segundos)) + 'sec', style1)
            else:
                ws.write(i + 4, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
            max = scotland[0].find("Tiempo_acumulado_max")
            Dividendo2 = float(max.text)
            divisor2 = int(60)
            minutos2 = math.floor(Dividendo2 / divisor2)
            segundos2 = Dividendo2 % divisor2
            if (minutos2 == 0):
                ws.write(i + 4, 3, str('%.f' % (segundos2)) + 'sec', style1)
            else:
                ws.write(i + 4, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
            average = scotland[0].find("Tiempo_acumulado_average")
            ws.write(i + 4, 4, str('%.2f' % float(average.text)) + 'sec', style1)
            presence = scotland.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 4, 5, str('%.2f' % per + '%'), style1)
            ws.write(i + 4, 6, impactosW.text, style1)
            ws.write(i + 4, 7, '%.f' % float(max.text), style1)
            graf3 = scotland.find('PRESENCE_Graf_3')
            m = 0
            total = 0
            resta = 0
            inicioF = 0
            while m <= 1000:
                apa = 'Aparicion' + str(m)
                aparicion = graf3.find(apa)
                if (aparicion is None):
                    break
                else:
                    m = m + 1
                    inicio = float(aparicion.find('Inicio').text)
                    fin = float(aparicion.find('Fin').text)
                    resta = fin - inicio

                    if resta > total:
                        total = resta
                        inicioF = inicio
            Dividendo3 = inicioF
            divisor3 = int(60)
            minutos3 = math.floor(Dividendo3 / divisor3)
            ws.write(i + 4, 8, minutos3, style1)
            area = scotland.find('AREA')
            ar = float(area.find('Media_Porcentaje_Frames').text)
            ws.write(i + 4, 9, str('%.2f' % ar + '%'), style1)
            summary = scotland.find('SUMMARY')
            sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
            partes = str(sum).split(".")
            if(float(partes[1]) > 59):
                partes[0] = float(partes[0]) +1
            sumT = sum + sumT
            ws.write(i + 4, 10, str('%.f'%float(partes[0])) + '%', style1)
            pos = scotland.find('POSITION')
            zo = 0
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                if (z > zo):
                    zo = z
                    zon1 = zon
                j = j + 1
            if zon1 == 'Zona1':
                ws.write(i + 4, 11, 'Higher-Left', style1)
            if zon1 == 'Zona2':
                ws.write(i + 4, 11, 'Higher-Center', style1)
            if zon1 == 'Zona3':
                ws.write(i + 4, 11, 'Higher-Right', style1)
            if zon1 == 'Zona4':
                ws.write(i + 4, 11, 'Center-Left', style1)
            if zon1 == 'Zona5':
                ws.write(i + 4, 11, 'Center-Center', style1)
            if zon1 == 'Zona6':
                ws.write(i + 4, 11, 'Center-Right', style1)
            if zon1 == 'Zona7':
                ws.write(i + 4, 11, 'Lower-Left', style1)
            if zon1 == 'Zona8':
                ws.write(i + 4, 11, 'Lower-Center', style1)
            if zon1 == 'Zona9':
                ws.write(i + 4, 11, 'Lower-Right', style1)
        # -------------------------------------------------------------------------------------------
        miche = proyecto[4].find("spar")
        impactosM = miche[0].find("Impactos")
        ws.write(i + 5, 1, impactosM.text, style1)
        total = miche[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 5, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 5, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = miche[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if (minutos2 == 0):
            ws.write(i + 5, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 5, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = miche[0].find("Tiempo_acumulado_average")
        ws.write(i + 5, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = miche.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 5, 5, str('%.2f' % per + '%'), style1)
        ws.write(i + 5, 6, impactosM.text, style1)
        ws.write(i + 5, 7, '%.f' % float(max.text), style1)
        graf3 = miche.find('PRESENCE_Graf_3')
        m = 0
        total = 0
        resta = 0
        inicioF = 0
        while m <= 1000:
            apa = 'Aparicion' + str(m)
            aparicion = graf3.find(apa)
            if (aparicion is None):
                break
            else:
                m = m + 1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                    total = resta
                    inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i + 5, 8, minutos3, style1)
        area = miche.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 5, 9, str('%.2f' % ar + '%'), style1)
        summary = miche.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        partes = str(sum).split(".")
        if(float(partes[1]) > 59):
            partes[0] = float(partes[0]) +1
        sumT = math.floor(sum) + sumT
        ws.write(i + 5, 10, str('%.f'% float(partes[0])) + '%', style1)
        pos = miche.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            if (z > zo):
                zo = z
                zon1 = zon
            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 5, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 5, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 5, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 5, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 5, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 5, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 5, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 5, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 5, 11, 'Lower-Right', style1)
        # -------------------------------------------------------------------------------------------

        # -------------------------------------------------------------------------------------------
        i = i + 9

for fichero in lstFiles:
    sumT = 0
    if fichero.startswith('_') == False:
        Dividendo = 0
        divisor = 0
        minutos = 0
        segundos = 0
        Dividendo2 = 0
        divisor2 = 0
        minutos2 = 0
        segundos2 = 0
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[1]  # primer elto del que obtenemos el título de nuestro video
        if fichero.startswith('_'):
            part = video.text.split("-")
            pais = part[1]
        ws.write(i, 0, video.text, style0)  # escribimos el titulo en la celda correspondiente
        ws.write_merge(i, i, 7, 8, 'Visualizations', style1)
        ws.write(i + 1, 0, 'Brands', style0)
        ws.write(i + 1, 1, 'Impacts', style1)
        ws.write(i + 1, 2, 'Total', style1)
        ws.write(i + 1, 3, 'Max', style1)
        ws.write(i + 1, 4, 'Average', style1)
        ws.write(i + 1, 5, 'Percentage', style1)
        ws.write(i + 1, 6, 'Impacts Over time', style1)
        ws.write(i + 1, 7, 'Max', style1)
        ws.write(i + 1, 8, 'Minute', style1)
        ws.write(i + 1, 9, 'Area', style1)
        ws.write(i + 1, 10, 'Summary', style1)
        ws.write(i + 1, 11, 'Position', style1)
        ws.write(i + 2, 0, 'eurovision', style1)
        ws.write(i + 3, 0, 'glasgow', style1)
        ws.write(i + 4, 0, 'scotland', style1)
        ws.write(i + 5, 0, 'spar', style1)

        eurovision = proyecto[4].find("eurovision")
        impactosC = eurovision[0].find("Impactos")
        ws.write(i + 2, 1, impactosC.text, style1)
        total = eurovision[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 2, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 2, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = eurovision[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if(minutos2==0):
            ws.write(i + 2, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 2, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = eurovision[0].find("Tiempo_acumulado_average")
        ws.write(i + 2, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 2, 5, str('%.2f'% per +'%'), style1)
        ws.write(i + 2, 6, impactosC.text, style1)
        ws.write(i+2, 7, '%.f' % float(max.text), style1)
        graf3 = eurovision.find('PRESENCE_Graf_3')
        m=0
        total=0
        resta=0
        inicioF=0
        while m<=1000:
            apa = 'Aparicion'+str(m)
            aparicion = graf3.find(apa)
            if(aparicion is  None):
                break
            else:
                m = m+1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                   total = resta
                   inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i+2,8, minutos3, style1)
        area = eurovision.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 2, 9, str('%.2f' % ar + '%'), style1)
        summary = eurovision.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        sumT = math.floor(sum) + sumT
        partes = str(sum).split(".")
        if(float(partes[1]) > 59):
            partes[0] = float(partes[0]) +1
        ws.write(i + 2, 10, str('%.f'% float(partes[0])) + '%', style1)
        pos = eurovision.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon= 'Zona'+str(j)
            z = int(pos.find(zon).text)
            if(z>zo):
                zo=z
                zon1 = zon

            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 2, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 2, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 2, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 2, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 2, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 2, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 2, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 2, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 2, 11, 'Lower-Right', style1)

        #-------------------------------------------------------------------------------------------
        glasgow = proyecto[4].find("glasgow")
        impactosD = glasgow[0].find("Impactos")
        ws.write(i + 3, 1, impactosD.text, style1)
        total = glasgow[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 3, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 3, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = glasgow[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if (minutos2 == 0):
            ws.write(i + 3, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 3, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = glasgow[0].find("Tiempo_acumulado_average")
        ws.write(i + 3, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = glasgow.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 3, 5, str('%.2f'% per +'%'), style1)
        ws.write(i + 3, 6, impactosD.text, style1)
        ws.write(i + 3, 7, '%.f' % float(max.text), style1)
        graf3 = glasgow.find('PRESENCE_Graf_3')
        m = 0
        total = 0
        resta = 0
        inicioF = 0
        while m <= 1000:
            apa = 'Aparicion' + str(m)
            aparicion = graf3.find(apa)
            if (aparicion is None):
                break
            else:
                m = m + 1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                    total = resta
                    inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i + 3, 8, minutos3, style1)
        area = glasgow.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 3, 9, str('%.2f' % ar + '%'), style1)
        summary = glasgow.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        sumT = math.floor(sum) + sumT
        partes = str(sum).split(".")
        if(float(partes[1]) > 59):
            partes[0] = float(partes[0]) +1
        ws.write(i + 3, 10, str('%.f'% float(partes[0])) + '%', style1)
        pos = glasgow.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            if (z > zo):
                zo = z
                zon1 = zon
            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 3, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 3, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 3, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 3, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 3, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 3, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 3, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 3, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 3, 11, 'Lower-Right', style1)
        #-------------------------------------------------------------------------------------------
        scotland = proyecto[4].find("scotland")
        impactosW = scotland[0].find("Impactos")
        ws.write(i + 4, 1, impactosW.text, style1)
        total = scotland[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 4, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 4, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = scotland[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if (minutos2 == 0):
            ws.write(i + 4, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 4, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = scotland[0].find("Tiempo_acumulado_average")
        ws.write(i + 4, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = scotland.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 4, 5, str('%.2f'% per +'%'), style1)
        ws.write(i + 4, 6, impactosW.text, style1)
        ws.write(i + 4, 7, '%.f' % float(max.text), style1)
        graf3 = scotland.find('PRESENCE_Graf_3')
        m = 0
        total = 0
        resta = 0
        inicioF = 0
        while m <= 1000:
            apa = 'Aparicion' + str(m)
            aparicion = graf3.find(apa)
            if (aparicion is None):
                break
            else:
                m = m + 1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                    total = resta
                    inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i + 4, 8, minutos3, style1)
        area = scotland.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 4, 9, str('%.2f' % ar + '%'), style1)
        summary = scotland.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        sumT = math.floor(sum) + sumT
        partes = str(sum).split(".")
        if(float(partes[1]) > 59):
            partes[0] = float(partes[0]) +1
        ws.write(i + 4, 10, str('%.f'% float(partes[0]))  + '%', style1)
        pos = scotland.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            if (z > zo):
                zo = z
                zon1 = zon
            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 4, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 4, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 4, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 4, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 4, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 4, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 4, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 4, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 4, 11, 'Lower-Right', style1)
        #-------------------------------------------------------------------------------------------
        miche = proyecto[4].find("spar")
        impactosM = miche[0].find("Impactos")
        ws.write(i + 5, 1, impactosM.text, style1)
        total = miche[0].find("Tiempo_acumulado_brand")
        Dividendo = float(total.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if (minutos == 0):
            ws.write(i + 5, 2, str('%.f' % (segundos)) + 'sec', style1)
        else:
            ws.write(i + 5, 2, str('%.f' % (minutos)) + 'min ' + str('%.f' % (segundos)) + 'sec', style1)
        max = miche[0].find("Tiempo_acumulado_max")
        Dividendo2 = float(max.text)
        divisor2 = int(60)
        minutos2 = math.floor(Dividendo2 / divisor2)
        segundos2 = Dividendo2 % divisor2
        if (minutos2 == 0):
            ws.write(i + 5, 3, str('%.f' % (segundos2)) + 'sec', style1)
        else:
            ws.write(i + 5, 3, str('%.f' % (minutos2)) + 'min ' + str('%.f' % (segundos2)) + 'sec', style1)
        average = miche[0].find("Tiempo_acumulado_average")
        ws.write(i + 5, 4, str('%.2f' % float(average.text)) + 'sec', style1)
        presence = miche.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 5, 5, str('%.2f'% per +'%'), style1)
        ws.write(i + 5, 6, impactosM.text, style1)
        ws.write(i + 5, 7, '%.f' % float(max.text), style1)
        graf3 = miche.find('PRESENCE_Graf_3')
        m = 0
        total = 0
        resta = 0
        inicioF = 0
        while m <= 1000:
            apa = 'Aparicion' + str(m)
            aparicion = graf3.find(apa)
            if (aparicion is None):
                break
            else:
                m = m + 1
                inicio = float(aparicion.find('Inicio').text)
                fin = float(aparicion.find('Fin').text)
                resta = fin - inicio

                if resta > total:
                    total = resta
                    inicioF = inicio
        Dividendo3 = inicioF
        divisor3 = int(60)
        minutos3 = math.floor(Dividendo3 / divisor3)
        ws.write(i + 5, 8, minutos3, style1)
        area = miche.find('AREA')
        ar = float(area.find('Media_Porcentaje_Frames').text)
        ws.write(i + 5, 9, str('%.2f' % ar + '%'), style1)
        summary = miche.find('SUMMARY')
        sum = float(summary.find('Porcentaje_del_Total_por_marcas').text)
        sumT = math.floor(sum) + sumT
        partes = str(sum).split(".")
        if(float(partes[1]) > 59):
            partes[0] = float(partes[0]) +1
        ws.write(i + 5, 10, str('%.f'% float(partes[0]))  + '%', style1)
        pos = miche.find('POSITION')
        zo = 0
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            if (z > zo):
                zo = z
                zon1 = zon
            j = j + 1
        if zon1 == 'Zona1':
            ws.write(i + 5, 11, 'Higher-Left', style1)
        if zon1 == 'Zona2':
            ws.write(i + 5, 11, 'Higher-Center', style1)
        if zon1 == 'Zona3':
            ws.write(i + 5, 11, 'Higher-Right', style1)
        if zon1 == 'Zona4':
            ws.write(i + 5, 11, 'Center-Left', style1)
        if zon1 == 'Zona5':
            ws.write(i + 5, 11, 'Center-Center', style1)
        if zon1 == 'Zona6':
            ws.write(i + 5, 11, 'Center-Right', style1)
        if zon1 == 'Zona7':
            ws.write(i + 5, 11, 'Lower-Left', style1)
        if zon1 == 'Zona8':
            ws.write(i + 5, 11, 'Lower-Center', style1)
        if zon1 == 'Zona9':
            ws.write(i + 5, 11, 'Lower-Right', style1)
        #-------------------------------------------------------------------------------------------

        #-------------------------------------------------------------------------------------------

        #-------------------------------------------------------------------------------------------
        print(sumT)
        i = i + 9

#ws.write(0, 1, 'Minutos', style1)
#ws.write(0, 2, 'Secs', style1)
#ws.write(0, 3, 'Frames', style1)
#ws.write(0, 4, 'Tiempo', style1)
wb.save('Eventos scotland.xls')
