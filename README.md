# README #

# Libraries to install #

---- Libraries for wxpython ---- Ubuntu 18.04

    sudo apt install make gcc libgtk-3-dev libwebkitgtk-dev libwebkitgtk-3.0-dev libgstreamer-gl1.0-0 freeglut3 freeglut3-dev python-gst-1.0 python3-gst-1.0 libglib2.0-dev ubuntu-restricted-extras libgstreamer-plugins-base1.0-dev

----WXPython---- Ubuntu 18.04

Be patient, this may take a long while

    pip3 install wxpython --user

----WXPython---- Ubuntu 16.04

Be patient, this may take a long while

     pip3 install -U \
      -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 \
      wxPython


----Opencv----

    pip3 install opencv-python --user

----Imgaug----

    pip3 install git+https://github.com/aleju/imgaug --user

---- Shapely-----

    pip3 install shapely --user
    
----Matplotlib----
    
    pip3 install matplotlib --user

----ffmpeg----

    sudo apt-get install ffmpeg

----PiXMLabs----

Change USUARIOBITBUCKET and write your bitbucket user 


    pip3 install git+https://USUARIOBITBUCKET@bitbucket.org/newagepixelabs/pixmlabs.git --user  (para Actualizar a�adir) -U
