import matplotlib  # librearia utilizada para crear figuras y subfiguras en python
from matplotlib.patches import Rectangle  # para dibujar los rectangulos

matplotlib.use('WXAgg')


class LogoRectangle():
    def __init__(self, rectangulo, panel, color, logo=0, listLogos=None):
        self.rectangulo = rectangulo
        self.panel = panel
        self.logo = logo
        self.listLogos = listLogos
        self.rn = None

        self.height = 20
        self.width = 0
        self.x = 0
        self.y = 0

        self.color = color
        self.creado = False

        self.dibujarLogo()

    def dibujarLogo(self):
        self.rectangulo.set_linewidth(2)
        self.x = int(self.rectangulo.get_x())
        self.y = int(self.rectangulo.get_y()) - int(self.height)
        self.width = int(self.rectangulo.get_width())

        self.rn = Rectangle((self.x, self.y), self.width, self.height, facecolor=self.color, edgecolor=self.color)

        self.annotation = self.panel.axes.annotate(self.listLogos[self.logo], (self.x + (self.width / 2), self.y + (self.height / 2) + 1),
                                 color='black', weight='bold', fontsize=10, ha='center', va='center')
        self.panel.axes.add_patch(self.rectangulo)

        self.panel.axes.add_patch(self.rn)

    #def borrarLogo(self):
    #    self.x = 0
    #    self.y = 0
    #    self.width = 0
    #    for rectangulo in self.rectangulos.values():
    #        self.panel.axes.remove(rectangulo)

    def set_xy(self, p):
        x, y = p
        logo_rectangle_y = y - self.height
        self.rectangulo.set_xy(p)
        self.annotation.remove()
        self.rn.set_xy((x, logo_rectangle_y))
        self.annotation = self.panel.axes.annotate(self.listLogos[self.logo], (x + (self.width / 2), logo_rectangle_y + (self.height / 2) + 1),
                                 color='black', weight='bold', fontsize=10, ha='center', va='center')

    def get_width(self):
        return self.rectangulo.get_width()

    def get_height(self):
        return self.rectangulo.get_height()

    def set_width(self, w):
        self.rectangulo.set_width(w)

    def set_height(self, h):
        self.rectangulo.set_height(h)

