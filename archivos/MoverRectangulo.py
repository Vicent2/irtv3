from pixmlabs import Point, create_bbox


class MoverRectangulo():
    def __init__(self, axes, canvas, panel, logoRectangulos, coords_Rect):



        self.rectangulo = None
        self.pressed = False
        self.axes = axes
        self.logoRectangulos = logoRectangulos
        self.canvas = canvas
        #self.coords_Rect = coords_Rect
        self.bboxes = coords_Rect
        self.aux = None
        self.coords = ""
        self.coords_viejas = ""
        self.panel = panel




        self.canvas.mpl_connect('button_press_event', self.MovementOnPress)
        self.canvas.mpl_connect('button_release_event', self.__onRelease)
        self.canvas.mpl_connect('motion_notify_event', self.__onMotion)




    def MovementOnPress(self, event):

        if event.xdata is not None and event.ydata is not None:

            self.pressed_point = Point(event.xdata, event.ydata)

            for bbox, rect in zip(self.bboxes, self.logoRectangulos):


                xmin, xmax, ymax, ymin = bbox.xmin, bbox.xmax, bbox.ymin, bbox.ymin - 20  # Default

                textBbox = create_bbox(bbox.frame, bbox.brand, (int(xmin), int(xmax), int(ymin), int(ymax)))
                if textBbox.is_inside(self.pressed_point):
                    self.old_bbox = bbox
                    self.pressed = True
                    self.rectangulo = rect




    # def onPress(self, event):
    #
    #     if event.xdata is not None and event.ydata is not None:
    #         # Upon initial press of the mouse record the origin and record the mouse as pressed
    #
    #         self.x0 = event.xdata
    #         self.y0 = event.ydata
    #
    #         for i, logo in enumerate(self.logoRectangulos):
    #
    #             self.minimo = logo.y + logo.height
    #             self.maximo = logo.x + logo.width
    #
    #             if ((self.maximo >= self.x0 >= logo.x) and (logo.y <= self.y0 <= self.minimo)):
    #                 self.coords = str(int(logo.x)) + "-" + str(int(self.minimo))
    #                 self.i=i
    #                 self.rectangulo = logo.rectangulo
    #                 self.coords_viejas = str(int(self.rectangulo.get_x())) + "-" + str(int(self.rectangulo.get_y()))
    #                 self.xminV=int(self.rectangulo.get_x())
    #                 self.yminV=int(self.rectangulo.get_y())
    #                 self.w = int(self.rectangulo.get_width())
    #                 self.h = int(self.rectangulo.get_height())
    #                 self.aux = logo
    #                 self.pressed = True
    #                 self.logoRectangulos.remove(logo)
    #                 logo.creado = True

    def __onMotion(self, event):

        if self.pressed:

            w, h = self.rectangulo.width, self.rectangulo.height()

            x, y = int(event.xdata), int(event.ydata)

            # if w < x + w < self.panel.imageSize[0] and h < y + h < self.panel.imageSize[1]:
            #     self.rectangulo.set_xy((x, y))
            self.axes.add_patch(self.rectangulo)
            self.axes.figure.canvas.draw()


    # def _onMotion(self, event):
    #
    #     if self.pressed:
    #         if event.xdata is not None and event.ydata is not None:
    #             # Upon initial press of the mouse record the origin and record the mouse as pressed
    #
    #             self.x0 = event.xdata
    #             self.y0 = event.ydata
    #             if self.x0 < 0:
    #                 self.x0 = 0
    #             if self.y0 < 0:
    #                 self.y0 = 0
    #             if self.x0 + self.w > self.panel.imageSize[0]:
    #                 self.x0 = self.x0 - ((self.x0 + self.w) - self.panel.imageSize[0])
    #             if self.y0 + self.h > self.panel.imageSize[1]:
    #                 self.y0 = self.y0 - ((self.y0 + self.h) - self.panel.imageSize[1])
    #
    #             self.rectangulo.set_xy((int(self.x0), int(self.y0)))
    #
    #         self.axes.add_patch(self.rectangulo)
    #         self.axes.figure.canvas.draw()

    def __onRelease(self, event):
        if not self.pressed or not self.pressed_tamanyo:

            w, h = self.rectangulo.get_width(), self.rectangulo.get_height()

            x, y = int(event.xdata), int(event.ydata)

            #if w < x + w < self.panel.imageSize[0] and h < y + h < self.panel.imageSize[1]:


    # def _onRelease(self, event):
    #
    #     if self.pressed:
    #         if event.xdata is not None and event.ydata is not None:
    #             self.x0 = event.xdata
    #             self.y0 = event.ydata
    #             if self.x0 < 0:
    #                 self.x0 = 0
    #             if self.y0 < 0:
    #                 self.y0 = 0
    #             if self.x0 + self.w > self.panel.imageSize[0]:
    #                 self.x0 = self.x0 - ((self.x0 + self.w) - self.panel.imageSize[0])
    #             if self.y0 + self.h > self.panel.imageSize[1]:
    #                 self.y0 = self.y0 - ((self.y0 + self.h) - self.panel.imageSize[1])
    #             # Upon initial press of the mouse record the origin and record the mouse as pressed
    #
    #
    #             self.rectangulo.set_xy((int(self.x0), int(self.y0)))
    #
    #             self.aux.x = int(self.x0)
    #             self.aux.y = int(self.y0 - 20)
    #
    #
    #             self.coords_Rect[self.i] = self.rectangulo
    #
    #             if ((self.coords_viejas) in self.coords_Rect):
    #                 del (self.coords_Rect[self.coords_viejas])
    #
    #         self.aux.dibujarLogo(self.rectangulo)
    #         self.axes.figure.canvas.draw()
    #
    #         for logo in self.logoRectangulos:
    #             if logo.creado == True:
    #                 self.logoRectangulos.remove(logo)
    #                 logo.borrarLogo()
    #         self.pressed = False
    #         self.canvas.draw()
    #
    #         self.panel._onRelease(event)
    #         bbox_extent=[self.xminV,self.yminV,int(self.x0),int(self.y0),self.logoRectangulos[self.i].color]
    #
    #     else:
    #         bbox_extent=[0,0,0,0,0]
    #
    #     #return bbox_extent
