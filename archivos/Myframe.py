import _pickle as cPickle
import csv
import glob  # glob es la libreria que nos permite trabajar con el estilo de pathname de unix
import os
import os.path
from collections import defaultdict
from datetime import datetime
import subprocess
import cv2
import wx  # wx es la libreria de wxpyhton para crear interfaces de usuario en python (es parecida a tkinter)
from matplotlib.colors import *
from matplotlib.patches import Rectangle  # para dibujar los rectangulos
from pixmlabs import PiXMLabs as PXml
from pixmlabs import create_bbox, Point
import icon.icons as icons
import main as main
from archivos import LogoRectangle as LR
from archivos import RectangleSelectImagePanel as RS
from augment.createcrops import Crops
from augment.createsamples import Samples
from augment.sampleaugment import aum
from extract_video.video2im import VideoConverter
from strings.dialog import dialog
from strings.menu import menu
from strings.multilan import DEFAULT_LANGUAGE
from strings.states import state
from yolo import initializeYOLO as iY



class MyFrame(wx.Frame):
    '''Clase que define la ventana de la aplicacion. Utilizamos la calse panel definida anteriormente y creamos una ventana
    con tres partes. La primera contiene los listbox de los logos y los rectangulos de detecciones, ademas de los botones.
    La segunda cuenta con el panel en el que se muestra la imagen y la tercera tiene los botones de anterior y siguiente.'''
    #######   TODO! IRT INIT ######
    '''INICIALIZACIÓN IRT '''

    def __init__(self, parent, id, title):
        ## TODO! __init__
        # Inicializaciones de las funciones


        super(MyFrame, self).__init__(parent, id, title, wx.DefaultPosition, wx.GetDisplaySize())


        self.ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.icons_ = icons.IconManager(self.ROOT_PATH)
        self.colorPixel=(216,216,216)
        #self.colorPixel = (216, 6, 0)
        self.grayAPP=(242 , 241 , 240)
        self.language = DEFAULT_LANGUAGE
        self.init_menubar()
        self.init_toolbar()

        self.boxes_paneles()
        self.variables()
        self.init_funcionales()
        self.ordenar_boxes()
        self.OnBind()
        self.OnButtonKey()

    def init_menubar(self):
        ## TODO! init_menubar
        self.menubar = wx.MenuBar()
        self.fileMenu = wx.Menu()
        self.fileMenu.Append(1, menu.OPEN_PROJECT, menu.OPEN_PROJECT)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(4, menu.OPEN_VIDEOS, menu.OPEN_VIDEOS)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(2, menu.OPEN_LOGOS, menu.OPEN_LOGOS)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(3, menu.OPEN_IMAGES, menu.OPEN_IMAGES)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(5, menu.SAVE_PROJECT, menu.SAVE_PROJECT)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(8, menu.ACTIVATE_DEL, menu.ACTIVATE_DEL)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(9, menu.UNDO, menu.UNDO)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(11, menu.XMLD, menu.XMLD)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(10, menu.FRAME_TXT, menu.FRAME_TXT)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(12, menu.LABELS, menu.LABELS)
        self.fileMenu.Append(13, menu.XML_LABELS, menu.XML_LABELS)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(15, menu.YOLO, menu.YOLO)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(wx.ID_HELP_CONTEXT, menu.LANGUAGE,menu.LANGUAGE)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(43, menu.REINIT, menu.REINIT)
        self.fileMenu.AppendSeparator()
        self.fileMenu.Append(6, menu.EXIT, menu.EXIT)

        self.menubar.Append(self.fileMenu, menu.FILE)
        self.SetMenuBar(self.menubar)

    def init_toolbar(self):
        ## TODO! init_toolbar

        self.mytoolbar = self.CreateToolBar()

        bm_logos = self.icons_.to_bitmap(icons.OPEN_LOGOS_PIC, 24)
        bm_home = self.icons_.to_bitmap(icons.OPEN_PROJECT_PIC, 24)
        bm_picture = self.icons_.to_bitmap(icons.OPEN_IMAGES_PIC, 24)
        bm_video = self.icons_.to_bitmap(icons.OPEN_VIDEO_PIC, 24)
        bm_save = self.icons_.to_bitmap(icons.SAVE_PIC, 24)
        bm_augment = self.icons_.to_bitmap(icons.AUGMENT_PIC, 24)
        bm_xml = self.icons_.to_bitmap(icons.XML_PIC, 24)
        bm_txt = self.icons_.to_bitmap(icons.XML_TXT_PIC, 24)
        bm_labels = self.icons_.to_bitmap(icons.LABELS_PIC, 24)
        bm_xml_labels = self.icons_.to_bitmap(icons.XML_LABELS_PIC, 24)
        bm_language = self.icons_.to_bitmap(icons.icon.LANGUAGE_PIC, 24)

        #bm_reinit = self.icons_.to_bitmap(icons.icon.OPEN_LOGOS_PIC, 24)


        self.mytoolbar.AddTool(1, icons.OPEN_PROJECT_NAME, bm_home, icons.icon.OPEN_PROJECT_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(4, icons.OPEN_VIDEO_NAME, bm_video, icons.icon.OPEN_VIDEO_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(2, icons.OPEN_LOGOS_NAME, bm_logos, icons.icon.OPEN_LOGOS_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(3, icons.OPEN_IMAGES_NAME, bm_picture, icons.icon.OPEN_IMAGES_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(11, icons.XML_NAME, bm_xml, icons.icon.XML_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(7, icons.AUGMENT_NAME, bm_augment, icons.icon.AUGMENT_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(10, icons.XML_TXT_NAME, bm_txt, icons.icon.XML_TXT_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(12, icons.LABELS_NAME, bm_labels, icons.icon.LABELS_HELP)
        self.mytoolbar.AddTool(13, icons.XML_LABELS_NAME, bm_xml_labels, icons.icon.XML_LABELS_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(5, icons.SAVE_NAME, bm_save, icons.icon.SAVE_HELP)
        self.mytoolbar.AddSeparator()
        self.mytoolbar.AddTool(wx.ID_HELP_CONTEXT, 'Language', bm_language, icons.icon.LANGUAGE_HELP)

    def reinit_toolbar(self):
        ## TODO! reinit_toolbar
        self.mytoolbar.SetToolShortHelp(1, icons.icon.OPEN_PROJECT_HELP)
        self.mytoolbar.SetToolShortHelp(2, icons.icon.OPEN_LOGOS_HELP)
        self.mytoolbar.SetToolShortHelp(3, icons.icon.OPEN_IMAGES_HELP)
        self.mytoolbar.SetToolShortHelp(4, icons.icon.OPEN_VIDEO_HELP)
        self.mytoolbar.SetToolShortHelp(5, icons.icon.SAVE_HELP)
        self.mytoolbar.SetToolShortHelp(7, icons.icon.AUGMENT_HELP)
        self.mytoolbar.SetToolShortHelp(10, icons.icon.XML_TXT_HELP)
        self.mytoolbar.SetToolShortHelp(11, icons.icon.XML_HELP)
        self.mytoolbar.SetToolShortHelp(12, icons.icon.LABELS_HELP)
        self.mytoolbar.SetToolShortHelp(13, icons.icon.XML_LABELS_HELP)
        self.mytoolbar.SetToolShortHelp(wx.ID_HELP_CONTEXT, icons.icon.LANGUAGE_HELP)
        self.mytoolbar.SetToolNormalBitmap(wx.ID_HELP_CONTEXT, self.icons_.to_bitmap(icons.icon.LANGUAGE_PIC, 24))

    def reinit_menubar(self):
        ## TODO! reinit_menubar
        self.fileMenu.SetLabel(1, menu.OPEN_PROJECT)
        self.fileMenu.SetLabel(2, menu.OPEN_LOGOS)
        self.fileMenu.SetLabel(3, menu.OPEN_IMAGES)
        self.fileMenu.SetLabel(4, menu.OPEN_VIDEOS)
        self.fileMenu.SetLabel(5, menu.SAVE_PROJECT)
        self.fileMenu.SetLabel(6, menu.EXIT)
        self.fileMenu.SetLabel(8, menu.ACTIVATE_DEL)
        self.fileMenu.SetLabel(9, menu.UNDO)
        self.fileMenu.SetLabel(10, menu.FRAME_TXT)
        self.fileMenu.SetLabel(11, menu.XMLD)
        self.fileMenu.SetLabel(12, menu.LABELS)
        self.fileMenu.SetLabel(13, menu.XML_LABELS)
        self.fileMenu.SetLabel(15, menu.YOLO)
        self.fileMenu.SetLabel(43, menu.REINIT)
        self.fileMenu.SetLabel(wx.ID_HELP_CONTEXT, menu.LANGUAGE)
        self.menubar.Remove(0)
        self.menubar.Append(self.fileMenu, menu.FILE)

    def variables(self):
        ## TODO! variables
        # Variables usadas por el programa

        self.lastRectangles = []
        self.imagenes = []
        self.ultimoRectangulo = None  # Variable par0a guardar el ultimo rectangulo dibujado
        self.listaRectangulosLogos = []
        self.detecImagenes = []  # array con los rectangulos de detecciones (array de array de diccionarios) [imagen][logo][rectangulo]
        self.logos = []  # array con el nombre de los logos a estudiar
        self.totalLogos = 0  # numero total de logos
        self.totalPictures = 0  # numero total de imagenes
        self.currentLogo = 0  # logo actual (posicion del array de logos)
        self.currentPicture = 0  # imagen actual (posicion del array fde imagenes)
        self.colores =['#33FF66', "#00FFFF","#FF0000", "#FFB400","#26CBC7","#21668F",'#FF668F','#21FF8F','#30668F','#2166EE','#2AA68F','#21644F','#28DD8F','#28AA8F','#2381FF']
        self.listaRectangulos = {}
        self.sizes = []


        self.estado=wx.TextCtrl(self,value=state.INIT,pos=(300,100),style=wx.TE_READONLY|wx.TE_CENTER|wx.BORDER_NONE,size=(300,25))
        self.estado.SetBackgroundColour(self.grayAPP)
        self.estado.SetForegroundColour('black')


        self.last_save_time = datetime.now()
        self.tiempo = wx.TextCtrl(self, value=state.LAST_SAVE + self.last_save_time.strftime("%H:%M"), pos=(2, 0),style=wx.TE_READONLY|wx.TE_CENTER|wx.BORDER_NONE,size=(200,25))
        self.tiempo.SetBackgroundColour(self.grayAPP)

        self.tiempo.SetForegroundColour('black')
        self.minutos = datetime.now().minute
        self.masDetect = 0
        convert = lambda text: int(text) if text.isdigit() else text
        self.alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]

    def boxes_paneles(self):
        ## TODO! boxes_paneles

        #Resolucion de nuestra pantalla:
        self.ancho, self.alto = self.resolucionPantalla()

        # cajas que definen el layout de la apliacion
        self.vbox = wx.BoxSizer(wx.VERTICAL)  # Caja que define el layout vertical
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)  # Caja que define el layout de la parte superior izquierda (listbox)
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)  # Caja que define el layout de la parte central (boton central)
        self.hbox4 = wx.BoxSizer(
            wx.HORIZONTAL)  # Caja que define el layout de la parte inferior (botones anterior y siguiente)
        self.vbox2 = wx.BoxSizer(wx.VERTICAL)

        # Definimos tres paneles, uno con los listbox y los botones, otro para la imagen y otro para los botones de abajo
        # self.panel = wx.Panel(self, -1, pos=(0, 0),
        #                       size=(wx.GetDisplaySize()[0], 250))  # panel con los listbox y los botones
        # self.panel2 = RS.RectangleSelectImagePanel(self, 0, 250, wx.GetDisplaySize()[0],
        #                                            wx.GetDisplaySize()[1] - 300, change_bbox=self.change_bbox)  # panel de la imagen
        #
        # self.panel3 = wx.Panel(self, -1, pos=(0, wx.GetDisplaySize()[1] - 100),
        #                        size=(wx.GetDisplaySize()[0], 100))  # panel para los botones inferior

        self.panel = wx.Panel(self, -1, pos=(0, 0),
                              size=(self.ancho, self.alto*0.26042))  # panel con los listbox y los botones
        # panel de la imagen
        self.panel2 = RS.RectangleSelectImagePanel(self, 0, self.alto*0.20, self.ancho,self.alto*0.68,
                                                   change_bbox=self.change_bbox)

        self.panel3 = wx.Panel(self, -1, pos=(0, self.alto*0.88),
                               size=(self.ancho, self.alto*0.40042))  # panel para los botones inferiores

    def init_funcionales(self):
        ## TODO! init_funcionales
        # Definimos Todos los botones, listas, checkboxs y textos

        # PANEL SUPERIOR

        self.list_rectangulos = wx.ListBox(self.panel, 26, (0, 0), (85,130), [],
                                           wx.LB_SINGLE)  # listbox de los rectangulos
        self.list_rectangulos.SetToolTip(wx.ToolTip(icons.icon.LIST_DETECT_HELP))
        self.list_rectangulos.SetBackgroundColour(self.colorPixel)

        self.list_logos = wx.ListBox(self.panel, 27, (0, 0), (85,130), [],
                                     wx.LB_SINGLE)  # listbox de los logos
        self.list_logos.SetToolTip(wx.ToolTip(icons.icon.LIST_LOGOS_HELP))
        self.list_logos.SetBackgroundColour(self.colorPixel)

        self.CheckBox = wx.CheckBox(self.panel, wx.ID_BOLD, icons.MORE_DET_NAME)
        self.CheckBox.SetToolTip(wx.ToolTip(icons.icon.MORE_DET_HELP))
        #self.CheckBox.SetBackgroundColour(self.colorPixel)

        bm_add = self.icons_.to_bitmap(icons.ADD_PIC, 18)
        self.btn_add = wx.BitmapButton(self.panel, wx.ID_ADD, bm_add)
        self.btn_add.SetWindowStyle(wx.NO_BORDER)
        self.btn_add.SetToolTip(wx.ToolTip(icons.icon.ADD_HELP))
        self.btn_add.SetBackgroundColour(self.colorPixel)

        bm_rem = self.icons_.to_bitmap(icons.REMOVE_PIC, 18)
        self.btn_borr = wx.BitmapButton(self.panel, wx.ID_REMOVE, bm_rem)
        self.btn_borr.SetToolTip(wx.ToolTip(icons.icon.REMOVE_HELP))
        self.btn_borr.SetBackgroundColour(self.colorPixel)

        bm_undo = self.icons_.to_bitmap(icons.UNDO_PIC, 18)
        self.btn_marchaAtras = wx.BitmapButton(self.panel, wx.ID_FILE2, bm_undo)
        self.btn_marchaAtras.SetToolTip(wx.ToolTip(icons.icon.UNDO_HELP))
        self.btn_marchaAtras.SetBackgroundColour(self.colorPixel)

        bm_deleteAll = self.icons_.to_bitmap(icons.REMOVE_ALL_PIC, 18)
        self.btn_borrar_todos = wx.BitmapButton(self.panel, wx.ID_FILE3, bm_deleteAll)
        self.btn_borrar_todos.SetToolTip(wx.ToolTip(icons.icon.REMOVE_ALL_HELP))
        self.btn_borrar_todos.SetBackgroundColour(self.colorPixel)

        bm_deleteDraw = self.icons_.to_bitmap(icons.DELETE_DRAW_PIC, 18)
        self.btn_borrar_marcados = wx.BitmapButton(self.panel, wx.ID_ABORT, bm_deleteDraw)
        self.btn_borrar_marcados.SetToolTip(wx.ToolTip(icons.icon.DELETE_DRAW_HELP))
        self.btn_borrar_marcados.SetBackgroundColour(self.colorPixel)

        bm_select = self.icons_.to_bitmap(icons.SELECT_PIC, 18)
        self.btn_selection = wx.BitmapButton(self.panel, wx.ID_FILE4, bm_select)
        self.btn_selection.SetToolTip(wx.ToolTip(icons.icon.SELECT_HELP))
        self.btn_selection.SetBackgroundColour(self.colorPixel)

        self.caja = wx.TextCtrl(self.panel)
        self.caja.SetToolTip(wx.ToolTip(icons.icon.BOX_HELP))
        self.caja.SetValue('1')
        self.caja.SetBackgroundColour(self.colorPixel)

        self.caja2 = wx.TextCtrl(self.panel)
        self.caja2.SetToolTip(wx.ToolTip(icons.icon.BOX_HELP))
        self.caja2.SetValue('1')
        self.caja2.SetBackgroundColour(self.colorPixel)
        # PANEL INFERIOR

        bm_next = self.icons_.to_bitmap(icons.NEXT_PIC, 18)
        self.btn_mas1 = wx.BitmapButton(self.panel3, wx.ID_PREVIEW_NEXT, bm_next)
        self.btn_mas1.SetToolTip(wx.ToolTip(icons.icon.NEXT_HELP))
        self.btn_mas1.SetBackgroundColour(self.colorPixel)

        bm_before = self.icons_.to_bitmap(icons.BEFORE_PIC, 18)
        self.btn_menos1 = wx.BitmapButton(self.panel3, wx.ID_PREVIEW_PREVIOUS, bm_before)
        self.btn_menos1.SetToolTip(wx.ToolTip(icons.icon.BEFORE_HELP))
        self.btn_menos1.SetBackgroundColour(self.colorPixel)

        bm_nextNum = self.icons_.to_bitmap(icons.NEXT_NUM_PIC, 18)
        self.btn_sig = wx.BitmapButton(self.panel3, wx.ID_LAST, bm_nextNum)
        self.btn_sig.SetToolTip(wx.ToolTip(icons.icon.NEXT_NUM_HELP))
        self.btn_sig.SetBackgroundColour(self.colorPixel)

        bm_beforeNum = self.icons_.to_bitmap(icons.BEFORE_NUM_PIC, 18)
        self.btn_prev = wx.BitmapButton(self.panel3, wx.ID_FIRST, bm_beforeNum)
        self.btn_prev.SetToolTip(wx.ToolTip(icons.icon.BEFORE_NUM_HELP))
        self.btn_prev.SetBackgroundColour(self.colorPixel)

        bm_nextDetect = self.icons_.to_bitmap(icons.NEXT_DETECT_PIC, 18)
        self.btn_sigDetec = wx.BitmapButton(self.panel3, wx.ID_FORWARD, bm_nextDetect)
        self.btn_sigDetec.SetToolTip(wx.ToolTip(icons.icon.NEXT_DETECT_HELP))
        self.btn_sigDetec.SetBackgroundColour(self.colorPixel)

        bm_beforeDetect = self.icons_.to_bitmap(icons.BEFORE_DETECT_PIC, 18)
        self.btn_prevDetec = wx.BitmapButton(self.panel3, wx.ID_BACKWARD, bm_beforeDetect)
        self.btn_prevDetec.SetToolTip(wx.ToolTip(icons.icon.BEFORE_DETECT_HELP))
        self.btn_prevDetec.SetBackgroundColour(self.colorPixel)

        bm_delete = self.icons_.to_bitmap(icons.DELETE_PIC, 18)
        self.btn_eliminarArchivos = wx.BitmapButton(self.panel3, wx.ID_DELETE, bm_delete)
        self.btn_eliminarArchivos.SetToolTip(wx.ToolTip(icons.icon.DELETE_HELP))
        self.btn_eliminarArchivos.SetBackgroundColour(self.colorPixel)

        self.act_desact(act=0)

    def ordenar_boxes(self):
        ## TODO! ordenar_boxes
        # Asignamos los elementos a los layout

        self.vbox.Add(self.hbox1, 0, wx.ALIGN_CENTER)
        self.vbox.Add(self.hbox3, 0, wx.ALIGN_CENTER)

        self.hbox1.Add(self.list_logos, 0, wx.TOP, self.alto*0.0520833)
        self.hbox1.Add(self.list_rectangulos, 0, wx.TOP, self.alto*0.0520833)
        self.hbox3.Add(self.btn_add, 0, wx.CENTER)
        self.hbox3.Add(self.btn_borr, 0, wx.CENTER)
        self.hbox3.Add(self.btn_marchaAtras, 0, wx.CENTER)
        self.hbox3.Add(self.btn_borrar_todos, 0, wx.CENTER)
        self.hbox3.Add(self.btn_borrar_marcados, 0, wx.CENTER)
        self.hbox3.Add(self.btn_selection, 0, wx.CENTER)
        self.hbox3.Add(self.caja, 0, wx.CENTER)
        self.hbox3.Add(self.caja2, 0, wx.CENTER)
        self.hbox3.Add(self.CheckBox, 1, wx.CENTER)

        self.vbox2.Add(self.hbox4, 0, wx.ALIGN_CENTER)
        self.hbox4.Add(self.btn_prevDetec, 0, wx.CENTER)
        self.hbox4.Add(self.btn_prev, 0, wx.CENTER)
        self.hbox4.Add(self.btn_menos1, 0, wx.CENTER)
        self.hbox4.Add(self.btn_mas1, 0, wx.CENTER)
        self.hbox4.Add(self.btn_sig, 0, wx.CENTER)
        self.hbox4.Add(self.btn_sigDetec, 0, wx.CENTER)
        self.hbox4.Add(self.btn_eliminarArchivos, 0, wx.CENTER)

        self.panel.SetSizer(self.vbox)
        self.panel3.SetSizer(self.vbox2)

    def OnBind(self):
        ## TODO! OnBind
        # Creamos los bind para los eventos de los botones y los listbox
        mouse = wx.MouseEvent()
        self.Bind(wx.EVT_CLOSE, self.OnAppExit, id=wx.ID_CLOSE_ALL)
        self.Bind(wx.EVT_BUTTON, self.OnBorrarDibujados, id=wx.ID_ABORT)
        self.Bind(wx.EVT_BUTTON, self.OnAdd, id=wx.ID_ADD)
        self.Bind(wx.EVT_BUTTON, self.OnRemove, id=wx.ID_REMOVE)
        self.Bind(wx.EVT_BUTTON, self.OnSiguiente, id=wx.ID_LAST)
        self.Bind(wx.EVT_BUTTON, self.OnAnterior, id=wx.ID_FIRST)
        self.Bind(wx.EVT_BUTTON, self.OnSiguienteMas1, id=wx.ID_PREVIEW_NEXT)
        self.Bind(wx.EVT_BUTTON, self.OnAnteriorMenos1, id=wx.ID_PREVIEW_PREVIOUS)
        self.Bind(wx.EVT_BUTTON, self.OnAnteriorDetec, id=wx.ID_BACKWARD)
        self.Bind(wx.EVT_BUTTON, self.OnSiguienteDetec, id=wx.ID_FORWARD)
        self.Bind(wx.EVT_BUTTON, self.OnMarchaAtras, id=wx.ID_FILE2)
        self.Bind(wx.EVT_LISTBOX, self.OnSelect, id=26)
        self.Bind(wx.EVT_LISTBOX, self.OnSelectLogos, id=27)
        self.Bind(wx.EVT_TOOL, self.OnAbrirProyecto, id=1)
        self.Bind(wx.EVT_TOOL, self.OnCargarLogos, id=2)
        self.Bind(wx.EVT_TOOL, self.OnAbrirImagenes, id=3)
        self.Bind(wx.EVT_TOOL, self.OnCargarVideo, id=4)
        self.Bind(wx.EVT_TOOL, self.OnSavearchives, id=5)
        self.Bind(wx.EVT_MENU, self.OnAppExit, id=6)
        self.Bind(wx.EVT_TOOL, self.OnChangeLanguage, id=wx.ID_HELP_CONTEXT)
        self.Bind(wx.EVT_TOOL, self.OnAugmentData, id=7)
        self.Bind(wx.EVT_MENU, self.OnActivateDelFiles, id=8)
        self.Bind(wx.EVT_MENU, self.OnMarchaAtras, id=9)
        self.Bind(wx.EVT_MENU, self.OnFramesTxt, id=10)
        self.Bind(wx.EVT_TOOL, self.OnCreateXml, id=11)
        self.Bind(wx.EVT_TOOL, self.OnCreateLabels, id=12)
        self.Bind(wx.EVT_TOOL, self.OnCreateLabelsToXmls, id=13)
        self.Bind(wx.EVT_MENU, self.OnReinitApp, id=43)
        self.Bind(wx.EVT_MENU, self.OnInitializeYOLO, id=15)
        self.Bind(wx.EVT_BUTTON, self.OnBorrarTodos, id=wx.ID_FILE3)
        self.Bind(wx.EVT_BUTTON, self.OnSelection, id=wx.ID_FILE4)
        self.Bind(wx.EVT_BUTTON, self.elimArchivos, id=wx.ID_DELETE)
        self.Bind(wx.EVT_CHECKBOX, self.OnCheckBox, id=wx.ID_BOLD)

    def OnButtonKey(self):
        ## TODO! OnButtonKey
        # Creación de los funcionales con el teclado
        self.btn_eliminarArchivos.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_sig.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_prev.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_borrar_todos.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_selection.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_borr.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_borrar_marcados.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_add.Bind(wx.EVT_KEY_UP, self.key)
        self.panel2.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_prevDetec.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_sigDetec.Bind(wx.EVT_KEY_UP, self.key)
        self.list_logos.Bind(wx.EVT_KEY_UP, self.key)
        self.list_rectangulos.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_mas1.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_menos1.Bind(wx.EVT_KEY_UP, self.key)
        self.btn_marchaAtras.Bind(wx.EVT_KEY_UP, self.key)


    def act_desact(self, act=0):
        ## TODO! act_desasct
        # Función para activar o desactivar la mayoría de los botones
        if act == 0:

            self.btn_eliminarArchivos.Disable()
            self.btn_add.Disable()
            self.btn_borr.Disable()
            self.mytoolbar.EnableTool(5, False)
            self.btn_sig.Disable()
            self.btn_prev.Disable()
            self.btn_mas1.Disable()
            self.btn_menos1.Disable()
            self.btn_sigDetec.Disable()
            self.btn_prev.Disable()
            self.btn_prevDetec.Disable()
            self.btn_borrar_todos.Disable()
            self.btn_selection.Disable()
            self.btn_borrar_marcados.Disable()
            self.btn_marchaAtras.Disable()
            self.fileMenu.Enable(5, False)
            self.fileMenu.Enable(8, False)
            self.fileMenu.Enable(9, False)

        else:
            self.btn_add.Enable()
            self.btn_borr.Enable()
            self.btn_sig.Enable()
            self.btn_prev.Enable()
            self.btn_mas1.Enable()
            self.btn_menos1.Enable()
            self.btn_prevDetec.Enable()
            self.btn_sigDetec.Enable()
            self.btn_borrar_todos.Enable()
            self.btn_selection.Enable()
            self.btn_borrar_marcados.Enable()
            self.btn_marchaAtras.Enable()
            self.mytoolbar.EnableTool(3, True)
            self.mytoolbar.EnableTool(4, True)
            self.mytoolbar.EnableTool(5, True)
            self.fileMenu.Enable(3, True)
            self.fileMenu.Enable(4, True)
            self.fileMenu.Enable(5, True)
            self.fileMenu.Enable(8, True)
            self.fileMenu.Enable(9, True)
            self.fileMenu.Enable(10, True)

    def OnActivateDelFiles(self, event):
        ## TODO! OnActivateDelFiles
        if self.btn_eliminarArchivos.Enabled:
            self.btn_eliminarArchivos.Disable()
        else:
            self.btn_eliminarArchivos.Enable()

    '''INICIALIZACIÓN IRT '''

    #######   TODO! INIT BUTTONS FUNCTIONS #######
    '''FUNCIONALIDADES BOTONES INICIO IRT'''

    def key(self, event):
        ## TODO! key
        # Teclas necesarias para realizar las funciones

        if event.GetEventType() == 10053:

            if event.GetKeyCode() == 82 and self.btn_mas1.Enabled:  # e (+1)
                self.OnSiguienteMas1(event)
                return
            if event.GetKeyCode() == 69 and self.btn_menos1.Enabled:  # r (-1)
                self.OnAnteriorMenos1(event)
                return
            if event.GetKeyCode() == 9 and self.btn_add.Enabled:  # TAB (Add)
                self.OnAdd(event)
                return
            if event.GetKeyCode() == 49 and self.btn_prevDetec.Enabled:  # 1 (AnteriorDeteccion)
                self.OnAnteriorDetec(event)
                return
            if event.GetKeyCode() == 50 and self.btn_sigDetec.Enabled:  # 2 (SiguienteDeteccion)
                self.OnSiguienteDetec(event)
                event.StopPropagation()
                # return
            if event.GetKeyCode() == 51 and self.btn_prev.Enabled:  # 3(Anterior)
                self.OnAnterior(event)
                return
            if event.GetKeyCode() == 52 and self.btn_sig.Enabled:  # 4(Siguiente)
                self.OnSiguiente(event)
                return
            if event.GetKeyCode() == 66 and self.btn_borr.Enabled:  # b (Borrar)
                self.OnRemove(event)
                return
            if event.GetKeyCode() == 80 and self.mytoolbar.GetToolEnabled:  # p (GuardarArchivos)
                self.OnSavearchives(event)
                return
            if event.GetKeyCode() == 83 and self.btn_selection.Enabled:  # s (Seleccionar)
                self.OnSelection(event)
                return
            if event.GetKeyCode() == 241 and self.btn_eliminarArchivos.Enabled:  # ñ (eliminararchivos)
                self.elimArchivos(event)
                return
            if event.GetKeyCode() == 68 and self.btn_borrar_marcados.Enabled:  # d (Borrar dibujados)
                self.OnBorrarDibujados(event)
                return
            if event.GetKeyCode() == 84 and self.btn_borrar_todos.Enabled:  # d (Borrar dibujados)
                self.OnBorrarTodos(event)
                return




    def OnCargarLogos(self, event):
        ## TODO! OnCargarLogos

        '''funcion para cargar los logos con los que tiene que trabajar el programa. Al hace click en el boton cargar logos'''
        # Abrimos un fileDialog para poder seleccionar la carpeta de logos
        openFileDialog = wx.DirDialog(None, dialog.DIALOG_LOGOS_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response=openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return
        # Definimos el path a cargar con la direcciona de la carpeta escogida
        pathc = openFileDialog.GetPath() + "/*"
        # Cargamos todos los archivos de la carpeta en la variable logos
        self.logos = glob.glob(pathc)
        self.logos = sorted(self.logos)
        self.logos2 = []
        self.currentLogo = 0
        self.totalLogos = len(self.logos)
        # habilitar boton de cargar los videos y las carpetas
        self.list_logos.Clear()
        # Anyadimos los logos al listbox de logos con el nombre de la imagen
        for l in self.logos:
            pos = l.rfind("/")
            pos2 = l.rfind(".")
            self.list_logos.Append(l[pos + 1:pos2])
            self.logos2.append(l[pos + 1:pos2])

        self.estado.SetForegroundColour('black')
        self.estado.SetValue(state.LOGOS_LOAD)

    def OnAbrirProyecto(self, event):
        ## TODO! OnAbrirProyecto
        openFileDialog = wx.FileDialog(None, dialog.DIALOG_IRT_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.irt')
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        self.pathinformation = openFileDialog.GetPath()
        self.OnLoadArchives(event)
        self.act_desact(act=1)
        self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])

    def OnLoadArchives(self, event):
        with open(self.pathinformation, 'rb') as fload:
            self.information = cPickle.load(fload)
        self.nameImagesDirectory = self.information['naIDi']
        self.pathImages = os.path.join(os.path.dirname(self.pathinformation),self.nameImagesDirectory)
        self.imagenes = self.information["imag"]
        self.xmls = self.information["xmls"]
        self.path = self.information["path"]



        #images[0] = /home/bambam/image0.jpg
        #Despues de la barra
        basename = os.path.basename(self.imagenes[0])
        #basename = image0.jpg
        #Antes de la barra
        dirname = os.path.dirname(self.imagenes[0])
        generalname = os.path.dirname(dirname)
        #dirname = /home/bambam

        if dirname != self.pathImages:
            self.xmls.update_path(os.path.join(generalname,'xmls'))
            for i, imagen in enumerate(self.imagenes):

                imagen = imagen.replace(dirname, self.pathImages)
                self.imagenes[i] = imagen

        self.totalPictures = self.information["toPi"]
        self.currentPicture = self.information["cuPi"]
        self.logos2 = self.information['logos']
        self.list_logos.Set(list(self.logos2))
        self.totalLogos = self.information['toLo']
        self.currentLogo = self.information['cuLo']
        self.sizes = self.information['siIm']

    def OnAbrirImagenes(self, event):
        ## TODO! OnAbrirImagenes
        '''Funcion para cargar una carpeta con imagenes. Al hacer click en el boton cargar imagenes.'''
        # Abrimos un fileDialog para poder seleccionar la carpeta en la que esten las imagenes
        if self.list_logos.IsEmpty():
            self.OnCargarLogos(event)
            if self.list_logos.IsEmpty():
                return

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_IMAGES_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        self.act_desact(act=1)
        self.pathImages = openFileDialog.GetPath()

        pos1 = self.pathImages.rfind('/')
        pos = self.pathImages[:pos1].rfind('/')

        self.path = self.pathImages[:pos1]

        self.nameImagesDirectory = self.pathImages[pos1 + 1:]

        self.pathinformation = self.path + self.pathImages[pos:pos1] + '.irt'
        projectNameDirectory = os.path.basename(self.path)
        # Creamos la estructura de array en la que vamos a almacenar los rectangulos de detecciones

        if os.path.exists(self.pathinformation) == False:

            pathc = os.path.join(self.pathImages, "*.jpg")

            # Cargamos todos los archivos de la carpeta en la variable imagenes
            self.imagenes = glob.glob(pathc)
            self.imagenes = sorted(self.imagenes, key=self.alphanum_key)

            msg = wx.MessageDialog(None, dialog.DIALOG_UNLIKE_SIZE_IMAGES, style=wx.YES_NO)
            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL
            )
            msg.Fit()
            if msg.ShowModal() == wx.ID_YES:
                self.imagesSize()

            self.totalPictures = len(self.imagenes)
            pathXmls=os.path.join(self.path, "xmls")
            if not os.path.exists(pathXmls):
                os.mkdir(pathXmls)
            if not os.listdir(pathXmls):
                path=os.path.join(pathXmls,os.path.splitext(os.path.basename(self.imagenes[0]))[0]+'.xml')
                img = cv2.imread(self.imagenes[0])

                size = [img.shape[1], img.shape[0], img.shape[2]]

                print(projectNameDirectory)
                self.xmls=PXml.from_name(path,projectNameDirectory, size)
            else:
                self.xmls = PXml(pathXmls)
                #self.xmls.clear_files()

            self.information = dict(
                imag=self.imagenes,
                xmls=self.xmls,
                path=self.path,
                toPi=self.totalPictures,
                cuPi=self.currentPicture,
                logos=self.logos2,
                toLo=self.totalLogos,
                cuLo=self.currentLogo,
                naIDi=self.nameImagesDirectory,
                siIm=self.sizes,
            )

            with open(self.pathinformation, 'wb') as fsave:
                cPickle.dump(self.information, fsave)

        self.OnLoadArchives(event)
        self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])

    def imagesSize(self):
        for image in self.imagenes:
            img = cv2.imread(image)

            size = [img.shape[1], img.shape[0], img.shape[2]]

            self.sizes.append(size)




    def OnCargarVideo(self,event):
        ## TODO! OnCargarVideo
        '''Funcion para cargar un video y separarlo en imagenes. Al hacer click en el boton cargar video.'''
        openFileDialog = wx.DirDialog(None, dialog.DIALOG_VIDEOS_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL:
            return

        pd = wx.ProgressDialog(dialog.DIALOG_IMAGES, dialog.DIALOG_LOADING, maximum=100, parent=self,
                               style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT)
        pd.Fit()
        pathc = os.path.join(openFileDialog.GetPath(), "*.mp4")

        files = glob.glob(pathc)
        files2 = []
        for file in files:
            if ' ' in file:
                file2=file.replace(' ','')
                os.rename(file,file2)
                files2.append(file2)
            else:
                files2.append(file)

        files_dict = [{'inp': f} for f in files2]

        msg = wx.MessageDialog(None, dialog.DIALOG_NUM_IMAGES, style=wx.YES_NO)
        msg.SetYesNoCancelLabels(
            dialog.DIALOG_LABEL_YES,
            dialog.DIALOG_LABEL_NO,
            dialog.DIALOG_LABEL_CANCEL
        )
        msg.Fit()
        if msg.ShowModal() == wx.ID_YES:
            msg.Destroy()
            dlg = wx.TextEntryDialog(None, dialog.DIALOG_CHOOSE, dialog.DIALOG_NUM_IM, style=wx.OK | wx.PD_AUTO_HIDE)
            dlg.Fit()
            default = str(10000)
            dlg.SetValue(default)

            if dlg.ShowModal() == wx.ID_OK:
                if not dlg.GetValue().isdigit():
                    num = int(default) - 1
                    msg = wx.MessageDialog(None, dialog.DIALOG_NO_NUM % (dlg.GetValue(), default), style=wx.OK)
                    if msg.ShowModal():
                        msg.Destroy()

                else:
                    num = int(dlg.GetValue()) - 1
                    dlg.Destroy()

            else:
                num = None
        else:
            num = None
        self.fps = 25
        result = VideoConverter(files_dict, pd=pd, num=num,fps = self.fps).to_jpeg()

        if result == wx.ID_OK:
            self.estado.SetForegroundColour('green')
            self.estado.SetValue(state.VIDEOS_DONE)

            msg = wx.MessageDialog(None, dialog.DIALOG_USE_IMAGES, style=wx.YES_NO)
            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL
            )
            msg.Fit()
            if msg.ShowModal() == wx.ID_YES:
                msg.Destroy()
                self.OnCargarLogos(None)

                self.OnAbrirImagenes(None)

            else:
                msg.Destroy()
                return


        else:
            self.estado.SetForegroundColour('red')
            self.estado.SetValue(state.VIDEOS_NO_DONE)

    def OnAugmentData(self, event):
        ## TODO! OnAugmentData
        '''Funcion para cargar las imágenes y xmls para aumentar las muestras . Al hacer click en el boton de aumento de muestras .'''
        # Acabar la creación de xmls de los samples
        done = False

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_IMAGES_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        path = openFileDialog.GetPath()

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_COPYCROPSTO_IMAGES, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        path2 = openFileDialog.GetPath()

        dlg = wx.TextEntryDialog(None, dialog.DIALOG_NUM_TRANSFORM, dialog.DIALOG_NUM, style=wx.OK | wx.PD_AUTO_HIDE)
        dlg.Fit()
        default = str(100)
        dlg.SetValue(default)
        response= dlg.ShowModal()
        if response == wx.ID_OK or response == 5101:
            if not dlg.GetValue().isdigit():
                num_transform = int(default)
                msg = wx.MessageDialog(None, dialog.DIALOG_NO_NUM % (dlg.GetValue(), default), style=wx.OK)
                if msg.ShowModal() == wx.ID_OK:
                    msg.Destroy()

            else:
                num_transform = int(dlg.GetValue())
                dlg.Destroy()
        else:
            num_transform = int(default)


        pos1 = path.rfind('/')
        generalPath = path[:pos1]

        pathXmls = os.path.join(generalPath, 'xmls')
        xmls= PXml(pathXmls)

        pathImages = os.path.join(path, "*.jpg")
        images = glob.glob(pathImages)
        images = sorted(images, key=self.alphanum_key)

        pd = wx.ProgressDialog(dialog.DIALOG_MAKING_SAMPLES, dialog.DIALOG_LOADING, maximum=len(images), parent=self,
                               style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT | wx.PD_REMAINING_TIME)

        pd.Fit()
        if not os.path.exists(pathXmls):
            pd.Destroy()
            msg = wx.MessageDialog(None, dialog.DIALOG_NO_XMLS, style=wx.OK | wx.ICON_EXCLAMATION)
            msg.Fit()
            response = msg.ShowModal()
            if response == wx.ID_OK or response == 5101:
                msg.Destroy()
                self.estado.SetValue(state.SAMPLES_NO_DONE)
                self.estado.SetForegroundColour('red')
                return


        else:
            cropsFiles = Crops(images, xmls, generalPath, pd).cut_frames()  # filenames = paths de los crops creados

            if cropsFiles == False:
                self.estado.SetValue(state.SAMPLES_NO_DONE)
                self.estado.SetForegroundColour('red')
                return

            pd = wx.ProgressDialog(dialog.DIALOG_MAKE_TRANSFORMCROPS, dialog.DIALOG_LOADING,
                                   maximum=len(cropsFiles) * num_transform,
                                   parent=self,
                                   style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT | wx.PD_REMAINING_TIME)
            pd.Fit()

            pathCropsTrans = os.path.join(generalPath, 'cropsTrans')

            if not os.path.exists(pathCropsTrans):
                os.mkdir(pathCropsTrans)
                cropsTransFiles, names = self.cropsTrans_util(cropsFiles, num_transform, pd, pathCropsTrans)
            else:
                msg = wx.MessageDialog(None, dialog.DIALOG_USE_CROPSTRANS, style=wx.YES_NO)
                msg.Fit()
                msg.SetYesNoCancelLabels(
                    dialog.DIALOG_LABEL_YES,
                    dialog.DIALOG_LABEL_NO,
                    dialog.DIALOG_LABEL_CANCEL
                )
                response = msg.ShowModal()
                if response == wx.ID_YES:
                    msg.Destroy()
                    pd.Destroy()
                    cropsTransFiles = glob.glob(os.path.join(pathCropsTrans, '*.png'))
                    cropsTransFiles = sorted(cropsTransFiles, key=self.alphanum_key)
                    names = []
                    for cT in cropsTransFiles:

                        pos1 = cT.rfind('_')
                        path_cT = cT[:pos1]
                        pos2 = path_cT.rfind('_')
                        path_cT = path_cT[:pos2]
                        pos3 = path_cT.rfind('_')
                        name = path_cT[pos3 + 1:]
                        names.append(name)

                else:
                    msg.Destroy()
                    cropsTransFiles, names = self.cropsTrans_util(cropsFiles, num_transform, pd, pathCropsTrans)



            pathImages = os.path.join(path2, '*.jpg')
            images = glob.glob(pathImages)
            images = sorted(images, key=self.alphanum_key)

            pd = wx.ProgressDialog(dialog.DIALOG_MAKE_SAMPLES, dialog.DIALOG_LOADING,
                                   maximum=len(cropsTransFiles),
                                   parent=self,
                                   style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT | wx.PD_REMAINING_TIME)

            pd.Fit()
            done = Samples(cropsTransFiles, names, images, generalPath, pd).add_crops()

            if done == True:
                self.estado.SetValue(state.SAMPLES_DONE)
                self.estado.SetForegroundColour('red')
            else:
                self.estado.SetValue(state.SAMPLES_NO_DONE)
                self.estado.SetForegroundColour('red')

    def OnFramesTxt(self, event):

        msg = wx.MessageDialog(None, dialog.DIALOG_USE_XMLD, style=wx.YES_NO)
        msg.SetYesNoCancelLabels(
            dialog.DIALOG_LABEL_YES,
            dialog.DIALOG_LABEL_NO,
            dialog.DIALOG_LABEL_CANCEL
        )
        response = msg.ShowModal()
        if response == wx.ID_YES:
            openFileDialog = wx.FileDialog(None, dialog.DIALOG_XMLD, style=wx.DD_DEFAULT_STYLE,wildcard='*.xml')
            response = openFileDialog.ShowModal()

            if response:
                filename = openFileDialog.GetPath()
        else:
            filename=self.OnCreateXml(event)

        frameTxtPath=os.path.join(os.path.dirname(filename), 'frameTxt')
        if not os.path.exists(frameTxtPath):
            os.mkdir(frameTxtPath)

        impact_dict = defaultdict(list)

        for impact in PXml.impacts(filename):
            brand, identifier, start, end, duration = impact
            impact_dict[brand].append([identifier, int(start*1000), int(end*1000)])

        for brand in impact_dict:
            with open(os.path.join(frameTxtPath,brand+'.csv'),'w') as f:
                writer = csv.writer(f)
                writer.writerow(['id', 'Start(ms)', 'End(ms)'])
                writer.writerows(impact_dict[brand])

        self.estado.SetValue(state.TXTS_DONE)
        self.estado.SetForegroundColour('red')

    def OnCreateXml(self,event):
        if self.list_logos.IsEmpty():
            self.OnCargarLogos(event)
            if self.list_logos.IsEmpty():
                return

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_XMLS_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        xmlsPath = openFileDialog.GetPath()
        xmlsG = PXml(xmlsPath)
        colors = {brand: color for brand, color in zip(self.logos2, self.colores)}
        name = os.path.basename(os.path.dirname(xmlsPath))

        msg = wx.MessageDialog(None, dialog.DIALOG_CREATION_XMLD, style= wx.ICON_EXCLAMATION)
        response = msg.ShowModal()
        xmlsG.to_xml(os.path.join(os.path.dirname(xmlsPath),name +'.xml'), self.logos2, colors)

        self.estado.SetValue(state.XML_DONE)
        self.estado.SetForegroundColour('red')
        filename=os.path.join(os.path.dirname(xmlsPath),name +'.xml')
        msg.Destroy()
        return filename

    def OnReinitApp(self,event):
        self.OnAppExit(event)

        main.main()

    def OnCreateLabels(self,event):
        if self.list_logos.IsEmpty():
            self.OnCargarLogos(event)
            if self.list_logos.IsEmpty():
                return

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_XMLS_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        xmlsPath = openFileDialog.GetPath()

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_IMAGES_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        imagesPath = openFileDialog.GetPath()
        image_name = glob.glob(os.path.join(imagesPath,'*'))[0]

        pos1 = image_name.rfind('/')
        pos2 = image_name.rfind('_')

        projectNameDir = image_name[pos1+1:pos2]

        namesFilename=os.path.join(os.path.dirname(xmlsPath),projectNameDir + '.names')
        names_file=open(namesFilename, 'w')
        for name in self.logos2:
            names_file.write(name + '\n')

        labelsPath = os.path.join(os.path.dirname(xmlsPath),'labels')
        if not os.path.exists(labelsPath):
            os.mkdir(labelsPath)

        JPEGI_Path = os.path.join(os.path.dirname(xmlsPath), 'JPEGImages')
        if not os.path.exists(JPEGI_Path):
            os.mkdir(JPEGI_Path)


        xmls = PXml(xmlsPath)

        detect_dict = defaultdict(list)

        for bbox in xmls.bboxes():
            frame = bbox.frame
            width_image,height_image,channels_images = xmls.shape_different(frame)
            bb = self.convert((width_image, height_image), bbox.extension)
            detect_dict[bbox.frame].append(str(self.logos2.index(bbox.brand)) + " " + " ".join([str(a) for a in bb]) + os.linesep)

        pd = wx.ProgressDialog(dialog.DIALOG_MAKE_LABELS, dialog.DIALOG_LOADING,
                               maximum=len(detect_dict),
                               parent=self,
                               style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT | wx.PD_REMAINING_TIME)
        pd.Fit()

        for i, frame in enumerate(detect_dict):
            (keep, _) = pd.Update(i + 1, dialog.DIALOG_MAKING_LABELS % (str(i+1)+ '/' + str(len(detect_dict))))
            pd.Fit()
            if not keep:
                pd.Destroy()
                self.estado.SetValue(state.LABELS_NO_DONE)
                self.estado.SetForegroundColour('red')
                return

            path=os.path.join(labelsPath,projectNameDir+'_frame'+str(frame)+'.txt')
            path_images = os.path.join(JPEGI_Path,projectNameDir+'_frame'+str(frame)+'.jpg')
            if not os.path.exists(path_images):
                path_images_ori = os.path.join(imagesPath,projectNameDir+'_frame'+str(frame)+'.jpg')
                os.system('cp ' + path_images_ori + ' ' + path_images)
            with open(path,'w') as f:
                f.writelines(detect_dict[frame])

        self.estado.SetValue(state.LABELS_DONE)
        self.estado.SetForegroundColour('red')

    def OnCreateLabelsToXmls(self,event):
        if self.list_logos.IsEmpty():
            self.OnCargarLogos(event)
            if self.list_logos.IsEmpty():
                return

        openFileDialog = wx.DirDialog(None, dialog.DIALOG_LABELS_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        labelsYoloPath = openFileDialog.GetPath()
        projectNameDirectory = os.path.basename(os.path.dirname(labelsYoloPath))

        xmlsPath = labelsYoloPath.replace('labelsYolo','xmls')
        xml0Path =  os.path.join(xmlsPath,projectNameDirectory+'_frame0.xml')

        if not os.path.exists(xmlsPath):
            os.mkdir(xmlsPath)

        # c, f, cha = cv2.imread(xml0Path.replace('xmls','images').replace('.xml','.jpg')).shape
        c = 1280
        f = 720
        cha = 3
        xmls = PXml.from_name(xml0Path,projectNameDirectory,(c, f, cha))

        labelsYolo = glob.glob(os.path.join(labelsYoloPath,'*.txt'))
        labelsYolo = sorted(labelsYolo,key=self.alphanum_key)

        pd = wx.ProgressDialog(dialog.DIALOG_MAKING_XMLS, dialog.DIALOG_LOADING,
                               maximum=len(labelsYolo),
                               parent=self,
                               style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE | wx.PD_CAN_ABORT | wx.PD_REMAINING_TIME)
        pd.Fit()

        for i,label in enumerate(labelsYolo):
            pos1 = label.rfind('frame')
            pos2 = label.rfind('.txt')
            j = int(label[pos1 + 5:pos2])
            (keep, _) = pd.Update(i + 1, dialog.DIALOG_MAKE_XML % (str(i + 1) + '/' + str(len(labelsYolo))))
            pd.Fit()
            if not keep:
                pd.Destroy()
                self.estado.SetValue(state.XML_LABELS_NO_DONE)
                self.estado.SetForegroundColour('red')
                return
            lines = open(label, 'r')
            for line in lines.readlines():
                line = line.strip()
                detection = line.split('\t')
                if detection[0] in self.logos2:

                    bbox = list(map(int,detection[1:5]))
                    extent = [bbox[0],bbox[2],bbox[1],bbox[3]]
                    bbox = create_bbox(j,detection[0],extent)
                    xmls.add_bbox(j,bbox)

        self.estado.SetValue(state.XML_LABELS_DONE)
        self.estado.SetForegroundColour('red')


    def OnInitializeYOLO(self,event):
        # openFileDialog = wx.DirDialog(None, dialog.DIALOG_YOLO_DIRECTORY, style=wx.DD_DEFAULT_STYLE)
        # response = openFileDialog.ShowModal()
        # if response == wx.ID_CANCEL or response == 5101:
        #     return
        #
        # pathYOLO = openFileDialog.GetPath()
        #
        # openFileDialog = wx.FileDialog(None, dialog.DIALOG_YOLO_DATA_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.data')
        # response = openFileDialog.ShowModal()
        # if response == wx.ID_CANCEL or response == 5101:
        #     return
        #
        # pathData = openFileDialog.GetPath()
        #
        # openFileDialog = wx.FileDialog(None, dialog.DIALOG_YOLO_CFG_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.cfg')
        # response = openFileDialog.ShowModal()
        # if response == wx.ID_CANCEL or response == 5101:
        #     return
        #
        # pathCfg = openFileDialog.GetPath()
        #
        # openFileDialog = wx.FileDialog(None, dialog.DIALOG_YOLO_WEIGHTS_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.weights')
        # response = openFileDialog.ShowModal()
        # if response == wx.ID_CANCEL or response == 5101:
        #     return
        #
        # pathWeights = openFileDialog.GetPath()
        #
        openFileDialog = wx.FileDialog(None, dialog.DIALOG_YOLO_VIDEO_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.mp4')
        response = openFileDialog.ShowModal()
        if response == wx.ID_CANCEL or response == 5101:
            return

        pathVideo = openFileDialog.GetPath()
        #
        # dlg = wx.TextEntryDialog(None, dialog.DIALOG_YOLO_THRESH, dialog.DIALOG_YOLO_THRESH, style=wx.OK | wx.PD_AUTO_HIDE)
        # dlg.Fit()
        # default = str(0.4)
        # dlg.SetValue(default)
        # response= dlg.ShowModal()
        # if response == wx.ID_OK or response == 5101:
        #     if not dlg.GetValue().isdigit():
        #         thresh = default
        #     else:
        #         thresh = dlg.GetValue()
        # else:
        #     thresh = default
        #
        # openFileDialog = wx.FileDialog(None, dialog.DIALOG_YOLO_CLASSES_FILE, style=wx.DD_DEFAULT_STYLE, wildcard='*.names')
        # response = openFileDialog.ShowModal()
        # if response == wx.ID_CANCEL or response == 5101:
        #     return
        #
        # pathClasses = openFileDialog.GetPath()

        pathYOLO = '/home/stone/darknet/'
        pathData = '/home/stone/darknet/cfg/marcas2.data'
        pathCfg = '/home/stone/darknet/cfg/marcas2.cfg'
        pathWeights = '/home/stone/darknet/marcas2_200000.weights'
        #pathVideo = '/home/stone/certina_limpio.mp4'
        thresh = '0.6'
        pathClasses = '/home/stone/darknet/data/marcas2.names'

        iY.initializeYOLO(pathYOLO,pathData,pathCfg,pathWeights,pathVideo,thresh,pathClasses,self).run()


    '''FUNCIONALIDADES BOTONES INICIO IRT'''

    #######   TODO! BUTTONS FUNCTIONS #######
    '''FUNCIONALIDADES BOTONES'''

    def OnMarchaAtras(self, event):
        ## TODO! OnMarchaAtras

        for i,rect in enumerate(self.panel2.rectangulos):

            if (i + 1 == len(self.panel2.rectangulos)):
                del self.panel2.rectangulos[i]
                self.panel2.canvas.draw()
                self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])
                return

        for r in self.lastRectangles:
            xmin=r.get_x()
            ymin=r.get_y()
            w=r.get_width()
            h=r.get_height()
            xmax=xmin+w
            ymax=ymin+h
            bbox=create_bbox(self.currentPicture,self.logos2[self.currentLogo],(xmin,xmax,ymin,ymax))
            self.xmls.remove_bbox(self.currentPicture,bbox)

        self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])
        self.lastRectangles = []

    def OnBorrarDibujados(self, event):
        ## TODO! OnBorrarDibujados
        # Función para eliminar aquellos rectangulos que se quedan en pantalla
        self.listaRectangulos.clear()
        self.panel2.rectangulos.clear()
        self.panel2.rect=Rectangle((0, 0), 0, 0, facecolor='None', edgecolor=self.colores[self.currentLogo])
        self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])

    def OnCheckBox(self, event):
        ## TODO! OnCheckBox
        # Función para activar/desactivar las variables del checkbox y poder así añadir más de una detección
        self.masDetect = self.CheckBox.GetValue()
        self.panel2.activerect = self.CheckBox.GetValue()


    def elimArchivos(self, event):
        ## TODO! elimArchivos
        # Elimina archivos (Filtrado de muestra para el entrenamiento)
        if self.limiteCaja():

            self.cajavacia()

            cantidad = int(self.caja.GetValue())

            imagenActual = self.currentPicture
            for i in range(cantidad):
                self.currentPicture = imagenActual + i
                os.remove(self.imagenes[self.currentPicture])
                del self.imagenes[self.currentPicture]
                self.xmls._remove_single_xml_and_update(self.currentPicture)

                ## Añadir eliminar de self.xmls

            self.currentPicture = self.currentPicture
            self.estado.SetValue(state.REMOVE_FILES % (str(cantidad)))

            self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])
            self.panel2.rectangulos = []

    def OnAdd(self, event):
        ## TODO! OnAdd
        '''Funcion del evento click del boton Añadir. Añadimos el nuevo rectangulo a la lista de detecciones y al listbox'''
        self.autoguardado()

        imagenActual = self.currentPicture
        self.lastRectangles = []
        self.list_rectangulos.Clear()
        self.listaRectangulos = {}  # Para que funcione correctamente, será un diccionario relacionando {imagen : [rectangulos (coords)]}
        aux = []
        if (self.limiteCaja()):

            self.cajavacia()

            cantidad = int(self.caja2.GetValue())

            cantidadDet = len(self.panel2.rectangulos)

            if cantidadDet == 0:
                cantidadDet = 1

            for i in range(cantidad):
                i = i + 1
                if self.masDetect == 0:
                    aux.append(self.panel2.rect)
                    self.panel2.rectangulos = aux

                rect_Previous = []
                for rect in self.panel2.rectangulos:
                    if rect in rect_Previous:
                        break
                    else:
                        rect_Previous.append(rect)

                    xmin = rect.get_x()
                    ymin = rect.get_y()
                    w = rect.get_width()
                    h = rect.get_height()
                    if w < 0:
                        xmin = xmin+w
                        w=abs(w)
                    if h < 0:
                        ymin = ymin+h
                        h=abs(h)


                    if (xmin != 0 and ymin != 0) and abs(w) >= 5 and abs(h) >= 5:
                        if not (self.lastRectangles.__contains__(rect)):
                            self.lastRectangles.append(rect)

                        bbox=create_bbox(self.currentPicture,self.logos2[self.colores.index(to_hex(rect.get_edgecolor()).upper())],(int(xmin),int(xmin+w),int(ymin),int(ymin+h)))

                        if self.sizes != []:

                            self.xmls.add_bbox(self.currentPicture,bbox,self.sizes[self.currentPicture])
                        else:
                            self.xmls.add_bbox(self.currentPicture, bbox)

                        self.list_rectangulos.Append('{}-{}'.format(xmin,ymin))

                        self.ultimoRectangulo = rect

                if cantidad >= 2:

                    if self.currentPicture == self.totalPictures - 1:  # Volvemos al principio
                        imagenActual = 0
                    self.currentPicture = imagenActual + i


            if cantidad<=1:
                self.OnBorrarDibujados(event)
            else:
                self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])



            self.estado.SetValue(state.ADD_DETECT % str(int(self.caja.GetValue()) * cantidadDet))

            self.estado.SetForegroundColour('black')

            self.deseleccionar()

    def OnRemove(self, event):
        ## TODO! OnRemove
        # Función para eliminar una detección de la listarectangulos
        self.autoguardado()
        if (self.limiteCaja()):

            # funcion para eliminar el rectangulo seleciconado. Al hacer click en el boton borrar
            self.cajavacia()

            cantidad = int(self.caja2.GetValue())  # comprobamos el valor

            imagenActual = self.currentPicture
            can = 1
            for i in range(cantidad):
                self.currentPicture = imagenActual + i
                if self.list_rectangulos.GetSelection() != wx.NOT_FOUND:  # Si tenemos algun elemento seleccionado
                    index=self.list_rectangulos.GetSelection()
                    bboxes=self.xmls.list_bboxes(self.currentPicture)
                    self.xmls.remove_bbox(self.currentPicture,bboxes[index])
                    can += 1
                else:
                    break
                # Cambiamos el conjunto de elementos del listbox de rectangulos


            self.panel2.rect = Rectangle((0, 0), 0, 0, facecolor='None', edgecolor=self.colores[self.currentLogo])
            self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])

            self.estado.SetValue(state.REMOVE_DETECT % str(can - 1))
            self.estado.SetForegroundColour('black')
            self.deseleccionar()

    def OnSelect(self, event, index=None):
        ## TODO! OnSelect
        '''funcion para seleccionar un rectangulo y cambiarlo de color. Al seleccionar un elemento del listbox de rectangulos'''
        if index is None:
            index = event.GetSelection()

        self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture],sel=index)
        self.list_rectangulos.SetSelection(index)

    def OnSavearchives(self, event):

        # Función para guardar los archivos de control de imágenes

        self.information = dict(
            imag=self.imagenes,
            xmls=self.xmls,
            path=self.path,
            toPi=self.totalPictures,
            cuPi=self.currentPicture,
            logos=self.logos2,
            toLo=self.totalLogos,
            cuLo=self.currentLogo,
            naIDi=self.nameImagesDirectory,
            siIm= self.sizes
        )

        with open(self.pathinformation, 'wb') as f:
            cPickle.dump(self.information, f)

        self.estado.SetForegroundColour('red')
        self.estado.SetValue(state.SAVE_FILES)

        self.last_save_time = datetime.now()
        self.tiempo.SetValue(state.LAST_SAVE + self.last_save_time.strftime("%H:%M"))

    def OnBorrarTodos(self, event):
        ## TODO! OnBorrarTodos
        # Función para eliminar todas las detecciones de la imagen
        # o si se selecciona una marca se eliminan todas las detecciones de esa marca
        self.autoguardado()
        if (self.limiteCaja()):

            self.cajavacia()

            cantidad = int(self.caja2.GetValue())

            imagenActual = self.currentPicture
            for i in range(cantidad):
                self.currentPicture = imagenActual + i

                if self.list_logos.GetSelection() == -1:
                    for bbox in self.xmls.bboxes(self.currentPicture):
                        self.xmls.remove_bbox(self.currentPicture,bbox)
                else:
                    for bbox in self.xmls.bboxes(self.currentPicture):
                        if bbox.brand==self.logos2[self.currentLogo]:
                            self.xmls.remove_bbox(self.currentPicture,bbox)

            self.actualizarImagenMostrarTodos(self.imagenes[self.currentPicture])

    '''FUNCIONALIDADES BOTONES'''

    #######   TODO! MOVE IN FRAMES #######
    '''FUNCIONALIDADES MOVERSE ENTRE LOS FRAMES'''

    def OnSiguienteDetec(self, event):
        ## TODO! OnsiguieteDetec
        # Función para pasar a la siguiente detección
        self.autoguardado()
        self.currentPicture=self.xmls.next_frame(self.currentPicture)

        self.panel2.rectangulos = []
        self.OnBorrarDibujados(event)

    def OnSiguiente(self, event):
        ## TODO! OnSiguiente
        '''funcion para pasar a la siguiente imagen del array de imagenes. Al hacer click en el boton siguiente'''
        self.autoguardado()

        if (self.limiteCaja()):

            self.cajavacia()

            cantidad = int(self.caja.GetValue())

            self.currentPicture = (self.currentPicture + cantidad) % self.totalPictures

            if self.currentPicture >= self.totalPictures - 1:  # Volvemos al principio
                resta = self.currentPicture - self.totalPictures
                self.currentPicture = 0 + resta

            self.OnBorrarDibujados(event)


            self.deseleccionar()
            self.panel2.rectangulos = []

            event.Skip()

    def OnSiguienteMas1(self, event):
        ## TODO! OnSiguienteMas1
        '''funcion para pasar a la siguiente imagen del array de imagenes. Al hacer click en el boton +1'''

        self.autoguardado()

        if self.currentPicture == self.totalPictures - 1:  # Volvemos al principio
            self.currentPicture = 0
        else:
            self.currentPicture += 1  # Pasamos a la siguiente
        self.OnBorrarDibujados(event)


        self.deseleccionar()
        self.panel2.rectangulos = []

        event.Skip()

    def OnAnteriorDetec(self, event):
        ## TODO! OnAnteriorDetec
        # Función para ir a la anterior detección
        self.autoguardado()
        self.currentPicture = self.xmls.prev_frame(self.currentPicture)
        self.panel2.rectangulos = []
        self.OnBorrarDibujados(event)

    def OnAnterior(self, event):
        ## TODO! OnAnterior
        '''funcion para pasar a la anterior imagen del array de imagenes. Al hacer click en el boton anterior'''
        self.autoguardado()

        cantidad = int(self.caja.GetValue())

        self.currentPicture = self.currentPicture - (cantidad % self.totalPictures)

        if self.currentPicture < 0:
            self.currentPicture = 0

        self.OnBorrarDibujados(event)


        self.deseleccionar()

        self.panel2.rectangulos = []

        event.Skip()

    def OnAnteriorMenos1(self, event):
        ## TODO! OnAnteriorMenos1
        '''funcion para pasar a la anterior imagen del array de imagenes. Al hacer click en el boton -1 '''

        self.autoguardado()

        self.currentPicture = self.currentPicture - 1

        if self.currentPicture < 0:
            self.currentPicture = 0

        self.OnBorrarDibujados(event)


        self.deseleccionar()
        self.panel2.rectangulos = []

        event.Skip()

    '''FUNCIONALIDADES MOVERSE ENTRE LOS FRAMES'''

    #######   TODO! GENERAL FUNCTIONS #######
    '''FUNCIONALIDADES GENERALES'''


    def actualizarImagenMostrarTodos(self, path, sel=None):
        ## TODO! actualizarImagenMostrarTodos
        # Función para mostrar la imagen actual y todas sus detecciones

        self.list_rectangulos.Clear()
        self.listaRectangulosLogos = []


        self.panel2.axes.clear()
        self.panel2.axes.set_axis_off()
        self.panel2.setImage(path)

        if self.masDetect == 0:
            self.panel2.axes.add_patch(self.panel2.rect)
        else:
            for rect in self.panel2.rectangulos:
                self.panel2.axes.add_patch(rect)

        for logo in self.listaRectangulosLogos:
            if logo.creado:
                self.listaRectangulosLogos.remove(logo)
                self.panel2.rectangulos.remove(logo)

        bboxes_list = self.xmls.list_bboxes(self.currentPicture)
        for i, bbox in enumerate(bboxes_list):

            index_logo = self.logos2.index(bbox.brand)

            (xmin,ymin), w, h = bbox.to_size()

            if i == sel:
                color = 'red'

            else:
                color = self.colores[index_logo]

            rect = Rectangle((xmin, ymin), w, h, facecolor='None', edgecolor=color)

            self.listaRectangulosLogos.append(LR.LogoRectangle(rect, self.panel2,self.colores[index_logo],
                                                                logo=index_logo ,listLogos=self.logos2))

            self.listaRectangulosLogos = self.borrarRepetidosListaRectangulosPorCoord(self.listaRectangulosLogos)
            self.panel2.SetListaRectangulosLogos(self.listaRectangulosLogos)


            #
            # if isinstance(bbox_extent, list):
            #     xminV,yminV,xmin,ymin=bbox_extent
            #
            #     self.currentLogo = self.colores.index()
            #
            #     bbox=create_bbox(self.currentPicture,self.logos2[self.currentLogo],(xminV,xminV+w,yminV,yminV+h))
            #     self.xmls.remove_bbox(self.currentPicture,bbox)
            #
            #     bbox=create_bbox(self.currentPicture,self.logos2[self.currentLogo],(xmin,xmin+w,ymin,ymin+h))
            #     self.xmls.add_bbox(self.currentPicture,bbox)

            self.list_rectangulos.Append('{}-{}'.format(xmin, ymin))

        bboxes_tamanyo = self.panel2.iniciarMover(bboxes_list)
        #if(bboxes_tamanyo is not None):
        #   self.panel2.change_bbox(bboxes_tamanyo[0], bboxes_tamanyo[1])
        #   bboxes_tamanyo = None

        self.panel2.canvas.draw()

        self.estado.SetForegroundColour('black')
        self.estado.SetValue(state.CURRENT % str(self.currentPicture))

    def deseleccionar(self):
        ## TODO! deseleccionar
        # Función para que no se quede ningún logo ni ningúna detección seleccionadas
        if self.list_rectangulos.GetSelection() != -1:
            self.list_rectangulos.Deselect(self.list_rectangulos.GetSelection())
        if self.list_logos.GetSelection() != -1:
            self.list_logos.Deselect(self.list_logos.GetSelection())

    def autoguardado(self):
        ## TODO! autoguardado
        # Función de autoguardado cada 30 minutos
        if (datetime.now() - self.last_save_time).total_seconds() >= 15 * 60:
            self.OnSavearchives(self)
            self.last_save_time = datetime.now()

    def OnSelection(self, event):
        ## TODO! OnSelection
        self.autoguardado()

        # Función para seleccionar una detección

        minx = self.panel2.rect.get_x()  # obtenemos las coordenadas del rectangulo
        miny = self.panel2.rect.get_y()

        if minx is not 0 and miny is not 0:
            for i,bbox in enumerate(self.xmls.bboxes(self.currentPicture)):
                if bbox.is_inside(Point(minx, miny)):
                    self.OnSelectLogos(event, index=self.logos2.index(bbox.brand))
                    self.OnSelect(event, index=i)
                    self.estado.SetForegroundColour('black')
                    self.estado.SetValue(state.SELECT_LOGO)
                    return

    def OnSelectLogos(self, event, index=-1):
        ## TODO! OnSelectLogos
        '''funcion para seleccionar un logo. Al hacer click en el listbox de logos'''
        # Cambiamos el color de los logos

        if index == -1:
            self.currentLogo = event.GetSelection()
        else:
            self.currentLogo = index

        self.panel2.rect.set_edgecolor(self.colores[self.currentLogo])
        self.list_logos.SetSelection(self.currentLogo)

        # actualizamos la imagen mostrando las detecciones del logo seleciconado

    def OnAppExit(self, event):
        ## TODO! OnAppExit

        if self.imagenes and (datetime.now() - self.last_save_time).total_seconds() > 5 * 60:

            msg = wx.MessageDialog(None, dialog.DIALOG_EXIT_SAVE, style=wx.CANCEL | wx.YES_NO | wx.ICON_EXCLAMATION)

            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL
            )
            response = msg.ShowModal()
            if response == wx.ID_YES:
                self.OnSavearchives(event)
                self.Destroy()
            elif response == wx.ID_NO:
                self.Destroy()

        else:
            self.Destroy()

    def cajavacia(self):
        ## TODO! cajavacia
        # Sirve para que la caja nunca se quede vacía
        if len(self.caja.GetValue()) == 0:
            self.caja.SetValue('1')
        if len(self.caja2.GetValue()) == 0:
            self.caja.SetValue('1')

    def limiteCaja(self):
        ## TODO! limiteCaja
        # Por si el numero introducido en la caja es demasiado grande:
        aux = False
        if int(self.caja.GetValue()) > 500 or int(self.caja2.GetValue()) > 500:
            msg = wx.MessageDialog(None, dialog.DIALOG_LABEL_LIMIT, style=wx.YES_NO)
            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL,
            )
            if msg.ShowModal() == wx.ID_YES:
                aux = True
        else:
            aux = True
        return aux

    def indent(self, elem, level=0):
        ## TODO! indent
        '''funcion auxiliar para indentar el xml'''
        i = "\n" + level * "\t"
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "\t"
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def borrarRepetidosListaRectangulosPorCoord(self, lista):
        ## TODO! borrarRepetidosListaRectuangulosPorCoord
        auX = 0
        auY = 0
        for l in lista:
            if (l.x == auX) and (l.y == auY):
                lista.remove(l)

            auX = l.x
            auY = l.y

        return lista

    def OnChangeLanguage(self, event):
        ## TODO! OnChangeLanguage

        if self.language == 'es-ES':
            language = 'en-GB'
        if self.language == 'en-GB':
            language = 'es-ES'

        dialog.change_language(language, child=True)

        self.estado.SetValue(state.CHANGE_LANGUAGE)
        self.estado.SetForegroundColour('red')

        self.tiempo.SetValue(state.LAST_SAVE + self.last_save_time.strftime("%H:%M"))

        self.language = language
        self.reinit_toolbar()
        self.reinit_funcionales()
        self.reinit_menubar()

    def reinit_funcionales(self):
        ## TODO! reinit_funcionales
        # Definimos Todos los botones, listas, checkboxs y textos
        # PANEL SUPERIOR

        self.list_rectangulos.SetToolTip(wx.ToolTip(icons.icon.LIST_DETECT_HELP))

        self.list_logos.SetToolTip(wx.ToolTip(icons.icon.LIST_LOGOS_HELP))

        self.CheckBox.SetToolTip(wx.ToolTip(icons.icon.MORE_DET_HELP))

        self.checkBoxAtOnce.SetToolTip(wx.ToolTip(icons.icon.DET_AT_ONCE_HELP))

        self.btn_add.SetToolTip(wx.ToolTip(icons.icon.ADD_HELP))

        self.btn_borr.SetToolTip(wx.ToolTip(icons.icon.REMOVE_HELP))

        self.btn_marchaAtras.SetToolTip(wx.ToolTip(icons.icon.UNDO_HELP))

        self.btn_borrar_todos.SetToolTip(wx.ToolTip(icons.icon.REMOVE_ALL_HELP))

        self.btn_borrar_marcados.SetToolTip(wx.ToolTip(icons.icon.DELETE_DRAW_HELP))

        self.btn_selection.SetToolTip(wx.ToolTip(icons.icon.SELECT_HELP))

        self.caja.SetToolTip(wx.ToolTip(icons.icon.BOX_HELP))
        self.caja2.SetToolTip(wx.ToolTip(icons.icon.BOX_HELP))


        # PANEL INFERIOR

        self.btn_mas1.SetToolTip(wx.ToolTip(icons.icon.NEXT_HELP))

        self.btn_menos1.SetToolTip(wx.ToolTip(icons.icon.BEFORE_HELP))

        self.btn_sig.SetToolTip(wx.ToolTip(icons.icon.NEXT_NUM_HELP))

        self.btn_prev.SetToolTip(wx.ToolTip(icons.icon.BEFORE_NUM_HELP))

        self.btn_sigDetec.SetToolTip(wx.ToolTip(icons.icon.NEXT_DETECT_HELP))

        self.btn_prevDetec.SetToolTip(wx.ToolTip(icons.icon.BEFORE_DETECT_HELP))

        self.btn_eliminarArchivos.SetToolTip(wx.ToolTip(icons.icon.DELETE_HELP))


    def cropsTrans_util(self, cropsFiles, num_transform, pd, pathCropsTrans):
        cropsTransFiles = []
        names = []
        for i, augmented_image in enumerate(aum.augment(cropsFiles, num_transform)):

            iFile = int(np.floor(i / num_transform))

            str1 = str((i % num_transform) + 1) + '/' + str(num_transform)
            str2 = str(iFile + 1) + '/' + str(len(cropsFiles))

            (keep, _) = pd.Update(i + 1, dialog.DIALOG_MAKE_TRANSFORMCROP % (str1, str2))
            pd.Fit()
            if not keep:
                pd.Destroy()
                self.estado.SetValue(state.SAMPLES_NO_DONE)
                self.estado.SetForegroundColour('red')
                return

            filename = cropsFiles[iFile]
            pos1 = filename.rfind('/')
            pos2 = filename.rfind('.')
            filename = filename[pos1 + 1:pos2]

            j = i % num_transform
            cropTransFile = os.path.join(pathCropsTrans, filename + '_' + str(j) + '.png')

            pos1 = filename.rfind('_')
            path1 = filename[0:pos1]
            pos2 = path1.rfind('_')
            name = path1[pos2 + 1:]

            r_channel, g_channel, b_channel = cv2.split(augmented_image)

            a_channel = np.where(augmented_image < 10, 0, 255).astype('uint8')

            a_channel = cv2.cvtColor(a_channel, cv2.COLOR_RGB2GRAY)

            img_RGBA = cv2.merge((r_channel, g_channel, b_channel, a_channel))

            cv2.imwrite(cropTransFile, img_RGBA)
            cropsTransFiles.append(cropTransFile)
            names.append(name)

        return cropsTransFiles, names

    def change_bbox(self, old_bbox, new_bbox):
        if old_bbox.frame != new_bbox.frame:
            raise ValueError('Not same frames to change')
        frame = old_bbox.frame
        self.xmls.add_bbox(frame, new_bbox)
        self.xmls.remove_bbox(frame, old_bbox)

    def convert(self,size, box):
        dw = 1. / (size[0])
        dh = 1. / (size[1])
        x = (box[0] + box[1]) / 2.0 - 1
        y = (box[2] + box[3]) / 2.0 - 1
        w = box[1] - box[0]
        h = box[3] - box[2]
        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh
        return (x, y, w, h)
    '''FUNCIONALIDADES GENERALES'''

    def resolucionPantalla(self):

        # Se define un par de variables con los comandos a pasar:
        cmd = ['xrandr']
        cmd2 = ['grep', '*']

        # Se ejecuta el comando xrandr y luego se abre una tuberia.
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)

        # Se ejecuta el segundo comando
        p2 = subprocess.Popen(cmd2, stdin=p.stdout, stdout=subprocess.PIPE)

        # Se cierra la salida estandar.
        p.stdout.close()

        # Obteccion de la resolucion
        resolution_string, junk = p2.communicate()
        resolution = resolution_string.split()[0]
        width, height = resolution.split(str.encode("x"))

        return (1920, 1080)
