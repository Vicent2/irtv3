import matplotlib  # librearia utilizada para crear figuras y subfiguras en python
import matplotlib.pyplot as plt  # libreria para crear las figuras y subfiguras de la aplicacion
import wx  # wx es la libreria de wxpyhton para crear interfaces de usuario en python (es parecida a tkinter)
from PIL import Image  # PIL python image library para trabajar con imagenes en python
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigureCanvas  # para el canvas donde ponemos la imagen y pintamos los rectangulos
from matplotlib.patches import Rectangle  # para dibujar los rectangulos

from archivos.MoverRectangulo import *

matplotlib.use('WXAgg')


class RectangleSelectImagePanel(wx.Panel):
    ''' Panel that contains an image that allows the users to select an area of the image with the mouse. The user clicks and
    holds the mouse to create a dotted rectangle, and when the mouse is released the rectangles origin, width and height can be
    read. The dimensions of these readings are always relative to the original image, so even if the image is scaled up larger
    to fit the panel the measurements will always refer to the original image.'''

    def __init__(self, parent, x, y, width, heigth, change_bbox=None, pathToImage=None, listRectangulosLogos=None):
        ''' Initialise the panel. Setting an initial image is optional.'''
        # Initialise the parent
        wx.Panel.__init__(self, parent, pos=(x, y), size=(width, heigth))

        # Intitialise the matplotlib figure
        self.grayAPP = (242 / 255, 241 / 255, 240 / 255)
        self.figure = plt.figure(num=1,figsize=(width,heigth),facecolor=self.grayAPP)

        # Create an axes, turn off the labels and add them to the figure

        self.axes = plt.Axes(self.figure, [0, 0, 1, 1])
        self.axes.set_axis_off()
        self.figure.add_axes(self.axes)

        # Add the figure to the wxFigureCanvas
        self.canvas = FigureCanvas(self, -1, self.figure)

        # Inicializo la lista de todos los posibles rectangulos
        self.rectangulos = []
        self.activerect = 0
        self.moverTodos = False

        if listRectangulosLogos:
            self.listaRectangulosLogos = listRectangulosLogos
        else:
            self.listaRectangulosLogos = []

        self.bboxes = None

        # Propiedades del rectangulo que contiene el nombre del logo cuando se anyade el rectangulo



        # Initialise the rectangle
        self.rect = Rectangle((0, 0), 0, 0, facecolor='None', edgecolor='#33FF66')
        self.x0 = None
        self.y0 = None
        self.x1 = None
        self.y1 = None
        self.axes.add_patch(self.rect)

        # Sizer to contain the canvas
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.canvas, 1, wx.ALL)
        self.sizer2.Add(self.sizer, 1, wx.ALL)
        self.SetSizer(self.sizer2)
        # self.Fit()

        # Connect the mouse events to their relevant callbacks
        self.canvas.mpl_connect('button_press_event', self._onPress)
        self.canvas.mpl_connect('button_release_event', self._onRelease)
        self.canvas.mpl_connect('motion_notify_event', self._onMotion)

        self.mr = None

        # Lock to stop the motion event from behaving badly when the mouse isn't pressed
        self.pressed = False
        self.pressed_movement = False

        #Variable para comprobar si hemos pulsado con objetivo
        # de cambiar el tamaño del rectangulo.
        self.pressed_tamanyo = False

        # If there is an initial image, display it on the figure
        if pathToImage is not None:
            self.setImage(pathToImage)

        self.change_bbox = change_bbox
        self.rects = []
        self.old_bboxes = []
        self.pressed_point_origin = ()
        plt.close(self.figure)

    def _onPress(self, event):


        ''' Callback to handle the mouse being clicked and held over the canvas'''

        # Check the mouse press was actually on the canvas
        if event.xdata is not None and event.ydata is not None:
            # Upon initial press of the mouse record the origin and record the mouse as pressed
            if event.button == 1:
                self.pressed = True

                self.x0 = int(event.xdata)
                self.y0 = int(event.ydata)

                for logo in self.listaRectangulosLogos:

                    minimo = logo.y + logo.height
                    maximo = logo.x + logo.width

                    # if ((maximo >= self.x0 >= logo.x) and (logo.y <= self.y0 <= minimo)) or self.moverTodos:
                    #
                    #     self.pressed = False
                    #
                    # else:
                    #
                    #     self.rect.set_linestyle('dashed')
                    #     self.rect.set_linewidth(2)

        self.canvas.flush_events()


    def _onRelease(self, event):
        '''Callback to handle the mouse being released over the canvas'''

        # Check that the mouse was actually pressed on the canvas to begin with and this isn't a rouge mouse
        # release event that started somewhere else
        if self.pressed:

            self.rect.set_visible(False)
            # Upon release draw the rectangle as a solid rectangle
            self.pressed = False

            self.rect.set_linestyle("solid")
            self.rect.set_linewidth(2)

            # self.canvas.draw()

            # Creo aqui una instancia rectangulo para que me deje dibujar mas de uno en el mismo canvas
            if self.activerect == 1:
                rectangulo = Rectangle((0, 0), 0, 0, facecolor='None', edgecolor=self.rect.get_edgecolor())
                rectangulo.set_linewidth(2)

            # Check the mouse was released on the canvas, and if it wasn't then just leave the width and
            # height as the last valuess set by the motion event
            if event.xdata is not None and event.ydata is not None:
                self.x1 = int(event.xdata)
                self.y1 = int(event.ydata)

            # Set the width and height and origin of the bounding rectangle
            self.boundingRectWidth = int(self.x1 - self.x0)
            self.boundingRectHeight = int(self.y1 - self.y0)
            self.boundingRectOrigin = (int(self.x0), int(self.y0))

            # Draw the bounding rectangle
            if self.activerect == 1:
                rectangulo.set_width(int(self.boundingRectWidth))
                rectangulo.set_height(int(self.boundingRectHeight))
                rectangulo.set_xy((int(self.x0), int(self.y0)))

                # Para tener las mismas coordenadas en ambas variables
                self.rect.set_width(int(self.boundingRectWidth))
                self.rect.set_height(int(self.boundingRectHeight))
                self.rect.set_xy((int(self.x0), int(self.y0)))
                # Anyado el nuevo rectangulo a la lista
                self.rectangulos.append(rectangulo)
                self.axes.add_patch(rectangulo)

            else:
                self.rect.set_width(int(self.boundingRectWidth))
                self.rect.set_height(int(self.boundingRectHeight))
                self.rect.set_xy((int(self.x0), int(self.y0)))
                self.rect.set_visible(True)
                self.axes.add_patch(self.rect)


        else:
            if self.rect in self.rectangulos:
                self.rectangulos.remove(self.rect)
        self.axes.figure.canvas.draw()

    def _onMotion(self, event):
        '''Callback to handle the motion event created by the mouse moving over the canvas'''

        # If the mouse has been pressed draw an updated rectangle when the mouse is moved so
        # the user can see what the current selection is
        if self.pressed:

            self.rect.set_visible(True)
            # Check the mouse was released on the canvas, and if it wasn't then just leave the width and
            # height as the last valuess set by the motion event
            if event.xdata is not None and event.ydata is not None:
                self.x1 = int(event.xdata)
                self.y1 = int(event.ydata)

            # Set the width and height and draw the rectangle
            self.rect.set_width(int(self.x1 - self.x0))
            self.rect.set_height(int(self.y1 - self.y0))
            self.rect.set_xy((int(self.x0), int(self.y0)))
            self.axes.add_patch(self.rect)
            self.axes.figure.canvas.draw()

        else:
            if self.rect in self.rectangulos:
                self.rectangulos.remove(self.rect)

    def _onPressMovement(self, event):


        if event.xdata is not None and event.ydata is not None:
            pressed_point = Point(event.xdata, event.ydata)

            for bbox, rect in zip(self.bboxes, self.listaRectangulosLogos):
                xmin, xmax, ymax, ymin = bbox.xmin, bbox.xmax, bbox.ymin, bbox.ymin - rect.height  # Default
                #rectBbox = create_bbox(bbox.frame, bbox.brand, (xmin, xmax, ymin, bbox.ymax))
                text_Bbox = create_bbox(bbox.frame, bbox.brand, (int(xmin), int(xmax), int(ymin), int(ymax)))
                if text_Bbox.is_inside(pressed_point) and event.button==1:
                    self.old_bbox = bbox
                    self._save_offset(pressed_point)
                    self.pressed_movement = True
                    self.rectangulo_movement = rect
                    self.rectangulo_movement.rectangulo.set_linestyle('dashed')
                else:
                    if event.button==3:
                        self.pressed_point_origin = Point(event.xdata, event.ydata)
                        self.old_bboxes.append(bbox)
                        self.pressed = False
                        rect.rectangulo.set_linestyle('dashed')
                        self.rects.append(rect)

                # else:
                #     #Compruebo que hemos pulsado dentro de la deteccion.
                #     if rectBbox.is_inside(pressed_point):
                #
                #         self.pressed = False #Esto es para evitar que me cree un rectangulo nuevo.
                #
                #         self.old_bbox_tamanyo = bbox #Guardo el rectangulo viejo para hacer luego el change_bbox
                #         self.pressed_tamanyo = True #Hemos pulsado dentro, asique pasa a ser true.
                #         self.rectangulo = rect
                #         #Cambio el tamaño al rectangulo a donde he pulsado con el cursor.
                #         self.rectangulo.set_height(event.ydata - self.rectangulo.y)
                #         self.rectangulo.set_width(event.xdata - self.rectangulo.x)


    def _onMotionMovement(self, event):
        if event.xdata is not None and event.ydata is not None:

            if self.pressed_movement and event.button == 1:

                w, h = self.rectangulo_movement.get_width(), self.rectangulo_movement.get_height()

                x, y = self._pressed_to_absolute(Point(int(event.xdata), int(event.ydata)))

                if w < x + w < self.imageSize[0] and h < y + h < self.imageSize[1]:
                    self.rectangulo_movement.set_xy((x, y))
                self.axes.add_patch(self.rectangulo_movement.rectangulo)
                self.axes.add_patch(self.rectangulo_movement.rn)
                self.axes.figure.canvas.draw()

            else:
                if event.button ==3:
                    self.prueba = True
                    x_aux, y_aux = Point(int(event.xdata), int(event.ydata))
                    if len(self.pressed_point_origin) == 2:
                        x = x_aux - self.pressed_point_origin[0]
                        y = y_aux - self.pressed_point_origin[1]

                    for rect in self.rects:
                        rect.set_xy((int(x + rect.x),int(y + rect.y)))
                        self.axes.add_patch(rect.rectangulo)
                        self.axes.add_patch(rect.rn)
                        self.axes.figure.canvas.draw()

            # if self.pressed_tamanyo: #Si quiero mover el rectangulo...:
            #
            #     #Me guardo el tamaño que le voy dando en w(anchura) y h(altura)
            #     w, h = Point(int(event.xdata - self.rectangulo.x), int(event.ydata - self.rectangulo.y-20))
            #
            #     self.rectangulo.set_height(h)
            #     self.rectangulo.set_width(w)
            #
            #     #Añado el rectangulo y el LogoRectangulo a axes, y lo dibujo.
            #     self.axes.add_patch(self.rectangulo.rectangulo)
            #     self.axes.add_patch(self.rectangulo.rn)
            #
            #     self.axes.figure.canvas.draw()

    def _onReleaseMovement(self, event):
        if event.xdata is not None and event.ydata is not None:

            if self.pressed_movement and event.button == 1:
                w, h = self.rectangulo_movement.get_width(), self.rectangulo_movement.get_height()
                x, y = self._pressed_to_absolute(Point(int(event.xdata), int(event.ydata)))


                # if w < x + w < self.imageSize[0] and h < y + h < self.imageSize[1]:
                #
                #     self.pressed_movement = False
                #     self.axes.figure.canvas.draw()
                #     new_bbox = create_bbox(self.old_bbox.frame, self.old_bbox.brand, (int(x), int(x+w), int(y), int(y+h)))
                #     self.change_bbox(self.old_bbox, new_bbox)
                #     self._remove_offset()
            else:
                if event.button ==3:


                    x_aux, y_aux = Point(int(event.xdata), int(event.ydata))

                    if len(self.pressed_point_origin) == 2:
                        x = x_aux - self.pressed_point_origin[0]
                        y = y_aux - self.pressed_point_origin[1]

                    i = 0
                    for old in self.old_bboxes:
                        w, h = self.rects[i].get_width(), self.rects[i].get_height()

                        new_bbox = create_bbox(old.frame, old.brand, (int(x + self.rects[i].x),int(x + self.rects[i].x+w), int(y + self.rects[i].y), int(y + self.rects[i].y+h)))
                        self.change_bbox(old, new_bbox)
                        i +=1
                    self.old_bboxes = list()
                    self.rects = list()

                        # if self.pressed_tamanyo: #Si estoy cambiando el tamaño...
            #
            #     w, h = Point(int(event.xdata - self.rectangulo.x), int(event.ydata - self.rectangulo.y-20))
            #
            #     #Igual que para mover el bbox, me creo una bbox nueva.
            #     new_bbox_tamanyo = create_bbox(self.old_bbox_tamanyo.frame, self.old_bbox_tamanyo.brand,
            #                                        (self.rectangulo.x, self.rectangulo.x + w, self.rectangulo.y, self.rectangulo.y + h))
            #
            #     self.pressed_tamanyo = False #Ya hemos soltado así que no queremos que se nos mueva mas.
            #
            #     self.change_bbox(self.old_bbox_tamanyo, new_bbox_tamanyo) #Cambio al nuevo bbox.
            #
            #     #Lo dibujo.
            #     self.axes.figure.canvas.draw()
            #     self._remove_offset()


    def setImage(self, pathToImage):
        '''Sets the background image of the canvas'''

        # Load the image into matplotlib and PIL
        image = matplotlib.image.imread(pathToImage)
        imPIL = Image.open(pathToImage)

        # Save the image's dimensions from PIL
        self.imageSize = imPIL.size

        # Add the image to the figure and redraw the canvas. Also ensure the aspect ratio of the image is retained.
        self.axes.imshow(image, aspect='equal')
        self.canvas.draw()

    def SetListaRectangulosLogos(self, lista):
        self.listaRectangulosLogos = lista

    def iniciarMover(self, bboxes):
        self.bboxes = bboxes
        self.canvas.mpl_connect('button_press_event', self._onPressMovement)
        self.canvas.mpl_connect('button_release_event', self._onReleaseMovement)
        self.canvas.mpl_connect('motion_notify_event', self._onMotionMovement)
        #return self.bboxes_tamanyo


    def _pressed_to_absolute(self, pressed):
        return Point(int(pressed.x - self.x_offset), int(pressed.y - self.y_offset))

    def _save_offset(self, pressed):
        self.x_offset = pressed.x - self.old_bbox.xmin
        self.y_offset = pressed.y - self.old_bbox.ymin

    def _remove_offset(self):
        self.x_offset, self.y_offset = None, None
