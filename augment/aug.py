from imgaug import augmenters as iaa

# random example images

# Sometimes(0.5, ...) applies the given augmenter in 50% of all cases,
# e.g. Sometimes(0.5, GaussianBlur(0.3)) would blur roughly every second image.
sometimes = lambda aug: iaa.Sometimes(0.5, aug)

# Define our sequence of augmentation steps that will be applied to every image
# All augmenters with per_channel=0.5 will sample one value _per image_
# in 50% of all cases. In all other cases they will sample new values
# _per channel_.
DEFAULT_SEQUENTIAl = iaa.Sequential(
    [
        # execute 0 to 5 of the following (less important) augmenters per image
        # don't execute all of them, as that would often be way too strong

        iaa.SomeOf((1, 3),
                   [
                       sometimes(iaa.Superpixels(p_replace=(0, 0.15), n_segments=(150, 400))),
                       # convert images into their superpixel representation
                       iaa.OneOf([
                           iaa.GaussianBlur((1.5, 1.75)),  # blur images with a sigma between 1.5 and 1.75
                           iaa.AverageBlur(k=(3, 5)),  # blur image using local means with kernel sizes between 2 and 5
                           iaa.MedianBlur(k=(3, 5)),  # blur image using local medians with kernel sizes between 2 and 5
                       ]),
                       iaa.Emboss(alpha=(0, 0.25), strength=1),  # emboss images
                       iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.02 * 255), per_channel=0.5),
                       # add gaussian noise to images

                       iaa.AddToHueAndSaturation((-30, 30)),  # change hue and saturation
                       iaa.ContrastNormalization((0.5, 2.0)),  # improve or worsen the contrast

                       # move pixels locally around (with random strengths)
                       # sometimes move parts of the image around

                   ],
                   random_order=True
                   ),
        iaa.SomeOf((1, 4),
                   [
                       sometimes(iaa.Affine(rotate=(-45, 45),
                                            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},
                                            translate_percent={"x": (-0.2, 0.2), "y": (-0.2, 0.2)},
                                            shear=(-16, 16),
                                            )),
                       sometimes(iaa.ElasticTransformation(alpha=(0.5, 3.5), sigma=0.25)),
                       sometimes(iaa.PiecewiseAffine(scale=(0.01, 0.05))),
                       sometimes(iaa.PerspectiveTransform(scale=(0.01, 0.1))),
                       sometimes(iaa.CoarseDropout((0.1, 0.2), size_percent=(0.01, 0.02))),
                   ],
                   random_order=True, )

    ],

    random_order=False)
# ai = seq.augment_image(im)
