import os
import xml.etree.ElementTree as ET
import numpy as np
import cv2
import wx
from strings.dialog import dialog
import glob
import re
from scipy.stats import mode


class Crops(object):
    def __init__(self, images, xmls, path, pd):
        self.images = images
        self.xmls = xmls
        self.path = os.path.join(path, 'crops')
        self.pd = pd
        self.len = len(images)

    @staticmethod
    def _crop(image, values):
        xmin, xmax, ymin, ymax = values
        if xmin >= xmax or ymin >= ymax:
            return None
        else:
            return image[ymin:ymax, xmin:xmax]

    def cut_detect(self):
        files = []
        for i, im in enumerate(self.images):
            (keep, _) = self.pd.Update(i + 1, dialog.DIALOG_MAKING_CROPS % (str(i + 1) + '/' + str(self.len)))
            self.pd.Fit()
            if not keep:
                self.pd.Destroy()
                return False
            pos1 = im.rfind('/')
            pos2 = im.rfind('.')
            nImage = im[pos1 + 1:pos2]
            image = cv2.imread(im)

            clases = []
            cont = []
            for bbox in self.xmls.bboxes(i):
                name=bbox.brand
                if not name in clases:
                    clases.append(name)
                    cont.append(0)

                index = clases.index(name)

                crop = self._crop(image, (bbox.xmin, bbox.xmax, bbox.ymin, bbox.ymax))

                if crop is None:
                    continue

                crop = np.where(crop[:, :, :] < 10, 10, crop[:, :, :])

                filename = os.path.join(self.path, (nImage + '_' + name + '_' + str(cont[index]) + '.jpg'))

                cv2.imwrite(filename, crop)
                files.append(filename)
                cont[index] += 1

        return files

    def cut_frames(self):
        if not os.path.exists(self.path):
            os.mkdir(self.path)
            files = self.cut_detect()
            return files
        else:
            msg = wx.MessageDialog(None, dialog.DIALOG_USE_CROPS, style=wx.YES_NO)

            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL
            )
            response = msg.ShowModal()
            if response == wx.ID_NO:
                msg.Destroy()
                files = self.cut_detect()
                return files

            elif response == wx.ID_YES:
                self.pd.Destroy()
                msg.Destroy()
                files = glob.glob(os.path.join(self.path, '*.jpg'))
                convert = lambda text: int(text) if text.isdigit() else text
                alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
                files = sorted(files, key=alphanum_key)
                return files
