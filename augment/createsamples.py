import os
import xml.etree.ElementTree as ET
import wx
import cv2
import numpy as np
import glob
from strings.dialog import dialog
from pixmlabs import PiXMLabs as PXml
from pixmlabs import create_bbox
import re

class Samples(object):
    def __init__(self, cropsTransFiles, names, images, generalPath, pd):
        self.cropsTransFiles = cropsTransFiles
        self.names = names
        self.images = images
        self.generalPath = generalPath
        self.pd = pd
        self.sizes = []
        convert = lambda text: int(text) if text.isdigit() else text
        self.alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]

    def indent(self, elem, level=0):

        '''funcion auxiliar para indentar el xml'''
        i = "\n" + level * "\t"
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "\t"
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level + 1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def create_xmls(self):
        bbox=create_bbox(self.i,self.name,(self.xmin,self.xmax,self.ymin,self.ymax))
        j = self.i%len(self.images)
        self.xmls.add_bbox(self.i,bbox,self.sizes[j])

    def blend_transparent(self, face_img, overlay_t_img):
        # Split out the transparency mask from the colour info
        overlay_img = overlay_t_img[:, :, :3]  # Grab the BRG planes
        overlay_mask = overlay_t_img[:, :, 3:]  # And the alpha plane

        # Again calculate the inverse mask
        background_mask = 255 - overlay_mask

        # Turn the masks into three channel, so we can use them as weights
        overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
        background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)

        # Create a masked out face image, and masked out overlay
        # We convert the images to floating point in range 0.0 - 1.0
        face_part = (face_img * (1 / 255.0)) * (background_mask * (1 / 255.0))
        overlay_part = (overlay_img * (1 / 255.0)) * (overlay_mask * (1 / 255.0))

        # And finally just add them together, and rescale it back to an 8bit integer image
        return np.uint8(cv2.addWeighted(face_part, 255.0, overlay_part, 255.0, 0.0))

    def sizeImages(self):
        for image in self.images:
            im = cv2.imread(image)
            f_im, c_im, col_im = im.shape
            size = c_im,f_im,col_im
            self.sizes.append(size)

    def rename(self):
        xmls = glob.glob(os.path.join(self.pathSamplesXmls,'*.xml'))
        xmls = sorted(xmls,key=self.alphanum_key)
        for i, xml in enumerate(xmls):
            dir =  os.path.dirname(xml)
            name_xml = os.path.basename(xml)
            pos1 = name_xml.rfind('frame')
            name_file_without_num = name_xml[:pos1+5]
            new_xml = os.path.join(dir,name_file_without_num+str(i)+'.xml')
            img = os.path.join(dir.replace('xmls','images'),name_xml.replace('.xml','.jpg'))
            new_img = os.path.join(dir.replace('xmls','images'),name_file_without_num+str(i)+'.jpg')
            os.rename(xml,new_xml)
            os.rename(img,new_img)


    def add_crops(self):
        done = False
        rep = -1
        pathSamples = os.path.join(self.generalPath, 'samples')
        if not os.path.exists(pathSamples):
            os.mkdir(pathSamples)

        pathSamplesXmls = os.path.join(pathSamples, 'xmls')
        if not os.path.exists(pathSamplesXmls):
            os.mkdir(pathSamplesXmls)

        pathSamplesImages = os.path.join(pathSamples, 'images')
        if not os.path.exists(pathSamplesImages):
            os.mkdir(pathSamplesImages)
            index = 0
        else:
            msg = wx.MessageDialog(None, dialog.DIALOG_USE_SAMPLES, style=wx.YES_NO | wx.ICON_EXCLAMATION)
            msg.Fit()
            msg.SetYesNoCancelLabels(
                dialog.DIALOG_LABEL_YES,
                dialog.DIALOG_LABEL_NO,
                dialog.DIALOG_LABEL_CANCEL
            )
            if msg.ShowModal() == wx.ID_YES:
                msg.Destroy()
                samples = len(glob.glob(os.path.join(pathSamplesImages, '*')))
                xmls = len(glob.glob(os.path.join(pathSamplesXmls, '*')))
                if samples != xmls:
                    msg = wx.MessageDialog(None, dialog.DIALOG_IMAGES_NO_XMLS, style=wx.OK | wx.ICON_EXCLAMATION)
                    msg.Fit()
                    if msg.ShowModal() == wx.OK or msg.ShowModal() == 5101:
                        msg.Destroy()
                        return done

                else:
                    index = samples

            else:
                index = 0

        self.sizeImages()

        im = cv2.imread(self.images[0])
        f_im, c_im, col_im = im.shape

        pathXML=os.path.join(pathSamplesXmls, os.path.splitext(os.path.basename(self.images[0]))[0] + '.xml')

        self.xmls = PXml.from_name(pathXML,project_name='jjuo',shape=(c_im,f_im,col_im))

        for i, cropTrans in enumerate(self.cropsTransFiles[index:]):
            self.i = i + index

            self.name = self.names[self.i]
            (keep, _) = self.pd.Update(self.i + 1,
                                       dialog.DIALOG_MAKE_SAMPLE % (str(self.i + 1) + '/' + str(len(self.cropsTransFiles))))
            self.pd.Fit()
            if not keep:
                self.pd.Destroy()
                return done

            j = self.i

            if j % len(self.images) == 0:
                rep += 1

            if j >= len(self.images):

                if rep == -1:
                    rep = int(np.floor(self.i / 40)) - 1

                j = j - ((rep + 1) * len(self.images))

            im = cv2.imread(self.images[j])

            f_im, c_im, col_im = im.shape

            cT = cv2.imread(cropTrans, -1)

            f_cT, c_cT, col_cT = cT.shape

            if f_im < f_cT or c_im < c_cT:
                # msg = wx.MessageDialog(None, dialog.DIALOG_SAMPLE_BIG_IMAGE % (os.path.basename(self.cropsTransFiles[i]), os.path.basename(self.images[j])),
                #                        style = wx.OK | wx.ICON_EXCLAMATION)
                # msg.Fit()
                # if msg.ShowModal() == wx.ID_OK or msg.ShowModal() == 5101:
                #     msg.Destroy()
                print(dialog.DIALOG_SAMPLE_BIG_IMAGE % (os.path.basename(self.cropsTransFiles[i]), os.path.basename(self.images[j])))
                continue


            else:
                if f_im - 1 > f_cT and c_im - 1 >  c_cT:
                    f_copy = np.random.randint(0, ((f_im - 1) - f_cT))
                    c_copy = np.random.randint(0, ((c_im - 1) - c_cT))

                    im_crop = im[f_copy:(f_copy + f_cT), c_copy:(c_copy + c_cT)]
                    im[f_copy:(f_copy + f_cT), c_copy:(c_copy + c_cT)] = self.blend_transparent(im_crop, cT)

                    filename = self.images[j]

                    pos1 = filename.rfind('/')
                    pos2 = filename.rfind('frame')

                    filename = filename[pos1 + 1:pos2+5]

                    reIn=0
                    if index:
                        reIn = 1

                    self.filenameIm = filename + str(self.i) + '.jpg'
                    cv2.imwrite(os.path.join(pathSamplesImages,self.filenameIm), im)


                    self.f = f_im
                    self.c = c_im

                    self.xmin = c_copy
                    self.xmax = c_copy + c_cT
                    self.ymin = f_copy
                    self.ymax = f_copy + f_cT

                    self.pathSamplesXmls = pathSamplesXmls

                    self.create_xmls()

        self.rename()
        done = True
        return done
