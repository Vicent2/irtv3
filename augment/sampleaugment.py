from augment.aug import DEFAULT_SEQUENTIAl
import cv2


class SampleAugmentator(object):
    def __init__(self, sequential=DEFAULT_SEQUENTIAl):
        self.seq = sequential

    def _augment_image(self, image):
        augim = self.seq.augment_image(image)
        return augim

    def _augment_images(self, images):
        augims = self.seq.augment_images(images)
        return augims

    def augment(self, im, per_image):
        if isinstance(im, str):
            im = [im]
        for i in self._give_images(im, per_image):
            yield self._augment_image(i)
            # return self._augment_images(list(self._give_images(im, per_image)))

    def _give_images(self, im, how_many=10):
        for i_name in im:
            the_image = cv2.imread(i_name)

            for _ in range(how_many):
                yield the_image


aum = SampleAugmentator()
