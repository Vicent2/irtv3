import os
from pixmlabs import PiXMLabs as PXml
from pixmlabs import create_bbox, Point
import glob
import re

convert = lambda text: int(text) if text.isdigit() else text
alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]


logos2 = ['eurovision','glasgow','scotland','spar']

paths = ['/home/stone/labels/GROUP1/bmx/labelsYolo',
         '/home/stone/labels/GROUP1/diving/labelsYolo',
         '/home/stone/labels/GROUP1/golf/labelsYolo',
         '/home/stone/labels/GROUP1/mtb/labelsYolo',
         '/home/stone/labels/GROUP1/open/labelsYolo',
         '/home/stone/labels/GROUP1/rowing/labelsYolo',
         '/home/stone/labels/GROUP1/swimming/labelsYolo',
         ]


paths = ['/media/stone/6EE36BE271AC75561/Videos_EUC_PROCESS/rowing/labelsYolo',
         '/media/stone/6EE36BE271AC75561/Videos_EUC_PROCESS/swimming/labelsYolo',
         ]

for labelsYoloPath in paths:
    projectNameDirectory = os.path.basename(os.path.dirname(labelsYoloPath))

    xmlsPath = labelsYoloPath.replace('labelsYolo', 'xmls')
    xml0Path = os.path.join(xmlsPath, projectNameDirectory + '_frame0.xml')

    if not os.path.exists(xmlsPath):
        os.mkdir(xmlsPath)

    # c, f, cha = cv2.imread(xml0Path.replace('xmls','images').replace('.xml','.jpg')).shape
    c = 1280
    f = 720
    cha = 3
    xmls = PXml.from_name(xml0Path, projectNameDirectory, (c, f, cha))

    labelsYolo = glob.glob(os.path.join(labelsYoloPath, '*.txt'))
    labelsYolo = sorted(labelsYolo, key=alphanum_key)



    for i, label in enumerate(labelsYolo):
        print(i,len(labelsYolo))
        lines = open(label, 'r')
        for line in lines.readlines():
            line = line.strip()
            detection = line.split('\t')
            if detection[0] in logos2:
                bbox = list(map(int, detection[1:5]))
                extent = [bbox[0], bbox[2], bbox[1], bbox[3]]
                bbox = create_bbox(i, detection[0], extent)
                xmls.add_bbox(i, bbox)