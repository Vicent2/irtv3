import os
from pixmlabs import PiXMLabs as PXml
from pixmlabs import create_bbox, Point
import glob
import re

convert = lambda text: int(text) if text.isdigit() else text
alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
colores = ['','','','']
logos2 = ['eurovision','glasgow','scotland','spar']
#
# paths = ['/home/stone/labels/GROUP1/bmx/xmls',
#          '/home/stone/labels/GROUP1/diving/xmls',
#          '/home/stone/labels/GROUP1/golf/xmls',
#          '/home/stone/labels/GROUP1/mtb/xmls',
#          '/home/stone/labels/GROUP1/open/xmls',
#          '/home/stone/labels/GROUP1/rowing/xmls',
#          '/home/stone/labels/GROUP1/swimming/xmls',]
# paths = ['/home/stone/labels/GROUP1/bmx/xmls',
#          '/home/stone/labels/GROUP1/diving/xmls',
#          '/home/stone/labels/GROUP1/golf/xmls',
#          '/home/stone/labels/GROUP1/mtb/xmls',
#          '/home/stone/labels/GROUP1/open/xmls',
#          ]
paths = [
        # '/media/stone/6EE36BE271AC75561/Videos_EUC_PROCESS/rowing/xmls',
         '/media/stone/6EE36BE271AC75561/Videos_EUC_PROCESS/swimming/xmls'
]
for xmlsPath in paths:


        xmlsG = PXml(xmlsPath)
        colors = {brand: color for brand, color in zip(logos2, colores)}
        name = os.path.basename(os.path.dirname(xmlsPath))

        xmlsG.to_xml(os.path.join(os.path.dirname(xmlsPath),name +'.xml'), logos2, colors)

