from boto.s3.connection import S3Connection
import os
import sys
import glob,re
import numpy as np

convert = lambda text: int(text) if text.isdigit() else text
ALPHANUM_KEY = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]


# # Uploads contents of LOCALFILE to Dropbox
# def backup():
#     with open(LOCALFILE, 'rb') as f:
#         # We use WriteMode=overwrite to make sure that the settings in the file
#         # are changed on upload
#         print("Uploading " + LOCALFILE + " to Dropbox as " + BACKUPPATH + "...")
#         try:
#             dbx.files_upload(f.read(), BACKUPPATH, mode=WriteMode('overwrite'))
#         except ApiError as err:
#             # This checks for the specific error where a user doesn't have enough Dropbox space quota to upload this file
#             if (err.error.is_path() and
#                     err.error.get_path().error.is_insufficient_space()):
#                 sys.exit("ERROR: Cannot back up; insufficient space.")
#             elif err.user_message_text:
#                 print(err.user_message_text)
#                 sys.exit()
#             else:
#                 print(err)
#                 sys.exit()


# Adding few functions to check file details
# def checkFileDetails():
#     print("Checking file details")
#
#     for entry in dropbox.files_list_folder('').entries:
#         print("File list is : ")
#         print(entry.name)


# Run this script independently
if __name__ == '__main__':
    files = ["sessions/CTR_D00-SES-1700-Cycling Track_ARC-0005.mxf","sessions/ROW_D00-SES-0800-Rowing_ARC-0004.mxf","Sessions/SWM_D01-SES-0800-Swimming_ARC-0007.mxf","Sessions/SYN_D01-SES-0730-Synchronised Swimming_ARC-0006.mxf","Sessions/GAR_D02-SES-1130-Artistic Gymnastics_ARC-000J.mxf","Sessions/CRD_D03-SES-1100-Cycling Road_ARC.mxf","Sessions/GAR_D03-SES-1300-Artistic Gymnastics_ARC-000Q.mxf","Sessions/ROW_D03-SES-0815-Rowing_ARC-000R.mxf","Sessions/ATH_D04-SES-1330-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D04-SES-1330-Athletics-ISOfeed-Track_ARC.mxf","Sessions/ATH_D04-SES-1330-Athletics-Integrated_ARC-000Z.mxf","Sessions/ATH_D04-SES-1400-Athletics-ISOFeed-Long Jump Group A_ARC.mxf","Sessions/ATH_D04-SES-1400-Athletics-ISOFeed-Long Jump Group B_ARC.mxf","Sessions/ATH_D04-SES-1500-Athletics-ISOFeed-Shot Put_ARC.mxf","Sessions/DIV_D04-SES-1030-Diving_ARC-0010.mxf","Sessions/ATH_D05-SES-0615-Athletics-ISOfeed-Race Walk_ARC.mxf","Sessions/ATH_D05-SES-0615-Athletics-Integrated_ARC-0014.mxf","Sessions/ATH_D05-SES-0800-Athletics-ISOfeed-Long Jump Group A_ARC.mxf","Sessions/ATH_D05-SES-0800-Athletics-ISOfeed-Long Jump Group B_ARC.mxf","Sessions/ATH_D05-SES-0800-Athletics-ISOfeed-Shot Put Group A_ARC.mxf","Sessions/ATH_D05-SES-0800-Athletics-ISOfeed-Shot Put Group B_ARC.mxf","Sessions/ATH_D05-SES-0940-Athletics-ISOfeed-Shot Put Group A_ARC.mxf","Sessions/ATH_D05-SES-0940-Athletics-ISOfeed-Shot Put Group B_ARC.mxf","Sessions/ATH_D05-SES-1600-Athletics-ISOfeed-High Jump Group A_ARC.mxf","Sessions/ATH_D05-SES-1600-Athletics-ISOfeed-High Jump Group B_ARC.mxf","Sessions/ATH_D05-SES-1600-Athletics-Integrated_ARC-001E.mxf","Sessions/ATH_D05-SES-1615-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D05-SES-1630-Athletics-ISOfeed-Pole Vault Group A_ARC.mxf","Sessions/ATH_D05-SES-1630-Athletics-ISOfeed-Pole Vault Group B_ARC.mxf","Sessions/CTR_D05-SES-0930-Cycling Track_ARC-001A.mxf","Sessions/MTB_D05-SES-0800-Mountain Bike_ARC-001C.mxf","Sessions/MTB_D05-SES-1300-Mountain Bike_ARC-001D.mxf","Sessions/SYN_D05-SES-1130-Synchronised Swimming_ARC-0017.mxf","Sessions/ATH_D06-SES-0700-Athletics-Integrated_ARC-001K.mxf","Sessions/ATH_D06-SES-0745-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D06-SES-0830-Athletics-ISOfeed-Triple Jump Group A_ARC.mxf","Sessions/ATH_D06-SES-0930-Athletics-ISOfeed-Pole Vault Group A_ARC.mxf","Sessions/ATH_D06-SES-1045-Athletics-ISOfeed-Pole Vault Group B_ARC.mxf","Sessions/ATH_D06-SES-1515-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D06-SES-1515-Athletics-Integrated_ARC-001M.mxf","Sessions/ATH_D06-SES-1515-Athletics-Integrated_ARC.mxf","Sessions/ATH_D06-SES-1600-Athletics-ISOfeed-High Jump Group A_ARC.mxf","Sessions/ATH_D06-SES-1600-Athletics-ISOfeed-High Jump Group B_ARC.mxf","Sessions/ATH_D06-SES-1715-Athletics-ISOfeed-Long Jump_ARC.mxf","Sessions/ATH_D06-SES-1730-Athletics-ISOfeed-Shot Put_ARC.mxf","Sessions/CRD_D06-SES-0730-Cycling Road_ARC-001P.mxf","Sessions/CRD_D06-SES-1130-Cycling Road_ARC-001Q.mxf","Sessions/ATH_D07-SES-0700-Athletics-Integrated_ARC-001V.mxf","Sessions/ATH_D07-SES-0715-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D07-SES-0800-Athletics-ISOfeed-Long Jump Group A_ARC.mxf","Sessions/ATH_D07-SES-0815-Athletics-ISOfeed-High Jump Group B_ARC.mxf","Sessions/ATH_D07-SES-1645-Athletics-ISOfeed-Pole Vault_ARC.mxf","Sessions/ATH_D07-SES-1645-Athletics-ISOfeed-Shot Put Group A_ARC.mxf","Sessions/ATH_D07-SES-1645-Athletics-ISOfeed-Shot Put Group B_ARC.mxf","Sessions/ATH_D07-SES-1645-Athletics-Integrated_ARC-0021.mxf","Sessions/ATH_D07-SES-1715-Athletics-ISOfeed-High Jump Group A_ARC.mxf","Sessions/ATH_D07-SES-1715-Athletics-ISOfeed-High Jump Group B_ARC.mxf","Sessions/ATH_D07-SES-1745-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/SWM_D07-SES-1515-Swimming_ARC-001U.mxf","Sessions/TRI_D07-SES-1200-Triathlon_ARC-001Y.mxf","Sessions/ATH_D08-SES-0730-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D08-SES-0730-Athletics-Integrated_ARC.mxf","Sessions/ATH_D08-SES-0815-Athletics-ISOfeed-Long Jumps Group A_ARC.mxf","Sessions/ATH_D08-SES-0815-Athletics-ISOfeed-Long Jumps Group B_ARC.mxf","Sessions/ATH_D08-SES-0900-Athletics-ISOfeed-Track_ARC.mxf","Sessions/ATH_D08-SES-1015-Athletics-ISOfeed-Triple Jumps Group A_ARC.mxf","Sessions/ATH_D08-SES-1015-Athletics-ISOfeed-Triple Jumps Group B_ARC.mxf","Sessions/ATH_D08-SES-1645-Athletics-ISOfeed-High Jumps_ARC.mxf","Sessions/ATH_D08-SES-1645-Athletics-ISOfeed-Track_ARC.mxf","Sessions/ATH_D08-SES-1645-Athletics-Integrated_ARC-0028.mxf","Sessions/ATH_D08-SES-1745-Athletics-ISOfeed-Triple Jumps_ARC.mxf","Sessions/ATH_D08-SES-1800-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/BMX_D08-SES-1030-BMX_ARC-002A.mxf","Sessions/TRI_D08-SES-1430-Triathlon_ARC-0029.mxf","Sessions/ATH_D09-SES-0700-Athletics-ISOfeed-Walk 20K_ARC.mxf","Sessions/ATH_D09-SES-0700-Athletics-Integrated_ARC-002H.mxf","Sessions/ATH_D09-SES-1730-Athletics-ISOfeed-High Jump_ARC.mxf","Sessions/ATH_D09-SES-1730-Athletics-Integrated_ARC.mxf","Sessions/ATH_D09-SES-1745-Athletics-ISOfeed-Track_ARC.mxf","Sessions/ATH_D09-SES-1745-Athletics-ISOfeed-Long Jumps_ARC.mxf","Sessions/ATH_D09-SES-1800-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/BMX_D09-SES-0830-BMX_ARC-002M.mxf","Sessions/GAR_D09-SES-1130-Artistic Gymnastics_ARC-002K.mxf","Sessions/GLF_D09-SES-1130-Golf_ARC-002N.mxf","Sessions/OWS_D09-SES-0930-Open Water Swimming_ARC-002J.mxf","Sessions/TRI_D09-SES-1600-Triathlon_ARC-002L.mxf","Sessions/ATH_D10-SES-0700-Athletics-ISOfeed-Marathon Ladies_ARC.mxf","Sessions/ATH_D10-SES-0700-Athletics-Integrated_ARC-002U.mxf","Sessions/ATH_D10-SES-0730-Athletics-ISOfeed-Marathon Men_ARC.mxf","Sessions/ATH_D10-SES-1615-Athletics-Integrated_ARC-002X.mxf","Sessions/ATH_D10-SES-1630-Athletics-ISOfeed-Pole Vault_ARC.mxf","Sessions/ATH_D10-SES-1645-Athletics-ISOfeed-Long Throws_ARC.mxf","Sessions/ATH_D10-SES-1730-Athletics-ISOfeed-Triple Jump_ARC.mxf","Sessions/CRD_D10-SES-0900-Cycling Road_ARC-002S.mxf","Sessions/DIV_D10-SES-1100-Diving_ARC-002Z.mxf","Sessions/GAR_D10-SES-1300-Artistic Gymnastics_ARC-002R.mxf","Sessions/GLF_D10-SES-1115-Golf_ARC.mxf","Sessions/OWS_D10-SES-0745-Open Water Swimming_ARC-002T.mxf"]
    new_files = []

    for file in files:
        new_file = file.replace('.mxf','.mp4')
        # print(new_file)
        new_files.append(new_file)

    hay = np.zeros_like(files)
    folder = '/media/stone/6EE36BE271AC7556/Eurochamps_Videos/'

    key = 'AKIAJLKCY5RLQBLSDP4Q'
    secret = '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'
    # files = 'test.mp4'
    c = S3Connection(key, secret, is_secure=False)
    bucket = c.get_bucket('ec2018-archive')
    for key in bucket.list():
        k = str(key.key)
        pos1 = k.find('Sessions')
        if k.find('.mp4') != -1:
            name = k[pos1:]
            print(name)
            if name == "Sessions/ATH_D10-SES-0700-Athletics-ISOfeed-Marathon Ladies_ARC.mp4":


                index = np.where(np.array(new_files) == name)[0][0]
                hay[index] = 1

                pos1 = k.rfind('/')
                last_folder = k[:pos1 + 1]
                final_folder = os.path.join(folder, last_folder).replace(' ', '')
                if not os.path.exists(final_folder):
                    os.makedirs(final_folder)
                # print(os.path.join(os.path.join(final_folder, k[pos1 +1:])))
                # print(os.path.join(os.path.join(final_folder, os.path.basename(k[:pos2+1]))))
                print(k)
                if not os.path.exists(os.path.join(os.path.join(final_folder, os.path.basename(k)).replace(' ',''))):
                    print('yes')
                    bktkey = bucket.get_key(k)
                    seconds = 60 * 60 * 12
                    url2 = bktkey.generate_url(expires_in=seconds)
                    os.system('wget -c' + ' \"' + url2 + '\" --directory-prefix=' + final_folder)

            else:
                print(0)


    print(hay)
    print(len(hay))
    indexes = np.where(np.array(hay) == '')[0]

    with open('missed.txt','w') as f:
        for index in indexes:

            f.write(new_files[index] + '\n')



    # TOKEN = 'Cw7uYTZ5p-AAAAAAAAAAEwelPfN5Ta4BWvMbKQuyc4rZw8bUOHzSSxtwpqeFmLpP'
    #
    # folders = sorted(glob.glob(os.path.join(folder,'mp4', '*')), key=ALPHANUM_KEY)
    # # print(folders)
    # for fol in folders:
    #     files =  sorted(glob.glob(os.path.join(fol,'Sessions', '*')), key=ALPHANUM_KEY)
    #     # print(files)
    #     for file in files:
    #         # LOCALFILE = file
    #         pos1 = file.find('?')
    #         # if pos1 != -1:
    #         #     os.rename(file,file[:pos1])
    #
    #         LOCALFILE = file[:pos1]
    #         name = os.path.basename(LOCALFILE)
    #         # print(name)
    #         pos2 = LOCALFILE.rfind('/mp4/')
    #         pos3 = LOCALFILE.rfind('Sessions')
    #         day = LOCALFILE[pos2 + 5: pos3]
    #         BACKUPPATH = '/PIXEL/PROYECTOS/EUC/EUC-Videos/' + day + name  # Keep the forward slash before destination filename
    #         print(BACKUPPATH)
    #
    #         # Check for an access token
    #         if (len(TOKEN) == 0):
    #             sys.exit("ERROR: Looks like you didn't add your access token. Open up backup-and-restore-example.py in a text editor and paste in your token in line 14.")
    #
    #         # Create an instance of a Dropbox class, which can make requests to the API.
    #         # print("Creating a Dropbox object...")
    #         dbx = dropbox.Dropbox(TOKEN)
    #
    #         # Check that the access token is valid
    #         try:
    #             dbx.users_get_current_account()
    #         except AuthError as err:
    #             sys.exit(
    #                 "ERROR: Invalid access token; try re-generating an access token from the app console on the web.")
    #
    #         try:
    #             checkFileDetails()
    #         except Error as err:
    #             sys.exit("Error while checking file details")
    #
    #         # print("Creating backup...")
    #         # Create a backup of the current settings file
    #         backup()
    #
    #         print("Done!")