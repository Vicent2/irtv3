import glob
import os
import re
import subprocess
import cv2
import numpy as np
from wx import ID_CANCEL, ID_OK

from strings.dialog import dialog


class bcolors:
    ENDL = '\n'
    TAB = '\t'
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @staticmethod
    def pokbmsg(msg):
        print(bcolors.OKBLUE + msg + bcolors.ENDC)

    @staticmethod
    def sokbmsg(msg):
        return bcolors.OKBLUE + msg + bcolors.ENDC


class VideoConverter(object):
    """    Class to convert a series of videos to images

    usage:
        v = VideoConverter(iterable).to_jpeg()

    accepts an iterable that contains any number of dictionaries with the following structure:

    dict(
        inp='/abs/path/to/the/inputfilename.mp4',  Required
        out='outputfilename.jpg',  Defaults      to the name of the video with jpg extension
        preinput=[],   List of options for ffmpeg defaults to []
        postinput=[],  List of options for ffmpeg defaults to ['-q:v', '1','-strict' 'unofficial']
        preoutput=[], List of options for ffmpeg defaults to ['-pix_fmt:v', 'yuv420p','-c:v', 'mjpeg']
        folder=[]      Name of the folder to store the images, defaults to the name of the video
    )
    """

    pattern_time = re.compile(r'Duration: (?P<hour>[0-9]+):(?P<min>[0-9]+):(?P<sec>[0-9]+).(?P<secdec>[0-9]+)')
    pattern_frames = re.compile(r'frame=( )*(?P<nFrame>[0-9]+)')

    def __init__(self, iterable, pd, num=None, fps=25):
        self.total_len = len(iterable)
        self.commands = iter(iterable)
        self.num = num
        self.fps = fps
        self.progress_dialog = pd
        self.keyword = 'frame'

    @staticmethod
    def sort_files_key(elemento, last_word='frame'):
        start = elemento.rfind(last_word)
        end = elemento.rfind('.')

        return int(elemento[start + len(last_word):end])

    @staticmethod
    def sort_files_key_fullreturn(elemento, last_word='frame'):
        start = elemento.rfind(last_word)
        end = elemento.rfind('.')

        return start, end, int(elemento[start + len(last_word):end])

    def total_time(self, video):
        print(video)

        proberesults = list()

        with subprocess.Popen(['ffprobe', video], executable='/usr/bin/ffprobe', stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True) as p:
            for line in p.stdout:
                proberesults.append(line.strip())
        proberesults = "\n".join(proberesults)
        match_result = self.pattern_time.search(proberesults)
        if match_result:
            hour, minutes, sec, secdec = match_result.group('hour'), match_result.group('min'), match_result.group(
                'sec'), match_result.group('secdec')
            time = int(hour) * 3600 + int(minutes) * 60 + int(sec) + int(secdec) / 100
            frames = round(time * self.fps)
        else:
            frames = None
        return frames

    def current_frame(self, ffmpegstring):
        match_result = self.pattern_frames.search(ffmpegstring)
        if match_result:
            frame = int(match_result.group('nFrame'))
        else:
            frame = None
        return frame

    def complete_command(self, args):
        inp = args["inp"]
        if 'out' not in args:
            out = self.file_name(inp) + '_frame.jpg'
        else:
            out = args["out"]

        base_name, ext = os.path.splitext(out)
        if ext in ['.jpeg', '.jpg', '.bmp', '.png', '.tiff', '.tif']:
            out = base_name + '%d' + ext

        if "preinput" not in args:
            preinput = []
        else:
            preinput = args["preinput"]

        if "postinput" not in args:
            postinput = ['-q:v', '1', '-strict', 'unofficial']
            # postinput = ['-q:v', '1', '-frames:v', '1000', '-strict', 'unofficial']
        else:
            postinput = args["postinput"]

        if "preoutput" not in args:

            if self.num is not None:
                imgps = float(self.num / self.total_secs)
            else:
                imgps = self.fps

            preoutput = ['-pix_fmt:v', 'yuv420p', '-c:v', 'mjpeg', '-r', str(imgps)]
        else:
            preoutput = args["preoutput"]

        DIR = os.path.dirname(inp)

        if "folder" in args:
            out = os.path.join(DIR, os.path.join(args["folder"], os.path.join('images', out)))
        else:
            in_name = os.path.split(os.path.splitext(inp)[0])[1]  # Video name, default name the folder
            out = os.path.join(DIR, os.path.join(in_name, os.path.join('images', out)))

        try:
            os.makedirs(os.path.dirname(out))
        except OSError:
            pass  # The directory already exists, no worries about it

        command_list = ['ffmpeg']
        command_list.extend(preinput)
        command_list.extend(['-i', inp])
        command_list.extend(postinput)
        command_list.extend(preoutput)
        command_list.extend([out])
        command_list.extend(['-cpu-used','1'])
        # command_list.extend(['-threads','2'])
        print(command_list)
        return command_list

    def rename(self, output_file):
        dir_name = os.path.dirname(output_file)
        images = sorted(glob.glob(dir_name + '/*.jpg'), key=self.sort_files_key)
        for im in images:
            start, end, number = self.sort_files_key_fullreturn(im, self.keyword)
            new_name = "{}{}{}{}".format(im[:start], self.keyword, number - 1, im[end:])
            os.rename(im, new_name)

    def to_jpeg(self):
        for i, com in enumerate(self.commands):
            if 'inp' in com:
                video_name = self.file_name(com['inp'])
                total_nframes = self.total_time(com['inp'])
                self.total_secs = total_nframes / self.fps
                if self.num is not None:
                    total_nframes = self.num
                full_command = self.complete_command(com)

                with subprocess.Popen(
                        full_command,
                        executable='/usr/bin/ffmpeg',
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.STDOUT,
                        bufsize=1,
                        universal_newlines=True
                ) as p:

                    for line in p.stdout:

                        nFrame = self.current_frame(line)
                        if nFrame is not None:
                            perc1, perc2 = (i / self.total_len), (1 / self.total_len) * (nFrame / total_nframes)

                            percentage = (perc1 + perc2) * 100

                            (keep, _) = self.progress_dialog.Update(percentage,
                                                                    dialog.DIALOG_GEN_IMAGES.format(i + 1,
                                                                                                    self.total_len,
                                                                                                    video_name))
                            self.progress_dialog.Fit()
                            if not keep:
                                p.stdin.write('q')
                                self.progress_dialog.Destroy()
                                return ID_CANCEL

                (keep, _) = self.progress_dialog.Update(percentage,
                                                        dialog.DIALOG_RENAMING_IMAGES.format(i + 1, self.total_len,
                                                                                             video_name)
                                                        )
                self.rename(full_command[-1])

        self.progress_dialog.Update(100)
        return ID_OK

    @staticmethod
    def file_name(file_path):
        name = os.path.splitext(os.path.basename(file_path))

        return name[0]
