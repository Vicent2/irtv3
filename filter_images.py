import glob
import os
import shutil
# path = 'path-to-folder with xmls folder and images folder'

path = '/media/stone/6EE36BE271AC75561/videos/OWS_D10-SES-0745-OpenWaterSwimming_ARC-002T'

new_images = os.path.join(path,'images-with-xmls')
if not os.path.exists(new_images):
    os.mkdir(new_images)

xmls = glob.glob(os.path.join(path,'xmls','*.xml'))


for xml in xmls:
    image = xml.replace('/xmls/','/images/').replace('.xml','.jpg')

    image2 = image.replace('/images/','/images-with-xmls/')

    shutil.copy(image,image2)



