import glob
import re

convert = lambda text: int(text) if text.isdigit() else text
alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]

labels = glob.glob('/home/stone/Rings_Men/labelsYolo/*')
labels = sorted(labels, key=alphanum_key)
W,H = 1280, 720
for label in labels:
    with open(label,'r') as f:
        reader = f.readlines()

        for line in reader:

            values = line.replace('\n','').split('\t')
            print(values)
            clase = values[0]
            xmin = int(values[1])
            ymin = int(values[2])
            xmax = int(values[3])
            ymax = int(values[4])

            w = (xmax - xmin) / W
            h = (ymax - ymin) / H

            xmin = xmin / W
            ymin = ymin / H

            left = (xmin - w / 2.) * W
            right = (xmin + w / 2.) * W
            top = (ymin - h / 2.) * H
            bot = (ymin + h / 2.) * H

            if left < 0: left = 0
            if right > W - 1: right = W-1
            if top < 0: top = 0
            if bot > H - 1: bot = H-1

            left = int(left)
            right = int(right)
            top = int(top)
            bot = int(bot)

            with open(label.replace('labelsYolo','labelsYolo2'),'a') as z:
                z.write(clase + '\t' + str(left) + '\t' + str(top) + '\t' + str(right) + '\t' + str(bot) + '\n')
