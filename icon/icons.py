import os

import wx

from strings.icon import icon

OPEN_LOGOS_PIC = 'icon/open_file.png'
OPEN_LOGOS_NAME = 'ButtonOpenLogos'
OPEN_LOGOS_HELP = icon.OPEN_LOGOS_HELP

OPEN_PROJECT_PIC = 'icon/open_home.png'
OPEN_PROJECT_NAME = 'ButtonOpen'
OPEN_PROJECT_HELP = icon.OPEN_PROJECT_HELP

OPEN_IMAGES_PIC = 'icon/open_picture.png'
OPEN_IMAGES_NAME = 'ButtonOpenImages'
OPEN_IMAGES_HELP = icon.OPEN_IMAGES_HELP

OPEN_VIDEO_PIC = 'icon/open_video.png'
OPEN_VIDEO_NAME = 'ButtonOpenVideo'
OPEN_VIDEO_HELP = icon.OPEN_VIDEO_HELP

SAVE_PIC = 'icon/save_proj.png'
SAVE_NAME = 'ButtonSave'
SAVE_HELP = icon.SAVE_HELP

AUGMENT_NAME = 'ButtonAugment'
AUGMENT_PIC = 'icon/augment_images.png'
AUGMENT_HELP = icon.AUGMENT_HELP

XML_NAME = 'ButtonGenerateXML'
XML_PIC = 'icon/xml.png'
XML_HELP = icon.XML_HELP

LABELS_NAME = 'ButtonGenerateLabels'
LABELS_PIC = 'icon/labels.png'
LABELS_HELP = icon.LABELS_HELP

XML_LABELS_NAME = 'ButtonGenerateXMLS_LABELS'
XML_LABELS_PIC = 'icon/xml_labels.png'
XML_LABELS_HELP = icon.XML_LABELS_HELP


XML_TXT_NAME = 'ButtonXMLTXT'
XML_TXT_PIC = 'icon/sound.png'
XML_TXT_HELP = icon.XML_TXT_HELP

ADD_PIC = 'icon/add.png'
ADD_HELP = icon.ADD_HELP

REMOVE_PIC = 'icon/remove.png'
REMOVE_HELP = icon.REMOVE_HELP

UNDO_PIC = 'icon/undo.png'
UNDO_HELP = icon.UNDO_HELP

REMOVE_ALL_PIC = 'icon/delete_all.png'
REMOVE_ALL_HELP = icon.REMOVE_ALL_HELP

DELETE_DRAW_PIC = 'icon/delete_draw.png'
DELETE_DRAW_HELP = icon.DELETE_DRAW_HELP

SELECT_PIC = 'icon/select.png'
SELECT_HELP = icon.SELECT_HELP

NEXT_PIC = 'icon/next.png'
NEXT_HELP = icon.NEXT_HELP

BEFORE_PIC = 'icon/before.png'
BEFORE_HELP = icon.BEFORE_HELP

NEXT_NUM_PIC = 'icon/nextNum.png'
NEXT_NUM_HELP = icon.NEXT_NUM_HELP

BEFORE_NUM_PIC = 'icon/beforeNum.png'
BEFORE_NUM_HELP = icon.BEFORE_NUM_HELP

NEXT_DETECT_PIC = 'icon/nextDetect.png'
NEXT_DETECT_HELP = icon.NEXT_DETECT_HELP

BEFORE_DETECT_PIC = 'icon/beforeDetect.png'
BEFORE_DETECT_HELP = icon.BEFORE_DETECT_HELP

DELETE_PIC = 'icon/delete.png'
DELETE_HELP = icon.DELETE_HELP

BOX_HELP = icon.BOX_HELP

LIST_DETECT_HELP = icon.LIST_DETECT_HELP

LIST_LOGOS_HELP = icon.LIST_LOGOS_HELP

MORE_DET_NAME = '+ Det.'
MORE_DET_HELP = icon.MORE_DET_HELP

DET_AT_ONCE_NAME = 'Move all'
DET_AT_ONCE_HELP = icon.DET_AT_ONCE_HELP

LANGUAGE_PIC = icon.LANGUAGE_PIC
LANGUAGE_HELP = icon.LANGUAGE_HELP


class IconManager:
    def __init__(self, root=None):
        self.root = root

    def set_root_dir(self, root):
        self.root = root

    def to_bitmap(self, icon, w, h=None):
        if self.root is None:
            raise AttributeError('%s has not root' % self.__class__)

        if h is None:
            h = w
        return wx.Image(os.path.join(self.root, icon)).Rescale(width=w, height=h).ConvertToBitmap()
