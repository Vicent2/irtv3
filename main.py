#! /usr/bin/env python
import os

import matplotlib  # librearia utilizada para crear figuras y subfiguras en python
import wx  # wx es la libreria de wxpyhton para crear interfaces de usuario en python (es parecida a tkinter)

matplotlib.use('WXAgg')
from strings.save_all import save_all

save_all()
from archivos import Myframe as MF


def main():
    app = wx.App()
 
    fr = MF.MyFrame(None, wx.ID_CLOSE_ALL, 'IRT')
    icon = fr.Icon
    ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
    iconpath = os.path.join(ROOT_PATH, 'icon/icono.png')
    icon.CopyFromBitmap(wx.Bitmap(iconpath, wx.BITMAP_TYPE_ANY))
    fr.SetIcon(icon)

    # Start the demo app
    fr.Show(True)
    app.MainLoop()

if __name__ == "__main__":
    # Create an demo application
    main()

