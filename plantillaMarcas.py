# -*- coding: utf-8 -*-
import os
import pandas as pd
import xlwt
import lxml
from lxml import etree
from datetime import datetime
from xlwt import *
import xlsxwriter
import math
from os import listdir



#--------------------------------------------------------------------------------------------------------------------------------
#IMPORTANTE, ANTES DE USAR EL SCRIPT
#Los xmls deben estar dentro de una carpeta, la ruta de esta carpeta es el path que es necesario modificar
#Este script sirve para cualquier número de xmls pero en caso de querer añadir más marcas será necesario añadir una "tabla/bloque"
#nueva correspondiente a la nueva marca
#-------------------------------------------------------------------------------------------------------------------------------

#Estilos
#Estilo cabecera
style0 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour dark_purple; borders: left medium, right medium, top medium, bottom medium;')
style01 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour sea_green; borders: left medium, right medium, top medium, bottom medium;')
style02 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour ocean_blue; borders: left medium, right medium, top medium, bottom medium;')
style03 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour gold; borders: left medium, right medium, top medium, bottom medium;')
style04 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour coral; borders: left medium, right medium, top medium, bottom medium;')
style05 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour sky_blue; borders: left medium, right medium, top medium, bottom medium;')
style1 = xlwt.easyxf('font: name Arial, bold on; align: wrap on, horiz center; borders: left medium, right medium, top medium, bottom medium;')
style3 = xlwt.easyxf('font: name Arial; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
style4 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour black;')

#creamos el excel y la fila de las cabeceras
wb = xlwt.Workbook()
ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)


#-----eurovision--------------------------------
path = 'xmls/'
#Lista vacia para incluir los ficheros
lstFiles = []
#Lista con todos los ficheros del directorio:
lstDir = os.walk(path)   #os.walk()Lista directorios y ficheros
# Crea una lista de los ficheros xml que existen en el directorio y los incluye a la lista.
for root, dirs, files in lstDir:
    for fichero in files:
        (nombreFichero, extension) = os.path.splitext(fichero)
        if (extension == ".xml"):
            lstFiles.append(nombreFichero + extension)
i = 1
nombres = ws.col(0)
nombres.width = 256 * 25
lives = ws.col(22)
lives.width = 256 * 20
days = ws.col(21)
days.width = 256 * 20
lstFiles.sort()
impac = 0
tot =0
perTotal=0
tiem = 0
arPro=0
zona1 = 0
zona2 = 0
zona3 = 0
zona4 = 0
zona5 = 0
zona6 = 0
zona7 = 0
zona8 = 0
zona9 = 0
l = 0 #contador de events y days
c = 0 #contador de lives
for fichero in lstFiles:

    if fichero.startswith('_'):
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video


        ws.write(i+1, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("eurovision")
        print(eurovision)
        impactosC = eurovision[0].find("Impactos")

        impac = float(impactosC.text) + impac
        ws.write(i+1, 1, impactosC.text, style3)
        total = eurovision[1].find("Tiempo_acumulado_brand")
        tot =  float(total.text) + tot
        partes = str(total.text).split(".")
        milesimas = int(partes[1])
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            if(segundos<10):
                ws.write(1 + i, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                ws.write(1 + i, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(1 + i, 2,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(1 + i, 2,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
            else:
                if (segundos < 10):
                    ws.write(1 + i, 2,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(1 + i, 2,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
        max = eurovision[0].find("Tiempo_acumulado_max")
        m = float(max.text)
        n = 1
        if (m > n):  # para obtener el máximo
            n = m
        Dividendo2 = float(max.text)
        ws.write(i+1,3,str('%.f'% float(max.text)),style3)
        average = eurovision[0].find("Tiempo_acumulado_average")
        Dividendo = float(average.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        partes = str(average.text).split(".")
        
        entera= int(partes[0])
        milesimas=str('%.2f' % (float(average.text)-entera))
        partes2 = str(milesimas).split(".")
        milesimas2 = int(partes2[1])
        if (minutos == 0):
            if(segundos<10):
                ws.write(1 + i, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                ws.write(1 + i, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(1 + i, 4,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(1 + i, 4,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
            else:
                if (segundos < 10):
                    ws.write(1 + i, 4,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(1 + i, 4,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 1, 5, str('%.2f' % per + '%'), style3)
        perTotal = per +  perTotal
        l = l+1#contador para calcular el promedio
        tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        # Para obetener min y segundos
        tiem = float(tiempo.text) + tiem
        Dividendo = float(tiempo.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if minutos > 60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2
        else:
            horas = 0
        if (horas < 10 and horas >= 0):
            if (segundos < 10):
                ws.write(i + 1, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 1, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        else:
            if (segundos < 10):
                ws.write(i + 1, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 1, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        pos = eurovision.find('POSITION')
        j = 1

        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            ws.write(i+1,j+8,z,style3)
            j = j+1
            if zon == 'Zona1':
                zona1 = zona1 + z
            if zon == 'Zona2':
                zona2 = zona2 + z
            if zon == 'Zona3':
                zona3 = zona3 + z
            if zon == 'Zona4':
                zona4 = zona4 + z
            if zon == 'Zona5':
                zona5 = zona5 + z
            if zon == 'Zona6':
                zona6 = zona6 + z
            if zon == 'Zona7':
                zona7 = zona7 + z
            if zon == 'Zona8':
                zona8 = zona8 + z
            if zon == 'Zona9':
                zona9 = zona9 + z
        area = eurovision.find('AREA')
        med = float(area.find('Media_Porcentaje_Frames').text)
        arPro = med + arPro
        ws.write(i+1, 19, str('%.2f' % med)+'%', style3)
        i = i + 1

#i=2
perTotalL = 0
for fichero in lstFiles:

    if fichero.startswith('_')== False:
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(1+i, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("eurovision")
        impactosC = eurovision[0].find("Impactos")
        impac = float(impactosC.text) + impac
        ws.write(1+i, 1, impactosC.text, style3)
        total = eurovision[1].find("Tiempo_acumulado_brand")
        tot = tot + float(total.text)

        partes = str(total.text).split(".")
        milesimas = int(partes[1])
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            if(segundos<10):
                ws.write(1 + i, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                ws.write(1 + i, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(1 + i, 2,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(1 + i, 2,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
            else:
                if (segundos < 10):
                    ws.write(1 + i, 2,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(1 + i, 2,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
        max = eurovision[0].find("Tiempo_acumulado_max")
        m = float(max.text)
        if (m > n):  # para obtener el máximo
            n = m
        Dividendo2 = float(max.text)
        ws.write(1+i,3,str('%.f'% float(max.text)),style3)
        average = eurovision[0].find("Tiempo_acumulado_average")
        Dividendo = float(average.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        partes = str(average.text).split(".")
        entera = int(partes[0] )
        milesimas = str('%.2f' % (float(average.text) - entera))
        partes2 = str(milesimas).split(".")
        milesimas2 = int(partes2[1])
        if (minutos == 0):
            if(segundos<10):
                ws.write(1 + i, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                ws.write(1 + i, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(1 + i, 4,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(1 + i, 4,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
            else:
                if (segundos < 10):
                    ws.write(1 + i, 4,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(1 + i, 4,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 1, 5, str('%.2f' % per + '%'), style3)
        if fichero.startswith('_DAY') == True:
            perTotal = perTotal + per
            l = l+1
        else:
            perTotalL = perTotalL + per
            c = c + 1
        #ws.write(i + 1, 4, str('%.2f' % float(average.text)) + 'sec', style3)
        tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        # Para obetener min y segundos
        tiem = tiem + float(tiempo.text)
        Dividendo = float(tiempo.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if minutos > 60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2

        else:
            horas = 0
        if(horas<10 or horas >=0):
            if(segundos<10):
                ws.write(i + 1, 6, str('0'+'%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)), style3)
            else:
                ws.write(i + 1, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                     '%.f' % (segundos)), style3)
        else:
            if (segundos < 10):
                ws.write(i + 1, 6, str( '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 1, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        pos = eurovision.find('POSITION')
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            ws.write(i + 1, j + 8, z, style3)
            j = j+1
            if zon == 'Zona1':
                zona1 = zona1 + z
            if zon == 'Zona2':
                zona2 = zona2 + z
            if zon == 'Zona3':
                zona3 = zona3 + z
            if zon == 'Zona4':
                zona4 = zona4 + z
            if zon == 'Zona5':
                zona5 = zona5 + z
            if zon == 'Zona6':
                zona6 = zona6 + z
            if zon == 'Zona7':
                zona7 = zona7 + z
            if zon == 'Zona8':
                zona8 = zona8 + z
            if zon == 'Zona9':
                zona9 = zona9 + z
        area = eurovision.find('AREA')
        med = float(area.find('Media_Porcentaje_Frames').text)
        arPro = arPro + med
        ws.write(1 + i, 19, str('%.2f' %med)+'%', style3)
        i=i+1


ws.write(1, 0, 'eurovision', style0)
ws.write(1,1,'Impacts',style1)
ws.write(1,2,'Total',style1)
ws.write(1,3,'Max',style1)
ws.write(1,4,'Average',style1)
ws.write(1,5,'Percentage',style1)
ws.write(1,6,'Duration',style1)
ws.write(1,9,'Zone1',style1)
ws.write(1,10,'Zone2',style1)
ws.write(1,11,'Zone3',style1)
ws.write(1,12,'Zone4',style1)
ws.write(1,13,'Zone5',style1)
ws.write(1,14,'Zone6',style1)
ws.write(1,15,'Zone7',style1)
ws.write(1,16,'Zone8',style1)
ws.write(1,17,'Zone9',style1)
ws.write(1,19,'Area',style1)
ws.write(1,21,'HL PERCENTAGE',style1)
ws.write(1,22,'LS PERCENTAGES',style1)
ws.write(i+1,8,'Total',style0)
ws.write(i+1,0,'Total',style0)
ws.write(i+1,1,impac,style0)
Dividendo = tot
divisor = int(60)
minutos = math.floor(Dividendo / divisor)
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if (minutos == 0):
    if (segundos < 10):
        ws.write(1 + i, 2, str('%.f' % (horas)+':00:0' + '%.f' % (segundos)), style0)
    else:
        ws.write(1 + i, 2, str('%.f' % (horas)+':00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style0)
else:
    if minutos > 60 or minutos == 60:
        horas = horas + 1
        minutos = minutos - 60
    if (minutos < 10):
        if (segundos < 10):
            ws.write(1 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)
    else:
        if (segundos < 10):
            ws.write(1 + i, 2,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 2,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)
redondeo = str(n).split(".")
ws.write(i+1,3,redondeo,style0)
aver = (tot/impac)
ws.write(i+1,4,'%.2f' % (aver),style0)
porcentaje = ((tot / tiem)*100)
ws.write(i+1,5,'%.2f' % porcentaje+'%',style0)
Dividendo = tiem
divisor = int(60)
frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
minutos = math.floor(Dividendo / divisor)
min = minutos
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if(horas<10 and horas>0):
    if (minutos < 10):
        if (segundos < 10):
            ws.write(1 + i, 6,
                     str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 6,
                     str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)
    else:
        if (segundos < 10):
            ws.write(1 + i, 6,
                     str('0'+'%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 6,'0'+'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)

else:
    if (minutos < 10):
        if (segundos < 10):
            ws.write(1 + i, 6,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 6,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)
    else:
        if (segundos < 10):
            ws.write(1 + i, 6,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style0)
        else:
            ws.write(1 + i, 6,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style0)

ws.write(1 + i, 9, zona1,style0)
ws.write(1 + i, 10, zona2,style0)
ws.write(1 + i, 11, zona3,style0)
ws.write(1 + i, 12, zona4,style0)
ws.write(1 + i, 13, zona5,style0)
ws.write(1 + i, 14, zona6,style0)
ws.write(1 + i, 15, zona7,style0)
ws.write(1 + i, 16, zona8,style0)
ws.write(1 + i, 17, zona9,style0)
ws.write(1 + i, 19, str('%.2f' % (arPro/len(lstFiles)) + '%'),style0)
ws.write(2,21,str('%.2f' % (perTotal/l) + '%'),style0)


#--------------------------------------------------------------------------------------
#-----------glasgow------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

ws.write(i + 3, 0, 'glasgow', style01)
ws.write(i + 3,1,'Impacts',style1)
ws.write(i + 3,2,'Total',style1)
ws.write(i + 3,3,'Max',style1)
ws.write(i + 3,4,'Average',style1)
ws.write(i + 3,5,'Percentage',style1)
ws.write(i + 3,6,'Duration',style1)
ws.write(i + 3,9,'Zone1',style1)
ws.write(i + 3,10,'Zone2',style1)
ws.write(i + 3,11,'Zone3',style1)
ws.write(i + 3,12,'Zone4',style1)
ws.write(i + 3,13,'Zone5',style1)
ws.write(i + 3,14,'Zone6',style1)
ws.write(i + 3,15,'Zone7',style1)
ws.write(i + 3,16,'Zone8',style1)
ws.write(i + 3,17,'Zone9',style1)
ws.write(i + 3,19,'Area',style1)
ws.write(i + 3,21,'HL PERCENTAGE',style1)
ws.write(i + 3,22,'LS PERCENTAGES',style1)
#ws.write(i + 4 + len(lstFiles),8,'Total',style01)
impac = 0
tot =0
perTotal=0
tiem = 0
arPro=0
zona1 = 0
zona2 = 0
zona3 = 0
zona4 = 0
zona5 = 0
zona6 = 0
zona7 = 0
zona8 = 0
zona9 = 0
k = i
for fichero in lstFiles:

    if fichero.startswith('_'):
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 4, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("glasgow")
        if eurovision != None:
            impactosC = eurovision[0].find("Impactos")
            impac = float(impactosC.text) + impac
            ws.write(i + 4, 1, impactosC.text, style3)
            total = eurovision[1].find("Tiempo_acumulado_brand")
            tot = float(total.text) + tot
            partes = str(total.text).split(".")
            milesimas = int(partes[1])
            Dividendo = float(total.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            if (minutos == 0):
                if(segundos<10):
                    ws.write(i + 4, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(4 + i, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                if(minutos<10):
                    if (segundos < 10):
                        ws.write(i+4, 2,
                             str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                    else:
                        ws.write(i+4, 2,
                                 str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                else:
                    if (segundos < 10):
                        ws.write(1 + i+4, 2,
                             str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                    else:
                        ws.write(1 + i+4, 2,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
            max = eurovision[0].find("Tiempo_acumulado_max")
            Dividendo2 = float(max.text)
            m = float(max.text)
            n = 1
            if (m>n):#para obtener el máximo
               n = m
            ws.write(i + 4,3,str('%.f'% float(max.text)),style3)
            average = eurovision[0].find("Tiempo_acumulado_average")
            Dividendo = float(average.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            partes = str(average.text).split(".")
            entera= int(partes[0])
            milesimas=str('%.2f' % (float(average.text)-entera))
            partes2 = str(milesimas).split(".")
            milesimas2 = int(partes2[1])
            if (minutos == 0):
                if(segundos<10):
                    ws.write(i+4, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i+4, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                if(minutos<10):
                    if (segundos < 10):
                        ws.write(4+i, 4,
                             str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                    else:
                        ws.write(4 + i, 4,
                                 str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                else:
                    if (segundos < 10):
                        ws.write(4 + i, 4,
                             str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                    else:
                        ws.write(4 + i, 4,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
            presence = eurovision.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 4, 5, str('%.2f' % per + '%'), style3)
            perTotal = per + perTotal
            tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
            tiem = float(tiempo.text) + tiem
            # Para obetener min y segundos
            Dividendo = float(tiempo.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if minutos > 60:
                Dividendo2 = minutos
                divisor2 = int(60)
                horas = math.floor(Dividendo2 / divisor2)
                minutos = Dividendo2 % divisor2
            else:
                horas = 0
            if (horas < 10 and horas >= 0):
                if (segundos < 10):
                    ws.write(i + 4, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 4, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 4, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 4, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            pos = eurovision.find('POSITION')
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                ws.write(i+4,j+8,z,style3)
                j = j+1
                if zon == 'Zona1':
                    zona1 = zona1 + z
                if zon == 'Zona2':
                    zona2 = zona2 + z
                if zon == 'Zona3':
                    zona3 = zona3 + z
                if zon == 'Zona4':
                    zona4 = zona4 + z
                if zon == 'Zona5':
                    zona5 = zona5 + z
                if zon == 'Zona6':
                    zona6 = zona6 + z
                if zon == 'Zona7':
                    zona7 = zona7 + z
                if zon == 'Zona8':
                    zona8 = zona8 + z
                if zon == 'Zona9':
                    zona9 = zona9 + z
            area = eurovision.find('AREA')
            med = float(area.find('Media_Porcentaje_Frames').text)
            arPro = med + arPro
            ws.write(4 + i, 19, str('%.2f' % med)+'%', style3)
            i = i+1

perTotalL = 0
for fichero in lstFiles:
    if fichero.startswith('_') == False:
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 4, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("glasgow")
        if eurovision != None:
            impactosC = eurovision[0].find("Impactos")
            impac = float(impactosC.text) + impac
            ws.write(i + 4, 1, impactosC.text, style3)
            total = eurovision[1].find("Tiempo_acumulado_brand")
            tot = tot + float(total.text)

            partes = str(total.text).split(".")
            milesimas = int(partes[1])
            Dividendo = float(total.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            if (minutos == 0):
                if (segundos < 10):
                    ws.write(i + 4, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(4 + i, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                if (minutos < 10):
                    if (segundos < 10):
                        ws.write(i + 4, 2,
                                 str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                    else:
                        ws.write(i + 4, 2,
                                 str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                else:
                    if (segundos < 10):
                        ws.write( i + 4, 2,
                                 str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                    else:
                        ws.write( i + 4, 2,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
            max = eurovision[0].find("Tiempo_acumulado_max")
            m = float(max.text)
            if (m>n):#para obtener el máximo
               n = m
            Dividendo2 = float(max.text)
            ws.write(i + 4, 3, str('%.f' % float(max.text)), style3)
            average = eurovision[0].find("Tiempo_acumulado_average")

            Dividendo = float(average.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            partes = str(average.text).split(".")
            entera = int(partes[0])
            milesimas = str('%.2f' % (float(average.text) - entera))
            partes2 = str(milesimas).split(".")
            milesimas2 = int(partes2[1])
            if (minutos == 0):
                if (segundos < 10):
                    ws.write(i + 4, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i + 4, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                if (minutos < 10):
                    if (segundos < 10):
                        ws.write(4 + i, 4,
                                 str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                    else:
                        ws.write(4 + i, 4,
                                 str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                else:
                    if (segundos < 10):
                        ws.write(4 + i, 4,
                                 str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                    else:
                        ws.write(4 + i, 4,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
            presence = eurovision.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 4, 5, str('%.2f' % per + '%'), style3)
            if fichero.startswith('_DAY') == True:
                perTotal = perTotal + per
            else:
                perTotalL = perTotalL + per
            tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
            tiem = tiem + float(tiempo.text)
            # Para obetener min y segundos
            Dividendo = float(tiempo.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if minutos > 60:
                Dividendo2 = minutos
                divisor2 = int(60)
                horas = math.floor(Dividendo2 / divisor2)
                minutos = Dividendo2 % divisor2
            else:
                horas = 0
            if (horas < 10 and horas >= 0):
                if (segundos < 10):
                    ws.write(i + 4, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 4, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 4, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 4, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            pos = eurovision.find('POSITION')
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                ws.write(i + 4, j + 8, z, style3)
                j = j + 1
                if zon == 'Zona1':
                    zona1 = zona1 + z
                if zon == 'Zona2':
                    zona2 = zona2 + z
                if zon == 'Zona3':
                    zona3 = zona3 + z
                if zon == 'Zona4':
                    zona4 = zona4 + z
                if zon == 'Zona5':
                    zona5 = zona5 + z
                if zon == 'Zona6':
                    zona6 = zona6 + z
                if zon == 'Zona7':
                    zona7 = zona7 + z
                if zon == 'Zona8':
                    zona8 = zona8 + z
                if zon == 'Zona9':
                    zona9 = zona9 + z
            area = eurovision.find('AREA')
            med = float(area.find('Media_Porcentaje_Frames').text)
            arPro = arPro + med
            ws.write(4 + i, 19, str('%.2f' % med)+'%', style3)
            i = i+1

ws.write(i + 4,0,'Total',style01)
ws.write(i + 4,1,impac,style01)
Dividendo = tot
divisor = int(60)
minutos = math.floor(Dividendo / divisor)
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if (minutos == 0):
    if (segundos < 10):
        ws.write(4 + i, 2, str('%.f' % (horas)+':00:0' + '%.f' % (segundos)), style01)
    else:
        ws.write(4 + i, 2, str('%.f' % (horas)+':00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style01)
else:
    if minutos > 60 or minutos == 60:
        horas = horas + 1
        minutos = minutos - 60
    if (minutos < 10):
        if (segundos < 10):
            ws.write(4 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style01)
        else:
            ws.write(4 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)
    else:
        if (segundos < 10):
            ws.write(4 + i, 2,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style01)
        else:
            ws.write(4 + i, 2,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)
    redondeo = str(n).split(".")
    ws.write(i+4,3,redondeo,style01)
    aver = (tot/impac)
    ws.write(i+4,4,'%.2f' % (aver),style01)
    porcentaje = ((tot / tiem)*100)
    ws.write(i+4,5,'%.2f' % porcentaje+'%',style01)
    Dividendo = float(tiem)
    divisor = int(60)
    frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
    minutos = math.floor(Dividendo / divisor)
    min = minutos
    segundos = Dividendo % divisor
    if minutos > 60:
        Dividendo2 = minutos
        divisor2 = int(60)
        horas = math.floor(Dividendo2 / divisor2)
        minutos = Dividendo2 % divisor2

    else:
        horas = 00
    if(horas<10 and horas>0):
        if (minutos < 10):
            if (segundos < 10):
                ws.write(4 + i, 6,
                         str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style01)
            else:
                ws.write(4 + i, 6,
                         str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)
        else:
            if (segundos < 10):
                ws.write(4 + i, 6,
                         str('0'+'%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style01)
            else:
                ws.write(4 + i, 6,'0'+'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)

    else:
        if (minutos < 10):
            if (segundos < 10):
                ws.write(4 + i, 6,
                         str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style01)
            else:
                ws.write(4 + i, 6,
                         str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)
        else:
            if (segundos < 10):
                ws.write(4 + i, 6,
                         str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style01)
            else:
                ws.write(4 + i, 6,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style01)
    ws.write(i + 4 ,8,'Total',style01)
    ws.write(4 + i, 9, zona1, style01)
    ws.write(4 + i, 10, zona2, style01)
    ws.write(4 + i, 11, zona3, style01)
    ws.write(4 + i, 12, zona4, style01)
    ws.write(4 + i, 13, zona5, style01)
    ws.write(4 + i, 14, zona6, style01)
    ws.write(4 + i, 15, zona7, style01)
    ws.write(4 + i, 16, zona8, style01)
    ws.write(4 + i, 17, zona9, style01)
    ws.write(4 + i, 19, str('%.2f' % (arPro/len(lstFiles)) + '%'),style01)
    ws.write(k + 4,21,str('%.2f' % (perTotal/l) + '%'),style01)

#--------------------------------------------------------------------------------------
#-----------scotland------------------------------------------------------------------------
#--------------------------------------------------------------------------------------

ws.write(i + 7, 0, 'scotland', style02)
ws.write(i + 7,1,'Impacts',style1)
ws.write(i + 7,2,'Total',style1)
ws.write(i + 7,3,'Max',style1)
ws.write(i + 7,4,'Average',style1)
ws.write(i + 7,5,'Percentage',style1)
ws.write(i + 7,6,'Duration',style1)
ws.write(i + 7,9,'Zone1',style1)
ws.write(i + 7,10,'Zone2',style1)
ws.write(i + 7,11,'Zone3',style1)
ws.write(i + 7,12,'Zone4',style1)
ws.write(i + 7,13,'Zone5',style1)
ws.write(i + 7,14,'Zone6',style1)
ws.write(i + 7,15,'Zone7',style1)
ws.write(i + 7,16,'Zone8',style1)
ws.write(i + 7,17,'Zone9',style1)
ws.write(i + 7,19,'Area',style1)
ws.write(i + 7,21,'HL PERCENTAGE',style1)
ws.write(i + 7,22,'LS PERCENTAGES',style1)
impac = 0
tot =0
perTotal=0
tiem = 0
arPro=0
zona1 = 0
zona2 = 0
zona3 = 0
zona4 = 0
zona5 = 0
zona6 = 0
zona7 = 0
zona8 = 0
zona9 = 0
k = i
for fichero in lstFiles:

    if fichero.startswith('_'):
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 8 , 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("scotland")
        if eurovision != None:
            impactosC = eurovision[0].find("Impactos")
            impac = float(impactosC.text) + impac
            ws.write(i + 8, 1, impactosC.text, style3)
            total = eurovision[1].find("Tiempo_acumulado_brand")
            tot = float(total.text) + tot
            partes = str(total.text).split(".")
            milesimas = int(partes[1])
            Dividendo = float(total.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            if (minutos == 0):
                if(segundos<10):
                    ws.write(i + 8, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(i + 8, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                if(minutos<10):
                    if (segundos < 10):
                        ws.write(i + 8, 2,
                             str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                    else:
                        ws.write(i + 8, 2,
                                 str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                else:
                    if (segundos < 10):
                        ws.write(i + 8, 2,
                             str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                    else:
                        ws.write(i + 8, 2,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
            max = eurovision[0].find("Tiempo_acumulado_max")
            m = float(max.text)
            n = 1
            if (m > n):  # para obtener el máximo
                n = m
            Dividendo2 = float(max.text)
            ws.write(i + 8,3,str('%.f'% float(max.text)),style3)
            average = eurovision[0].find("Tiempo_acumulado_average")

            Dividendo = float(average.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            partes = str(average.text).split(".")
            entera= int(partes[0])
            milesimas=str('%.2f' % (float(average.text)-entera))
            partes2 = str(milesimas).split(".")
            milesimas2 = int(partes2[1])
            if (minutos == 0):
                if(segundos<10):
                    ws.write(i + 8, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i + 8, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                if(minutos<10):
                    if (segundos < 10):
                        ws.write(i +8, 4,
                             str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                    else:
                        ws.write(i + 8, 4,
                                 str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                else:
                    if (segundos < 10):
                        ws.write(i + 8, 4,
                             str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                    else:
                        ws.write(i + 8, 4,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
            presence = eurovision.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 8, 5, str('%.2f' % per + '%'), style3)
            perTotal = per + perTotal
            tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
            tiem = float(tiempo.text) + tiem
            # Para obetener min y segundos
            Dividendo = float(tiempo.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if minutos > 60:
                Dividendo2 = minutos
                divisor2 = int(60)
                horas = math.floor(Dividendo2 / divisor2)
                minutos = Dividendo2 % divisor2
            else:
                horas = 0
            if (horas < 10 and horas >= 0):
                if (segundos < 10):
                    ws.write(i + 8, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 8, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 8, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 8, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            pos = eurovision.find('POSITION')
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                ws.write(i+8,j+8,z,style3)
                j = j+1
                if zon == 'Zona1':
                    zona1 = zona1 + z
                if zon == 'Zona2':
                    zona2 = zona2 + z
                if zon == 'Zona3':
                    zona3 = zona3 + z
                if zon == 'Zona4':
                    zona4 = zona4 + z
                if zon == 'Zona5':
                    zona5 = zona5 + z
                if zon == 'Zona6':
                    zona6 = zona6 + z
                if zon == 'Zona7':
                    zona7 = zona7 + z
                if zon == 'Zona8':
                    zona8 = zona8 + z
                if zon == 'Zona9':
                    zona9 = zona9 + z
            area = eurovision.find('AREA')
            med = float(area.find('Media_Porcentaje_Frames').text)
            arPro = med + arPro
            ws.write(8 + i, 19, str('%.2f' % med)+'%', style3)
            i = i+1

perTotalL = 0
for fichero in lstFiles:
    if fichero.startswith('_') == False:
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 8, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("scotland")
        if eurovision != None:
            impactosC = eurovision[0].find("Impactos")
            impac = float(impactosC.text) + impac
            ws.write(i + 8, 1, impactosC.text, style3)
            total = eurovision[1].find("Tiempo_acumulado_brand")
            tot = tot + float(total.text)

            partes = str(total.text).split(".")
            milesimas = int(partes[1])
            Dividendo = float(total.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            if (minutos == 0):
                if (segundos < 10):
                    ws.write(i + 8, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(i + 8, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                if (minutos < 10):
                    if (segundos < 10):
                        ws.write(i + 8, 2,
                                 str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                    else:
                        ws.write(i + 8, 2,
                                 str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                else:
                    if (segundos < 10):
                        ws.write(i + 8, 2,
                                 str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
                    else:
                        ws.write(i + 8, 2,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas), style3)
            max = eurovision[0].find("Tiempo_acumulado_max")
            m = float(max.text)
            if (m > n):  # para obtener el máximo
                n = m
            Dividendo2 = float(max.text)
            ws.write(i + 8, 3, str('%.f' % float(max.text)), style3)
            average = eurovision[0].find("Tiempo_acumulado_average")
            Dividendo = float(average.text)
            divisor = int(60)
            minutos = math.floor(Dividendo / divisor)
            segundos = Dividendo % divisor
            partes = str(average.text).split(".")
            entera = int(partes[0])
            milesimas = str('%.2f' % (float(average.text) - entera))
            partes2 = str(milesimas).split(".")
            milesimas2 = int(partes2[1])
            if (minutos == 0):
                if (segundos < 10):
                    ws.write(i + 8, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i + 8, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                if (minutos < 10):
                    if (segundos < 10):
                        ws.write(i + 8, 4,
                                 str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                    else:
                        ws.write(i + 8, 4,
                                 str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                else:
                    if (segundos < 10):
                        ws.write(i +8, 4,
                                 str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
                    else:
                        ws.write(i + 8, 4,
                                 str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                     '%.f' % milesimas2), style3)
            presence = eurovision.find('PRESENCE_Graf_2')
            per = float(presence.find('Porcentaje_brand').text)
            ws.write(i + 8, 5, str('%.2f' % per + '%'), style3)
            if fichero.startswith('_DAY') == True:
                perTotal = perTotal + per
            else:
                perTotalL = perTotalL + per
            tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
            tiem = tiem + float(tiempo.text)
            # Para obetener min y segundos
            Dividendo = float(tiempo.text)
            divisor = int(60)
            frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
            minutos = math.floor(Dividendo / divisor)
            min = minutos
            segundos = Dividendo % divisor
            if minutos > 60:
                Dividendo2 = minutos
                divisor2 = int(60)
                horas = math.floor(Dividendo2 / divisor2)
                minutos = Dividendo2 % divisor2
            else:
                horas = 0
            if (horas < 10 and horas >= 0):
                if (segundos < 10):
                    ws.write(i + 8, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i + 8, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 8, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '0' + '%.f' % (segundos)), style3)
                else:
                    ws.write(i +8, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                        '%.f' % (segundos)), style3)

            pos = eurovision.find('POSITION')
            j = 1
            while j <= 9:
                zon = 'Zona' + str(j)
                z = int(pos.find(zon).text)
                ws.write(i + 8, j + 8, z, style3)
                j = j + 1
                if zon == 'Zona1':
                    zona1 = zona1 + z
                if zon == 'Zona2':
                    zona2 = zona2 + z
                if zon == 'Zona3':
                    zona3 = zona3 + z
                if zon == 'Zona4':
                    zona4 = zona4 + z
                if zon == 'Zona5':
                    zona5 = zona5 + z
                if zon == 'Zona6':
                    zona6 = zona6 + z
                if zon == 'Zona7':
                    zona7 = zona7 + z
                if zon == 'Zona8':
                    zona8 = zona8 + z
                if zon == 'Zona9':
                    zona9 = zona9 + z
            area = eurovision.find('AREA')
            med = float(area.find('Media_Porcentaje_Frames').text)
            arPro = arPro + med
            ws.write(8 + i, 19, str('%.2f' % med)+'%', style3)
            i = i+1

ws.write(i + 8, 0, 'Total', style02)
ws.write(i + 8 ,1,impac,style02)
Dividendo = tot
divisor = int(60)
minutos = math.floor(Dividendo / divisor)
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if (minutos == 0):
    if (segundos < 10):
        ws.write(8 + i, 2, str('%.f' % (horas)+':00:0' + '%.f' % (segundos)), style02)
    else:
        ws.write(8 + i, 2, str('%.f' % (horas)+':00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style02)
else:
    if minutos > 60 or minutos == 60:
        horas = horas + 1
        minutos = minutos - 60
    if (minutos < 10):
        if (segundos < 10):
            ws.write(8 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style02)
        else:
            ws.write(8 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)
    else:
        if (segundos < 10):
            ws.write(8 + i, 2,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style02)
        else:
            ws.write(8 + i, 2,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)
    redondeo = str(n).split(".")
    ws.write(i+8,3,redondeo,style02)
    #aver = (tot/impac)
    ws.write(i+8,4,'%.2f' % (aver),style02)
    porcentaje = ((tot / tiem)*100)
    ws.write(i+8,5,'%.2f' % porcentaje+'%',style02)
    Dividendo = float(tiem)
    divisor = int(60)
    frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
    minutos = math.floor(Dividendo / divisor)
    min = minutos
    segundos = Dividendo % divisor
    if minutos > 60:
        Dividendo2 = minutos
        divisor2 = int(60)
        horas = math.floor(Dividendo2 / divisor2)
        minutos = Dividendo2 % divisor2

    else:
        horas = 00
    if(horas<10 and horas>0):
        if (minutos < 10):
            if (segundos < 10):
                ws.write(8 + i, 6,
                         str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style02)
            else:
                ws.write(8 + i, 6,
                         str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)
        else:
            if (segundos < 10):
                ws.write(8 + i, 6,
                         str('0'+'%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style02)
            else:
                ws.write(8 + i, 6,'0'+'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)

    else:
        if (minutos < 10):
            if (segundos < 10):
                ws.write(8 + i, 6,
                         str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style02)
            else:
                ws.write(8 + i, 6,
                         str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)
        else:
            if (segundos < 10):
                ws.write(8 + i, 6,
                         str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                         style02)
            else:
                ws.write(8 + i, 6,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style02)
    ws.write(i + 8 ,8,'Total',style02)
    ws.write(8 + i, 9, zona1, style02)
    ws.write(8 + i, 10, zona2, style02)
    ws.write(8 + i, 11, zona3, style02)
    ws.write(8 + i, 12, zona4, style02)
    ws.write(8 + i, 13, zona5, style02)
    ws.write(8 + i, 14, zona6, style02)
    ws.write(8 + i, 15, zona7, style02)
    ws.write(8 + i, 16, zona8, style02)
    ws.write(8 + i, 17, zona9, style02)
    ws.write(8 + i, 19, str('%.2f' % (arPro/len(lstFiles)) + '%'),style02)
    ws.write(k + 8,21,str('%.2f' % (perTotal/l) + '%'),style02)

#--------------------------------------------------------------------------------------
#-----------spar-------------------------------------------------------------------
#--------------------------------------------------------------------------------------

ws.write(i + 11, 0, 'spar', style03)
ws.write(i + 11,1,'Impacts',style1)
ws.write(i + 11,2,'Total',style1)
ws.write(i + 11,3,'Max',style1)
ws.write(i + 11,4,'Average',style1)
ws.write(i + 11,5,'Percentage',style1)
ws.write(i + 11,6,'Duration',style1)
ws.write(i + 11,9,'Zone1',style1)
ws.write(i + 11,10,'Zone2',style1)
ws.write(i + 11,11,'Zone3',style1)
ws.write(i + 11,12,'Zone4',style1)
ws.write(i + 11,13,'Zone5',style1)
ws.write(i + 11,14,'Zone6',style1)
ws.write(i + 11,15,'Zone7',style1)
ws.write(i + 11,16,'Zone8',style1)
ws.write(i + 11,17,'Zone9',style1)
ws.write(i + 11,19,'Area',style1)
ws.write(i + 11,21,'HL PERCENTAGE',style1)
ws.write(i + 11,22,'LS PERCENTAGES',style1)
impac = 0
tot =0
perTotal=0
tiem = 0
arPro=0
zona1 = 0
zona2 = 0
zona3 = 0
zona4 = 0
zona5 = 0
zona6 = 0
zona7 = 0
zona8 = 0
zona9 = 0
k=i
for fichero in lstFiles:

    if fichero.startswith('_'):
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 12 , 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("spar")
        impactosC = eurovision[0].find("Impactos")
        impac = float(impactosC.text) + impac
        ws.write(i + 12, 1, impactosC.text, style3)
        total = eurovision[1].find("Tiempo_acumulado_brand")
        tot = float(total.text) + tot
        partes = str(total.text).split(".")
        milesimas = int(partes[1])
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            if(segundos<10):
                ws.write(i + 12, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                ws.write(i + 12, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(i + 12, 2,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(i + 12, 2,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 12, 2,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
                else:
                    ws.write(i + 12, 2,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
        max = eurovision[0].find("Tiempo_acumulado_max")
        m = float(max.text)
        n = 1
        if (m > n):  # para obtener el máximo
            n = m
        Dividendo2 = float(max.text)
        ws.write(i + 12,3,str('%.f'% float(max.text)),style3)
        average = eurovision[0].find("Tiempo_acumulado_average")
        Dividendo = float(average.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        partes = str(average.text).split(".")
        entera= int(partes[0])
        milesimas=str('%.2f' % (float(average.text)-entera))
        partes2 = str(milesimas).split(".")
        milesimas2 = int(partes2[1])
        if (minutos == 0):
            if(segundos<10):
                ws.write(i + 12, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                ws.write(i + 12, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
        else:
            if(minutos<10):
                if (segundos < 10):
                    ws.write(i +12, 4,
                         str('0'+'%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i + 12, 4,
                             str('0'+'%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 12, 4,
                         str('%.f' % (minutos)) + ':' + str('0'+'%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
                else:
                    ws.write(i + 12, 4,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        ws.write(i + 12, 5, str('%.2f' % per + '%'), style3)
        perTotal = per + perTotal
        tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        tiem = float(tiempo.text) + tiem
        # Para obetener min y segundos
        Dividendo = float(tiempo.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if minutos > 60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2
        else:
            horas = 0
        if (horas < 10 and horas >= 0):
            if (segundos < 10):
                ws.write(i + 12, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 12, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        else:
            if (segundos < 10):
                ws.write(i + 12, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 12, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        pos = eurovision.find('POSITION')
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            ws.write(i+12,j+8,z,style3)
            j = j+1
            if zon == 'Zona1':
                zona1 = zona1 + z
            if zon == 'Zona2':
                zona2 = zona2 + z
            if zon == 'Zona3':
                zona3 = zona3 + z
            if zon == 'Zona4':
                zona4 = zona4 + z
            if zon == 'Zona5':
                zona5 = zona5 + z
            if zon == 'Zona6':
                zona6 = zona6 + z
            if zon == 'Zona7':
                zona7 = zona7 + z
            if zon == 'Zona8':
                zona8 = zona8 + z
            if zon == 'Zona9':
                zona9 = zona9 + z
        area = eurovision.find('AREA')
        med = float(area.find('Media_Porcentaje_Frames').text)
        arPro = med + arPro
        ws.write(12 + i, 19, str('%.2f' % med)+'%', style3)
        i = i + 1

perTotalL = 0
for fichero in lstFiles:
    if fichero.startswith('_') == False:
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        ws.write(i + 12, 0, video.text, style3)  # escribimos el titulo en la celda correspondiente
        eurovision = proyecto[4].find("spar")
        impactosC = eurovision[0].find("Impactos")
        impac = float(impactosC.text) + impac
        ws.write(i + 12, 1, impactosC.text, style3)
        total = eurovision[1].find("Tiempo_acumulado_brand")
        tot = tot + float(total.text)
        partes = str(total.text).split(".")
        milesimas = int(partes[1])
        Dividendo = float(total.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        if (minutos == 0):
            if (segundos < 10):
                ws.write(i + 12, 2, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
            else:
                ws.write(i + 12, 2, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas), style3)
        else:
            if (minutos < 10):
                if (segundos < 10):
                    ws.write(i + 12, 2,
                             str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
                else:
                    ws.write(i + 12, 2,
                             str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
            else:
                if (segundos < 10):
                    ws.write(i + 12, 2,
                             str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
                else:
                    ws.write(i + 12, 2,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas), style3)
        max = eurovision[0].find("Tiempo_acumulado_max")
        m = float(max.text)
        if (m > n):  # para obtener el máximo
            n = m
        Dividendo2 = float(max.text)
        ws.write(i + 12, 3, str('%.f' % float(max.text)), style3)
        average = eurovision[0].find("Tiempo_acumulado_average")
        Dividendo = float(average.text)
        divisor = int(60)
        minutos = math.floor(Dividendo / divisor)
        segundos = Dividendo % divisor
        partes = str(average.text).split(".")
        entera = int(partes[0])
        milesimas = str('%.2f' % (float(average.text) - entera))
        partes2 = str(milesimas).split(".")
        milesimas2 = int(partes2[1])
        if (minutos == 0):
            if (segundos < 10):
                ws.write(i + 12, 4, str('00:0' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
            else:
                ws.write(i + 12, 4, str('00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style3)
        else:
            if (minutos < 10):
                if (segundos < 10):
                    ws.write(i + 12, 4,
                             str('0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
                else:
                    ws.write(i + 12, 4,
                             str('0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
            else:
                if (segundos < 10):
                    ws.write(i +12, 4,
                             str('%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
                else:
                    ws.write(i + 12, 4,
                             str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)) + '.' + str(
                                 '%.f' % milesimas2), style3)
        presence = eurovision.find('PRESENCE_Graf_2')
        per = float(presence.find('Porcentaje_brand').text)
        if fichero.startswith('_DAY') == True:
            perTotal = perTotal + per
        else:
            perTotalL = perTotalL + per
        ws.write(i + 12, 5, str('%.2f' % per + '%'), style3)
        tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        tiem = tiem + float(tiempo.text)
        # Para obetener min y segundos
        Dividendo = float(tiempo.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if minutos > 60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2
        else:
            horas = 0
        if (horas < 10 and horas >= 0):
            if (segundos < 10):
                ws.write(i + 12, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i + 12, 6, str('0' + '%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        else:
            if (segundos < 10):
                ws.write(i + 12, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '0' + '%.f' % (segundos)), style3)
            else:
                ws.write(i +12, 6, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str(
                    '%.f' % (segundos)), style3)
        pos = eurovision.find('POSITION')
        j = 1
        while j <= 9:
            zon = 'Zona' + str(j)
            z = int(pos.find(zon).text)
            ws.write(i + 12, j + 8, z, style3)
            j = j + 1
            if zon == 'Zona1':
                zona1 = zona1 + z
            if zon == 'Zona2':
                zona2 = zona2 + z
            if zon == 'Zona3':
                zona3 = zona3 + z
            if zon == 'Zona4':
                zona4 = zona4 + z
            if zon == 'Zona5':
                zona5 = zona5 + z
            if zon == 'Zona6':
                zona6 = zona6 + z
            if zon == 'Zona7':
                zona7 = zona7 + z
            if zon == 'Zona8':
                zona8 = zona8 + z
            if zon == 'Zona9':
                zona9 = zona9 + z
        area = eurovision.find('AREA')
        med = float(area.find('Media_Porcentaje_Frames').text)
        arPro = arPro + med
        ws.write(12 + i, 19, str('%.2f' % med)+'%', style3)
        i = i+1

ws.write(i + 12,0,'Total',style03)
ws.write(i+12,1,impac,style03)
Dividendo = tot
divisor = int(60)
minutos = math.floor(Dividendo / divisor)
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if (minutos == 0):
    if (segundos < 10):
        ws.write(12 + i, 2, str('%.f' % (horas)+':00:0' + '%.f' % (segundos)), style03)
    else:
        ws.write(12 + i, 2, str('%.f' % (horas)+':00:' + '%.f' % (segundos)) + '.' + str('%.f' % milesimas2), style03)
else:
    if minutos > 60 or minutos == 60:
        horas = horas + 1
        minutos = minutos - 60
    if (minutos < 10):
        if (segundos < 10):
            ws.write(12 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 2,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)
    else:
        if (segundos < 10):
            ws.write(12 + i, 2,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 2,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)
redondeo = str(n).split(".")
ws.write(i+12,3,redondeo,style03)
#aver = (tot/impac)
ws.write(i+12,4,'%.2f' % (aver),style03)
porcentaje = ((tot / tiem)*100)
ws.write(i+12,5,'%.2f' % porcentaje+'%',style03)
Dividendo = float(tiem)
divisor = int(60)
frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
minutos = math.floor(Dividendo / divisor)
min = minutos
segundos = Dividendo % divisor
if minutos > 60:
    Dividendo2 = minutos
    divisor2 = int(60)
    horas = math.floor(Dividendo2 / divisor2)
    minutos = Dividendo2 % divisor2

else:
    horas = 00
if(horas<10 and horas>0):
    if (minutos < 10):
        if (segundos < 10):
            ws.write(12 + i, 6,
                     str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 6,
                     str('0'+'%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)
    else:
        if (segundos < 10):
            ws.write(12 + i, 6,
                     str('0'+'%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 6,'0'+'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)

else:
    if (minutos < 10):
        if (segundos < 10):
            ws.write(12 + i, 6,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 6,
                     str('%.f' % (horas)+':0' + '%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)
    else:
        if (segundos < 10):
            ws.write(12 + i, 6,
                     str('%.f' % (horas)+':'+'%.f' % (minutos)) + ':' + str('0' + '%.f' % (segundos)),
                     style03)
        else:
            ws.write(12 + i, 6,'%.f' % (horas)+':'+str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style03)
ws.write(i + 12 ,8,'Total',style03)
ws.write(12 + i, 9, zona1, style03)
ws.write(12 + i, 10, zona2, style03)
ws.write(12 + i, 11, zona3, style03)
ws.write(12 + i, 12, zona4, style03)
ws.write(12 + i, 13, zona5, style03)
ws.write(12 + i, 14, zona6, style03)
ws.write(12 + i, 15, zona7, style03)
ws.write(12 + i, 16, zona8, style03)
ws.write(12 + i, 17, zona9, style03)
ws.write(12 + i, 19, str('%.2f' % (arPro/len(lstFiles)) + '%'),style03)
ws.write(k + 12,21,str('%.2f' % (perTotal/l) + '%'),style03)



wb.save('Marcas scotland.xls')
