from pixmlabs import PiXMLabs as PXml
from pixmlabs import create_bbox, Point
import numpy as np
from pixmlabs.bresenham import bham_select

def get_zones(corners, size, squares=9):
    zones = dict()
    sq_line = np.sqrt(squares)
    x_grid = np.arange(0, size[0], size[0] / sq_line)
    y_grid = np.arange(0, size[1], size[1] / sq_line)

    x1, y1 = bham_select(corners['topleft'], corners['bottomleft'])
    x2, y2 = bham_select(corners['topleft'], corners['topright'])
    x3, y3 = bham_select(corners['topright'], corners['bottomright'])
    x4, y4 = bham_select(corners['bottomright'], corners['bottomleft'])

    zone_points = np.dstack(
        (np.concatenate((x1, x2, x3, x4)), np.concatenate((y1, y2, y3, y4)))
    )[0]

    # zone_points = [(x, y) for x, y in zip(
    #     (np.concatenate((x1, x2, x3, x4)), np.concatenate((y1, y2, y3, y4)))
    # )]

    for x, y in zone_points:
        # print(x)
        x_sq = np.argmax(x_grid > x) - 1 if np.argmax(x_grid > x) != 0 else sq_line - 1
        y_sq = np.argmax(y_grid > y) - 1 if np.argmax(y_grid > y) != 0 else sq_line - 1
        # print('corner', (x, y), 'x_sq', x_sq, 'y_sq', y_sq)
        zones[int((x_sq * sq_line + y_sq) + 1)] = True
        print(int((x_sq * sq_line + y_sq) + 1))

    return zones


ymin = 700
ymax = 1000
xmin = 0
xmax = 10

corners = dict(
    topleft=(xmin, ymin),
    bottomleft=(xmin, ymax),
    topright=(xmax, ymin),
    bottomright=(xmax, ymax)
)

get_zones(corners, (1280,720))