import glob
import os
import re

convert = lambda text: int(text) if text.isdigit() else text
alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]

path = '/media/stone/TOSHIBA_EXT/EuroChamps/video_0/images'


images = glob.glob(os.path.join(path,'*.jpg'))
images = sorted(images, key=alphanum_key)

for i, image in enumerate(images):
    print(i, len(images))
    pos1 = image.rfind('frame')
    pos2 = image.rfind('.jpg')


    image2 = image[:pos1 + 5] + str(int(image[pos1 + 5: pos2]) + 1) + '.jpg'

    os.rename(image, image2)
