from .multilan import MultiLanguageBase


class Dialogs(MultiLanguageBase):
    SUBDIR = 'dialogs'

    def __init__(self, lan=None):
        super(Dialogs, self).__init__(lan)


dialog = Dialogs()
