from .multilan import MultiLanguageBase


class Icons(MultiLanguageBase):
    SUBDIR = 'icons'

    def __init__(self, lan=None):
        super(Icons, self).__init__(lan)


icon = Icons()
