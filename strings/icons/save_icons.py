import _pickle as cPickle
import os
from os.path import join


def save_icons(save=True, verbose=False):
    FILES_ROOT = os.path.dirname(os.path.abspath(__file__))

    esES = dict(
        OPEN_LOGOS_HELP='Cargar clases...',
        OPEN_PROJECT_HELP='Cargar proyecto...',
        OPEN_IMAGES_HELP='Cargar imágenes...',
        OPEN_VIDEO_HELP='Cargar vídeos...',
        SAVE_HELP='Guardar proyecto',
        ADD_HELP='Añadir detección(a)',
        REMOVE_HELP='Eliminar detección(b)',
        UNDO_HELP='Deshacer detección añadida/dibujada(z)',
        REMOVE_ALL_HELP='Eliminar todos las detecciones de las marca/s(t)',
        DELETE_DRAW_HELP='Eliminar las detecciones dibujadas(d)',
        SELECT_HELP='Seleccionar detección(s)',
        NEXT_HELP='Siguiente frame(r)',
        BEFORE_HELP='Anterior frame(e)',
        NEXT_NUM_HELP='Siguiente frame (Con número)(4)',
        BEFORE_NUM_HELP='Anterior frame (Con número)(3)',
        NEXT_DETECT_HELP='Siguiente frame con detección(2)',
        BEFORE_DETECT_HELP='Anterior frame con detección(1)',
        DELETE_HELP='Eliminar imagen y .xml(ñ)',
        BOX_HELP='Número para añadir detecciones en más frames',
        LIST_DETECT_HELP='Lista de detecciones',
        LIST_LOGOS_HELP='Lista de clases',
        MORE_DET_HELP='Añadir más de una detección',
        DET_AT_ONCE_HELP='Mueve todas las detecciones a la vez',
        LANGUAGE_HELP='English',
        LANGUAGE_PIC='icon/uk.png',
        AUGMENT_HELP='Aumenta la muestra',
        XML_HELP='Crear XML Dashboard',
        XML_TXT_HELP='Crear TXT del XML Dashboard',
        RESTART_HELP='Reinicia IRT',
        LABELS_HELP = 'Crear Labels de los XML',
        XML_LABELS_HELP='Crear XML de los Labels',
        # I've got plenty more where that came from
    )
    enGB = dict(
        OPEN_LOGOS_HELP='Load classes...',
        OPEN_PROJECT_HELP='Load project...',
        OPEN_IMAGES_HELP='Load images...',
        OPEN_VIDEO_HELP='Load videos...',
        SAVE_HELP='Save project',
        ADD_HELP='Add detection(a)',
        REMOVE_HELP='Remove detection(b)',
        UNDO_HELP='Undo detection added/drawn(z)',
        REMOVE_ALL_HELP='Remove all brand/s detections(t)',
        DELETE_DRAW_HELP='Remove detections drawn(d)',
        SELECT_HELP='Select detection(s)',
        NEXT_HELP='Next frame(r)',
        BEFORE_HELP='Previous frame(e)',
        NEXT_NUM_HELP='Next frame (with box number)(4)',
        BEFORE_NUM_HELP='Previous frame (with box number)(3)',
        NEXT_DETECT_HELP='Next frame with detections(2)',
        BEFORE_DETECT_HELP='Previous frame with detections(1)',
        DELETE_HELP='Delete image and .xml(ñ)',
        BOX_HELP='Box number for add detections in more frames',
        LIST_DETECT_HELP='Detections list',
        LIST_LOGOS_HELP='Classes name list',
        MORE_DET_HELP='Add more than one detection',
        DET_AT_ONCE_HELP='Move all detections at the same time',
        LANGUAGE_HELP='Español',
        LANGUAGE_PIC='icon/es.png',
        AUGMENT_HELP='Augment samples',
        XML_HELP='Make Dashboard XML' ,
        XML_TXT_HELP='Make TXTs from Dashboard XML',
        LABELS_HELP='Make Labels from XMLs',
        XML_LABELS_HELP='Make XMLs from Labels',


        # I've got plenty more where that came from
    )

    if save:
        with open(join(FILES_ROOT, 'en-GB'), 'wb') as f_en, open(join(FILES_ROOT, 'es-ES'), 'wb') as f_es:
            cPickle.dump(esES, f_es)
            cPickle.dump(enGB, f_en)
    if verbose:
        print(join(FILES_ROOT, 'en-GB'))


if __name__ == '__main__':
    save_icons()
