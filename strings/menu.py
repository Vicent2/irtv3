from .multilan import MultiLanguageBase


class Menus(MultiLanguageBase):
    SUBDIR = 'menus'

    def __init__(self, lan=None):
        super(Menus, self).__init__(lan)


menu = Menus()
