import _pickle as cPickle
import os
from os.path import join


def save_menus(save=True, verbose=False):
    FILES_ROOT = os.path.dirname(os.path.abspath(__file__))

    esES = dict(
        OPEN_PROJECT='Cargar proyecto...\tCtrl+P',
        OPEN_LOGOS='Cargar clases...\tCtrl+L',
        OPEN_IMAGES='Cargar imágenes...\tCtrl+I',
        OPEN_VIDEOS='Cargar videos...\tCtrl+V',
        SAVE_PROJECT='Guardar proyecto...\tCtrl+S',
        FILE='Archivo',
        ACTIVATE_DEL='Des/Activar Elim. Archivos\tCtrl+D',
        UNDO='Deshacer detecciones añadidas/dibujadas\tCtrl+Z',
        EXIT='Salir\tCtrl+Q',
        FRAME_TXT='Crear txt de las detecciones\tCtrl+A',
        XMLD='Crear XML Dashboard\tCtrl+G',
        LABELS='Crear labels de los xmls\tCtrl+Y',
        XML_LABELS='Crear xmls de los labels\tCtrl+U',
        YOLO = 'Inicializar YOLO\tCtrl+E',
        LANGUAGE='English\tCtrl+B',
        REINIT='Reiniciar IRT',


        # I've got plenty more where that came from
    )
    enGB = dict(
        OPEN_PROJECT='Load project...\tCtrl+P',
        OPEN_LOGOS='Load classes...\tCtrl+L',
        OPEN_IMAGES='Load images...\tCtrl+I',
        OPEN_VIDEOS='Load videos...\tCtrl+V',
        SAVE_PROJECT='Save project...\tCtrl+S',
        FILE='File',
        ACTIVATE_DEL='Push on/off Del. files\tCtrl+D',
        UNDO='Undo detections added/drawn\tCtrl+Z',
        EXIT='Quit\tCtrl+Q',
        XMLD='Create Dashboard XML\tCtrl+G',
        FRAME_TXT='Create detections txt\tCtrl+A',
        LABELS='Create labels from xmls\tCtrl+Y',
        XML_LABELS='Create xmls from labels\tCtrl+U',
        YOLO='Initialize YOLO\tCtrl+E',
        LANGUAGE='Español\tCtrl+B',
        REINIT='Restart IRT',


        # I've got plenty more where that came from
    )

    if save:
        with open(join(FILES_ROOT, 'en-GB'), 'wb') as f_en, open(join(FILES_ROOT, 'es-ES'), 'wb') as f_es:
            cPickle.dump(esES, f_es)
            cPickle.dump(enGB, f_en)
    if verbose:
        print(join(FILES_ROOT, 'en-GB'))


if __name__ == '__main__':
    save_menus()
