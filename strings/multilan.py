import _pickle as cPickle
import os

DEFAULT_LANGUAGE = 'es-ES'


class MultiLanguageBase(object):
    """Base class for all the multilanguage managers in the application
    """
    SUPPORTED_LANGUAGES = ['es-ES', 'en-GB']
    DEFAULT_LANGUAGE = 'es-ES'
    MY_PATH = os.path.dirname(os.path.abspath(__file__))
    SUBDIR = ''
    language_instances = set()

    def __init__(self, lan=None, *args, **kwargs):
        if lan in self.SUPPORTED_LANGUAGES:
            self.lan = lan
        else:
            self.lan = self.DEFAULT_LANGUAGE
        self._data = None

        self._load_data()

        MultiLanguageBase.language_instances.add(self)

    def __getattr__(self, item):
        if item in self._data:
            return self._data[item]
        else:
            raise AttributeError("{} instance has no attribute {}".format(self.__class__.__name__, item))

    def change_language(self, lan, child=False):
        if child:
            self._change_child_language(lan)
            return

        if lan in self.SUPPORTED_LANGUAGES:
            self.lan = lan
            self._load_data()

    def _change_child_language(self, lan):
        if lan in self.SUPPORTED_LANGUAGES:
            for child in MultiLanguageBase.language_instances:
                child.change_language(lan)

    def _load_data(self):
        self.data_file = os.path.join(self.MY_PATH, os.path.join(self.SUBDIR, self.lan))
        with open(self.data_file, 'rb') as f:
            self._data = cPickle.load(f)
