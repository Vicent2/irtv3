from strings.status.save_state import save_state
from strings.icons.save_icons import save_icons
from strings.menus.save_menus import save_menus
from strings.dialogs.save_dialogs import save_dialogs


def save_all(save=True, verbose=False):
    save_state(save, verbose)
    save_icons(save, verbose)
    save_dialogs(save, verbose)
    save_menus(save, verbose)
