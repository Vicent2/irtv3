from .multilan import MultiLanguageBase


class States(MultiLanguageBase):
    SUBDIR = 'status'

    def __init__(self, lan=None):
        super(States, self).__init__(lan)


state = States()
