import _pickle as cPickle
import os
from os.path import join


def save_state(save=True, verbose=False):
    FILES_ROOT = os.path.dirname(os.path.abspath(__file__))

    esES = dict(
        INIT='IRT inicializada',
        LAST_SAVE='Última vez guardado: ',
        LOGOS_LOAD="Clases cargadas",
        REMOVE_FILES='Eliminadas %s imágenes y .xml',
        ADD_DETECT='Añadidas %s detecciones',
        REMOVE_DETECT='Eliminadas %s detecciones',
        SAVE_DETECT="Guardado = %s",
        CURRENT="Actual = %s",
        SELECT_LOGO='Logo seleccionado',
        SAVE_FILES='Archivos Guardados',
        CHANGE_LANGUAGE='Español',
        SAMPLES_DONE='¡¡MUESTRAS CREADAS!!',
        SAMPLES_NO_DONE='¡¡CREACIÓN DE LA MUESTRA ANULADO!!',
        VIDEOS_DONE='¡¡IMÁGENES CREADAS!!',
        VIDEOS_NO_DONE='¡¡CREACIÓN DE LAS IMÁGENES ANULADO!!',
        XML_DONE='¡¡XML CREADO!!',
        TXTS_DONE='¡¡TXTS CREADOS!!',
        LABELS_DONE='LABELS CREADOS!!',
        LABELS_NO_DONE='CREACIÓN DE LOS LABELS ANULADO!!',
        XML_LABELS_DONE='XMLS CREADOS!!',
        XML_LABELS_NO_DONE='CREACIÓN DE LOS XMLS ANULADO!!',

        # I've got plenty more where that came from
    )
    enGB = dict(
        INIT='IRT ready',
        LAST_SAVE='Last save: ',
        LOGOS_LOAD="Classes loaded",
        REMOVE_FILES='%s images and .xml removed',
        ADD_DETECT='%s detections added',
        REMOVE_DETECT='%s detections deleted',
        SAVE_DETECT="Saved = %s",
        CURRENT="Current = %s",
        SELECT_LOGO='Selected logo',
        SAVE_FILES='Saved files',
        CHANGE_LANGUAGE='English',
        SAMPLES_DONE='SAMPLES DONE!!',
        SAMPLES_NO_DONE='ABORTED SAMPLES CREATION!!',
        VIDEOS_DONE='VIDEOS DONE!!',
        VIDEOS_NO_DONE='ABORTED IMAGES CREATION!!',
        XML_DONE='XML DONE!!',
        TXTS_DONE='TXTS DONE!!',
        LABELS_DONE='LABELS DONE!!',
        LABELS_NO_DONE='ABORTED LABELS CREATION!!',
        XML_LABELS_DONE='XMLS DONE!!',
        XML_LABELS_NO_DONE='ABORTED XMLS CREATION!!',

        # I've got plenty more where that came from
    )

    if save:
        with open(join(FILES_ROOT, 'en-GB'), 'wb') as f_en, open(join(FILES_ROOT, 'es-ES'), 'wb') as f_es:
            cPickle.dump(esES, f_es)
            cPickle.dump(enGB, f_en)
    if verbose:
        print(join(FILES_ROOT, 'en-GB'))


if __name__ == '__main__':
    save_state()
