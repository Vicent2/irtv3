# -*- coding: utf-8 -*-
import os
import pandas as pd
import xlwt
import lxml
from lxml import etree
from datetime import datetime
from xlwt import *
import xlsxwriter
import math
from os import listdir

#IMPORTANTE, ANTES DE USAR EL SCRIPT
#Los xmls deben estar dentro de una carpeta, la ruta de esta carpeta es el path que es necesario modificar

#filename = '_LIVE_STAGE_SS16.xml'
#file = open(filename, mode='r')#archivo de lectura

#print(file.read())
#file.close()
#Estilos
#Estilo cabecera
style0 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour blue; borders: left thick, right thick, top thick, bottom thick;')
#Estilo celdas nombres xmls
style1 = xlwt.easyxf('font: name Arial, bold on; align: wrap on, horiz center; borders: left thick, right thick, top thick, bottom thick;')
#Estilo celdas interiores
style3 = xlwt.easyxf('font: name Arial; align: wrap on, horiz center; borders: left thin, right thin, top thin, bottom thin;')
#Estilo resultados
style4 = xlwt.easyxf('font: name Arial, colour white, bold on; align: wrap on, horiz center;  pattern: pattern solid, fore_colour black;')
#Estilo para la columna del tiempo
#style2 = xlwt.easyxf('',num_format_str='h:mm:ss')
#creamos el excel y la fila de las cabeceras
wb = xlwt.Workbook()
ws = wb.add_sheet('A Test Sheet',cell_overwrite_ok=True)
ws.write(0, 0, 'Video', style0)
ws.write(0, 1, 'Minutes', style1)
ws.write(0, 2, 'Secs', style1)
ws.write(0, 3, 'Frames', style1)
ws.write(0, 4, 'Time', style1)
nombres = ws.col(0)
nombres.width = 256 * 25
#lectura de los ficheros de la carpeta
#Variable para la ruta al directorio
path = 'xmls/'
#Lista vacia para incluir los ficheros
lstFiles = []
#Lista con todos los ficheros del directorio:
lstDir = os.walk(path)   #os.walk()Lista directorios y ficheros
# Crea una lista de los ficheros xml que existen en el directorio y los incluye a la lista.
for root, dirs, files in lstDir:
    for fichero in files:
        (nombreFichero, extension) = os.path.splitext(fichero)
        if (extension == ".xml"):
            lstFiles.append(nombreFichero + extension)
            # print (nombreFichero+extension)

tiempoFinal=0
i = 1
framesFinal=0
lstFiles.sort()
for fichero in lstFiles:

    if fichero.startswith('_'):
        doc = etree.parse('xmls/' + fichero)
        proyecto = doc.getroot()  # buscamos la raiz de nuestro xml
        video = proyecto[0]  # primer elto del que obtenemos el título de nuestro video
        tiempo = proyecto[3]  # obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        # Para obetener min y segundos
        Dividendo = float(tiempo.text)
        divisor = int(60)
        frames = math.floor(Dividendo * int(25))  # calculamos el número de frames
        minutos = math.floor(Dividendo / divisor)
        min = minutos
        segundos = Dividendo % divisor
        if minutos > 60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2
        else:
            horas = 00
        ws.write(i, 0, video.text, style1)  # escribimos el titulo en la celda correspondiente
        ws.write(i, 1, '%.f' % (min), style3)
        ws.write(i, 2, str('%.f' % (segundos)), style3)
        ws.write(i, 3, str(frames), style3)  # escribimos el numero de frames total
        ws.write(i, 4, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style3)
        tiempoFinal = float(tiempo.text) + tiempoFinal
        framesFinal = frames + framesFinal
        i = i + 1

for fichero in lstFiles:
    #print(fichero)
    Dividendo=0
    Dividendo2=0
    divisor=0
    divisor2=0
    horas=0
    minutos=0
    segundos=0
    if fichero.startswith('_') == False:
        doc = etree.parse('xmls/'+fichero)
        proyecto = doc.getroot()#buscamos la raiz de nuestro xml
        video = proyecto[0]#primer elto del que obtenemos el título de nuestro video
        tiempo = proyecto[3]#obtenemos el tiempo del video, de donde tendremos que sacar tanto los min como segun.
        #Para obetener min y segundos
        Dividendo=float(tiempo.text)
        divisor=int(60)
        frames = math.floor(Dividendo*int(25))#calculamos el número de frames
        minutos=math.floor(Dividendo/divisor)
        min = minutos
        segundos=Dividendo%divisor
        if minutos>60:
            Dividendo2 = minutos
            divisor2 = int(60)
            horas = math.floor(Dividendo2 / divisor2)
            minutos = Dividendo2 % divisor2
        else:
            horas = 00
        ws.write(i, 0, video.text, style1)  # escribimos el titulo en la celda correspondiente
        ws.write(i, 1, '%.f' % (min),style3)
        ws.write(i, 2, str('%.f' % (segundos)),style3)
        ws.write(i, 3, str(frames),style3)# escribimos el numero de frames total
        ws.write(i, 4, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)),style3)
        tiempoFinal= float(tiempo.text) + tiempoFinal
        framesFinal = frames + framesFinal
        i=i+1

    ws.write(i, 3, framesFinal,style4)
    Dividendo=float(tiempoFinal)
    divisor=int(60)
    minutos=math.floor(Dividendo/divisor)
    segundos=Dividendo%divisor
    if(segundos>50):
        ws.write(i,1, str(minutos+1),style4)
    else:
        ws.write(i, 1, str(minutos), style4)
    #aqui añadir el total de min  y segundos
    if minutos>60:
        Dividendo2 = minutos
        divisor2 = int(60)
        horas = math.floor(Dividendo2 / divisor2)
        minutos = Dividendo2 % divisor2
    else:
        horas = 00
    ws.write(i, 4, str('%.f' % (horas)) + ':' + str('%.f' % (minutos)) + ':' + str('%.f' % (segundos)), style4)
    # forma de hacer la formula ws.write(2, 2, xlwt.Formula("A3+B3"))
    #ws.Columns.AutoFit()
wb.save('Tiempos WRC.xls')
