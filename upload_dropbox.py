# -*- coding: utf-8 -*-
import os
import sys
import dropbox
from dropbox.files import WriteMode
from dropbox.exceptions import ApiError, AuthError
import glob
from dropbox.files import WriteMode

# # Uploads contents of LOCALFILE to Dropbox
def backup():
    with open(LOCALFILE, 'rb') as f:
        # We use WriteMode=overwrite to make sure that the settings in the file
        # are changed on upload
        print("Uploading " + LOCALFILE + " to Dropbox as " + BACKUPPATH + "...")
        file_size = os.path.getsize(LOCALFILE)

        CHUNK_SIZE = 4 * 1024 * 1024

        if file_size <= CHUNK_SIZE:
            dbx.files_upload(f.read(), BACKUPPATH)
        else:

            upload_session_start_result = dbx.files_upload_session_start(f.read(CHUNK_SIZE))
            cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,
                                                       offset=f.tell())
            commit = dropbox.files.CommitInfo(path=BACKUPPATH)

            while f.tell() < file_size:
                if ((file_size - f.tell()) <= CHUNK_SIZE):
                    dbx.files_upload_session_finish(f.read(CHUNK_SIZE),
                                                    cursor,
                                                    commit)
                else:
                    dbx.files_upload_session_append(f.read(CHUNK_SIZE),
                                                    cursor.session_id,
                                                    cursor.offset)
                    cursor.offset = f.tell()


dropbox_folder = '/PIXEL/PROYECTOS/EUC/EUC-Videos/'
folders = glob.glob('/media/stone/6EE36BE271AC7556/Eurochamps_Videos/mp4/*')
TOKEN = 'Cw7uYTZ5p-AAAAAAAAAMzHytG1xdcrC9q9W3_BFZtWLN2zQS7JwMgYjWJupu503H'
num_files = 0
for folder in folders:
    day = os.path.basename(folder)
    # day = 'D' + day[1:]
    folder_sess = os.path.join(folder,'Sessions')
    files = glob.glob(os.path.join(folder_sess,'*'))

    for file in files:
        pos1 = file.find('?')
        if pos1 != -1:
            file_no_sig = file[:pos1].replace(' ','')
            os.rename(file,file_no_sig)
        else:
            file_no_sig = file.replace(' ','')
            os.rename(file,file_no_sig)

        num_files += 1
        # path_file_upload = os.path.join(dropbox_folder,day,os.path.basename(file_no_sig))
        #
        # LOCALFILE = file_no_sig
        # BACKUPPATH = path_file_upload
        # print(BACKUPPATH)
        #
        # # Check for an access token
        # if (len(TOKEN) == 0):
        #     sys.exit(
        #         "ERROR: Looks like you didn't add your access token. Open up backup-and-restore-example.py in a text editor and paste in your token in line 14.")
        #
        # # Create an instance of a Dropbox class, which can make requests to the API.
        # # print("Creating a Dropbox object...")
        # dbx = dropbox.Dropbox(TOKEN)
        #
        # # Check that the access token is valid
        # try:
        #     dbx.users_get_current_account()
        # except AuthError as err:
        #     sys.exit(
        #         "ERROR: Invalid access token; try re-generating an access token from the app console on the web.")
        #
        # backup()
        #
        # print("Done!")
print(num_files)

#

