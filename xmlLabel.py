import xml.etree.ElementTree as ET
import os
import glob
import re
import cv2

def convertion(size, box):
    dw = 1. / (size[0])
    dh = 1. / (size[1])
    x = (box[0] + box[1]) / 2.0 - 1
    y = (box[2] + box[3]) / 2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return (x, y, w, h)

clases = []
with open('clases.labels','r') as f:
    reader = f.readlines()
    for line in reader:
        if line != '\n':
            clases.append(line.split('\n')[0])


xmls = glob.glob('xmls/*')
convert = lambda text: int(text) if text.isdigit() else text
alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
xmls = sorted(xmls, key=alphanum_key)

for i, xml in enumerate(xmls):
    print(i,len(xmls))
    label = xml.replace('xmls','labels').replace('.xml','.txt')

    infile = open(xml, 'r')
    tree = ET.parse(infile)
    root = tree.getroot()

    size = root.find('size')
    w = int(size.findtext('width'))
    h = int(size.findtext('height'))

    with open(label,'w') as f:
        for obj in root.iter('object'):
            name = obj.findtext('name')
            class_index = clases.index(name)

            if obj.find('bndbox') != None:

                for bndbox in obj.iter('bndbox'):
                    xmin = int(float(bndbox.findtext('xmin')))
                    xmax = int(float(bndbox.findtext('xmax')))
                    ymin = int(float(bndbox.findtext('ymin')))
                    ymax = int(float(bndbox.findtext('ymax')))

                    bb = convertion((w,h),(xmin,xmax,ymin,ymax))


                    f.write(str(class_index) + " " + " ".join([str(a) for a in bb]) + os.linesep)