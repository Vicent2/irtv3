import xml.etree.ElementTree as ET
import sys
from pixmlabs.event import SubEventParser
from pixmlabs import _indent
from pprint import pprint
import math

def add_subelement_text(root, subelname, text):
    ET.SubElement(root, subelname).text = str(text)

def populate_element(root, kpi_dict, total_time):

    pprint(kpi_dict)

    g1 = ET.SubElement(root, 'PRESENCE_Graf_1')
    graph1_dict = kpi_dict['graph1']
    ET.SubElement(g1, 'Tiempo_acumulado_brand').text = str(graph1_dict['timeTotal'])
    ET.SubElement(g1, 'Tiempo_acumulado_max').text = str(graph1_dict['timeMax'])
    ET.SubElement(g1, 'Tiempo_acumulado_min').text = str(graph1_dict['timeMin'])
    ET.SubElement(g1, 'Tiempo_acumulado_average').text = str(graph1_dict['timeAvg'])
    ET.SubElement(g1, 'Impactos').text = str(int(graph1_dict['impacts']))
    g2 = ET.SubElement(root, 'PRESENCE_Graf_2')
    graph2_dict = kpi_dict['graph2']
    ET.SubElement(g2, 'Porcentaje_brand').text = str(graph2_dict['percentage'])
    ET.SubElement(g2, 'Tiempo_acumulado_brand').text = str(graph2_dict['timeTotal'])
    g3 = ET.SubElement(root, 'PRESENCE_Graf_3')
    graph3_dict = kpi_dict['graph3']
    for i in graph3_dict:
        apa = ET.SubElement(g3, 'Aparicion{}'.format(i))
        add_subelement_text(apa, 'Inicio', graph3_dict[i][0])
        add_subelement_text(apa, 'Fin', graph3_dict[i][1])

    g4 = ET.SubElement(root, 'TIMELINE')
    look_index = 0
    accumulated = 0
    keep_looking = True
    for i in range(math.ceil(float(total_time)) + 1):
        try:
            #print(i, graph3_dict[look_index][1])
            if keep_looking and i > graph3_dict[look_index][1]:
                accumulated += 1
                look_index += 1
        except KeyError:
            keep_looking = False
        ET.SubElement(g4, 'Tiempo_Acumulado_seg{}'.format(i)).text = str(accumulated)


    position = ET.SubElement(root, 'POSITION')
    new_zone = [round(zone) for zone in kpi_dict['area']]
    tot = sum(new_zone)
    if tot != 100:
        v = 100 - tot
        inx = new_zone.index(max(new_zone))
        new_zone[inx] = new_zone[inx] + v

    for i, zone in enumerate(new_zone):
        ET.SubElement(position, 'Zona{}'.format(i+1)).text = str(zone)


    area = ET.SubElement(root, 'AREA')
    ET.SubElement(area, 'Media_Porcentaje_Frames').text = str(kpi_dict['areaAvg'])

    summary = ET.SubElement(root, 'SUMMARY')
    ET.SubElement(summary, 'Porcentaje_del_Total_por_marcas').text = str(kpi_dict['summary'])

    return root

def main(filename):
    old_root = ET.parse(filename).getroot()
    tiempo_video = old_root.find('video_duration').text
    info_dict = SubEventParser(filename)()


    new_root = ET.Element('Proyecto')
    add_subelement_text(new_root, 'folder', 'undefined')
    add_subelement_text(new_root, 'Video', 'undefined')
    add_subelement_text(new_root, 'Enlace', 'undefined')
    add_subelement_text(new_root, 'Tiempo_Video', tiempo_video)
    brands = ET.SubElement(new_root, 'brands')
    for brand in info_dict:
        brand_element = ET.Element(brand)
        brand_element = populate_element(brand_element, info_dict[brand], tiempo_video)
        brands.append(brand_element)

    _indent(new_root).write(filename.replace('.xml','_DASH.xml'))








if __name__=='__main__':
    main(sys.argv[1])