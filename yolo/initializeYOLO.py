import os
import subprocess
import time

import wx


class initializeYOLO(object):
    def __init__(self, pathYOLO, pathData, pathCfg, pathWeights, pathVideo, thresh, pathClasses, parent):
        self.pathYOLO = pathYOLO
        self.pathData = pathData
        self.pathCfg = pathCfg
        self.pathWeights = pathWeights
        self.pathVideo = pathVideo
        self.thresh = '-thresh ' + thresh
        self.pathClasses = pathClasses
        self.parent = parent
        self.colors = ['#9e31b7', "#51963a", "#21668f", "#ffb400", "#fe3f2b", "#26cbc7"]

    def run(self):
        classes = [cls.strip() for cls in open(self.pathClasses, 'r').readlines()]

        if '' in classes:
            del classes[classes.index('')]
        n_classes = len(classes)
        cont_classes = [0] * n_classes
        classes_frame = [0] * n_classes
        cont_impacts = [0] * n_classes
        impacts = [0] * n_classes
        previous_frame = [0] * n_classes
        pathExe = os.path.join(self.pathYOLO, 'darknet')
        full_command = ['-', 'detector', 'demo', self.pathData, self.pathCfg, self.pathWeights, self.pathVideo,
                        self.thresh]
        frame = 0
        Pds = []
        Sbs = []
        font = wx.Font(18, wx.FONTFAMILY_ROMAN, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_BOLD)
        for i, clas in enumerate(classes):

            pd = wx.ProgressDialog(clas + ' (mm:ss % Impacts)', '',
                                   maximum=101,
                                   parent=self.parent,
                                   style=wx.PD_APP_MODAL | wx.PD_AUTO_HIDE)

            pd.SetFont(font)
            pd.SetToolTip('Presence\'s percentages between the brands on the event')
            pd.FitInside()
            sB = wx.StatusBar(pd, name=clas)
            sB.SetStatusText('%s 00:00 0%% 0 Impacts' % clas)
            sB.SetFont(font)
            sB.SetForegroundColour('white')
            if i >= len(self.colors):
                z = i % len(self.colors)
            else:
                z = i
            sB.SetOwnBackgroundColour(self.colors[z])
            sB.SetSize((500, 50))
            Sbs.append(sB)

            pd.SetSize((500, 120))
            pd.SetPosition((1420, 20 + 120 * i))
            pd.SetOwnBackgroundColour(self.colors[z])
            pd.InheritsBackgroundColour()
            pd.Update(0)
            Pds.append(pd)
            time.sleep(0.2)
        for i, pd in enumerate(Pds):
            pd.Update(cont_classes[i] / (sum(cont_classes) + 1) * 100)
        with subprocess.Popen(
                full_command,
                executable=pathExe,
                stdout=subprocess.PIPE,
                bufsize=0,
                universal_newlines=True,

        ) as p:
            for line in p.stdout:

                cls = line.split('\t')[0]
                if line.strip().isdigit():
                    frame = int(line.strip())
                elif cls in classes:
                    index = classes.index(cls)
                    if frame != classes_frame[index]:
                        classes_frame[index] = frame

                        if classes_frame[index] == previous_frame[index] + 1:
                            cont_impacts[index] += 1

                            if cont_impacts[index] >= 6:
                                if cont_impacts[index] == 6:
                                    impacts[index] += 1
                                    cont_classes[index] += 6
                                else:
                                    cont_classes[index] += 1


                        else:
                            cont_impacts[index] = 0
                        previous_frame[index] = classes_frame[index]
                        for i, pd in enumerate(Pds):
                            pd.Update(cont_classes[i] / (sum(cont_classes) + 1) * 100)

                            minu = int((cont_classes[i] / 25 / 60))
                            seg = cont_classes[i] / 25 - (minu * 60)
                            Sbs[i].SetStatusText('%s %02d:%02d %2.1f%% %d Impacts' % (
                                classes[i], minu, seg, cont_classes[i] / (sum(cont_classes) + 1) * 100, impacts[i]))
